package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentOwnerRegistrationBinding;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;

public class OwnerRegistration extends Fragment {
    EditText emailEditText, passwordEditText, confirmPasswordEditText, nameEditText, surnameEditText, numberEditText, streetEditText, cityEditText, countryEditText;
    private FragmentOwnerRegistrationBinding binding;
    public OwnerRegistration() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOwnerRegistrationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnOwnerNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User owner = new User(binding.etEmail.getText().toString(), binding.etPassword.getText().toString(), binding.etName.getText().toString(), binding.etSurname.getText().toString(), binding.etPhone.getText().toString(), binding.etStreet.getText().toString(), binding.etCity.getText().toString(), binding.etCountry.getText().toString(), null, null);
                Bundle bundle = new Bundle();
                bundle.putParcelable("owner", owner);
                Navigation.findNavController(view).navigate(R.id.navigateToCompanyRegistration, bundle);
            }

        });
    }
}