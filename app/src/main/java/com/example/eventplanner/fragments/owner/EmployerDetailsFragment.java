package com.example.eventplanner.fragments.owner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployerDetailsBinding;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.squareup.picasso.Picasso;

import java.time.DayOfWeek;

public class EmployerDetailsFragment extends Fragment {

    private FragmentEmployerDetailsBinding binding;
    private User employer;

    public EmployerDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentEmployerDetailsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){
            employer = getArguments().getParcelable("employer");
            bindData();
        }
        binding.btnEditTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToEditWorkingTime);
            }
        });

        binding.btnSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("employer", employer);
                Navigation.findNavController(view).navigate(R.id.navigateToSchedule, bundle);
            }
        });
    }

    private void bindData(){
        binding.textName.setText(employer.getName() + " " + employer.getSurname());
        binding.textEmail.setText(employer.getEmail());
        binding.textAddress.setText(employer.getStreet() + ", " + employer.getCity() + ", " + employer.getCountry());
        binding.textPhone.setText(employer.getPhone());
        Picasso.get().load(employer.getProfileImage()).into(binding.ivProfile);

        for(WorkingDay day : employer.getWorkingDays()){
            if (day.getDayOfWeek().equals(DayOfWeek.MONDAY)){
                binding.monFrom.setText(day.getStartTime().toString());
                binding.monTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.TUESDAY)){
                binding.tueFrom.setText(day.getStartTime().toString());
                binding.tueTo.setText(day.getEndTime().toString());
            }if (day.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)){
                binding.wedFrom.setText(day.getStartTime().toString());
                binding.wedTo.setText(day.getEndTime().toString());
            }if (day.getDayOfWeek().equals(DayOfWeek.THURSDAY)){
                binding.thuFrom.setText(day.getStartTime().toString());
                binding.thuTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.FRIDAY)){
                binding.friFrom.setText(day.getStartTime().toString());
                binding.friTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.SATURDAY)){
                binding.satFrom.setText(day.getStartTime().toString());
                binding.satTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.SUNDAY)){
                binding.sunFrom.setText(day.getStartTime().toString());
                binding.sunTo.setText(day.getEndTime().toString());
            }
        }
    }
}