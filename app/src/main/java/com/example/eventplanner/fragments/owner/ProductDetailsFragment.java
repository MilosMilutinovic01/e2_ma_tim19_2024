package com.example.eventplanner.fragments.owner;

import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.async.CompanyLoadedListener;
import com.example.eventplanner.async.OnBudgetsLoadedListener;
import com.example.eventplanner.databinding.FragmentProductDetailsBinding;
import com.example.eventplanner.model.Budget;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.BudgetService;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.ProductService;
import com.squareup.picasso.Picasso;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductDetailsFragment extends Fragment {

    private FragmentProductDetailsBinding binding;
    private Product product;
    private Company company;
    private ProductService productService;
    private CompanyService companyService;
    private AuthService authService;
    public ProductDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProductDetailsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        productService = new ProductService(requireContext());
        authService = new AuthService(requireContext());
        companyService = new CompanyService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if (user.getRole().equals(Role.OWNER)) {
                        binding.btnEditProduct.setVisibility(View.VISIBLE);
                        binding.btnDeleteProduct.setVisibility(View.VISIBLE);
                        binding.tvVisibility.setVisibility(View.VISIBLE);
                    } else if (user.getRole().equals(Role.EMPLOYER)){
                        binding.tvVisibility.setVisibility(View.VISIBLE);
                    } else if (user.getRole().equals(Role.EVENT_ORGANIZER)){
                        if(product.isAvailable()){
                            binding.btnBuyProduct.setVisibility(View.VISIBLE);
                        }
                        binding.btnViewCompany.setVisibility(View.VISIBLE);
                    }
                });

        if (getArguments() != null) {
            this.product = getArguments().getParcelable("selectedProduct");
            if (getArguments() != null) {
                bindProductDetails(this.product);
            }
        }
        binding.btnEditProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("product",product);
                Navigation.findNavController(view).navigate(R.id.navigateToEditProduct,bundle);
            }
        });
        binding.btnDeleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productService.deleteProduct(product.getId());
                Navigation.findNavController(view).navigate(R.id.navigateToProducts);
            }
        });
        EventService eventService = new EventService(requireContext());
        List<String> eventNames = new ArrayList<>();
        List<Event> allEvents = new ArrayList<>();
        eventService.getAllEvents(new EventService.OnEventsLoadedListener() {
            @Override
            public void onEventsLoaded(List<Event> events) {
                for(Event event: events){
                    eventNames.add(event.getEventName());
                    allEvents.add(event);
                }
            }

            @Override
            public void onFailure(Exception e) {

            }
        });
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, eventNames);
        binding.dropDownEvents.setAdapter(adapter1);

        binding.btnBuyProduct.setOnClickListener(v -> {
            BudgetService budgetService = new BudgetService(requireContext());
            authService.getCurrentUser()
                    .addOnSuccessListener(user -> {
                        if (user != null) {
                            String selectedEventName = binding.dropDownEvents.getText().toString();
                            for (Event event : allEvents) {
                                if (event.getEventName().equals(selectedEventName)) {
                                    String eventId = event.getId();
                                    budgetService.getBudgetByEventId(eventId, new OnBudgetsLoadedListener() {
                                        @Override
                                        public void onBudgetsLoaded(List<Budget> budgets) {
                                            // Not used in this case
                                        }

                                        @Override
                                        public void onBudgetLoaded(Budget budget) {
                                            updateOrCreateBudget(budget, budgetService);
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e("ProductDetailsFragment", "Error loading budget: " + e.getMessage());
                                            createNewBudget(eventId, budgetService);
                                        }
                                    });
                                    break;
                                }
                            }
                        } else {
                            Toast.makeText(requireContext(), "User not authenticated", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(e -> {
                        Log.e("ProductDetailsFragment", "Error getting current user: " + e.getMessage());
                        Toast.makeText(requireContext(), "Failed to create budget", Toast.LENGTH_SHORT).show();
                    });
        });


        binding.btnViewCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                companyService.getCompanyById(product.getCompanyId(), new CompanyLoadedListener() {
                    @Override
                    public void onCompanyLoaded(Company company) {
                        bundle.putParcelable("company",company);
                        Navigation.findNavController(view).navigate(R.id.navigateToCompanyProfile, bundle);
                    }
                    @Override
                    public void onFailure(Exception e) {
                        Log.e("TAG", "Error getting company: " + e.getMessage());
                    }
                });
            }
        });
    }
    private void bindProductDetails(Product product) {
        binding.tvName.setText(product.getName());
        binding.tvDescription.setText(product.getDescription());
        binding.tvOldPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", product.getPrice()));

        if (product.getDiscount() != 0.0) {
            double newPrice = product.getPrice() - (product.getPrice() * (product.getDiscount() / 100));
            binding.tvNewPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
            binding.tvOldPrice.setTextColor(ContextCompat.getColor(getContext(), android.R.color.darker_gray));
            binding.tvOldPrice.setPaintFlags(binding.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.tvNewPrice.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        } else {
            binding.tvNewPrice.setVisibility(View.GONE);
        }

        binding.tvCategory.setText(product.getCategory().getName() + ", " + product.getSubcategory().getName());

        StringBuilder eventTypesText = new StringBuilder();
        for (EventType eventType : product.getEventTypes()) {
            eventTypesText.append(eventType.getName()).append(", ");
        }
        eventTypesText.setLength(eventTypesText.length() - 2);
        binding.tvEventTypes.setText(eventTypesText.toString());


        if (product.isAvailable()) {
            binding.tvAvailable.setText("AVAILABLE");
            binding.tvAvailable.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark));
        } else {
            binding.tvAvailable.setText("NOT AVAILABLE");
            binding.tvAvailable.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        }

        if (product.isVisible()) {
            binding.tvVisibility.setText("VISIBLE");
        } else {
            binding.tvVisibility.setText("NOT VISIBLE");
        }

        Picasso.get()
                .load(product.getImage())
                .into(binding.ivProductImg);
    }

    private void updateOrCreateBudget(Budget budget, BudgetService budgetService) {
        List<Budget.BudgetItem> budgetItems = budget.getBudgetItems();
        boolean itemExists = false;
        for (Budget.BudgetItem item : budgetItems) {
            if (item.getCategory().equals(product.getCategory().getName()) &&
                    item.getSubcategory().equals(product.getSubcategory().getName())) {
                item.setPlannedAmount(item.getPlannedAmount() + product.getPrice());
                itemExists = true;
                break;
            }
        }
        if (!itemExists) {
            Budget.BudgetItem budgetItem = new Budget.BudgetItem(product.getCategory().getName(), product.getSubcategory().getName(), product.getPrice());
            budgetItem.setPurchased(true);
            budgetItems.add(budgetItem);
        }
        budget.setTotalBudget(budget.getTotalBudget() + product.getPrice());
        budgetService.saveBudget(budget);
        Toast.makeText(requireContext(), "Budget updated successfully", Toast.LENGTH_SHORT).show();
    }

    private void createNewBudget(String eventId, BudgetService budgetService) {
        Budget newBudget = new Budget();
        newBudget.setEventId(eventId);
        newBudget.setTotalBudget(product.getPrice());
        List<Budget.BudgetItem> budgetItems = new ArrayList<>();
        Budget.BudgetItem budgetItem = new Budget.BudgetItem(product.getCategory().getName(), product.getSubcategory().getName(), product.getPrice());
        budgetItem.setPurchased(true);
        budgetItems.add(budgetItem);
        newBudget.setBudgetItems(budgetItems);
        budgetService.saveBudget(newBudget);
        Toast.makeText(requireContext(), "New budget created successfully", Toast.LENGTH_SHORT).show();
    }

}