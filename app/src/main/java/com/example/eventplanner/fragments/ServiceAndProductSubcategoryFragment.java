package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.ServiceAndProductCategoryAdapter;
import com.example.eventplanner.adapters.ServiceAndProductSubcategoryAdapter;
import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.async.OnSubcategoriesLoadedListener;
import com.example.eventplanner.async.SubcategoryUploadCallback;
import com.example.eventplanner.databinding.FragmentServiceAndProductSubcategoryBinding;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.SubcategoryService;

import java.util.ArrayList;
import java.util.List;


public class ServiceAndProductSubcategoryFragment extends Fragment implements RecyclerViewInterface {
    private ServiceAndProductSubcategoryAdapter serviceAndProductSubcategoryAdapter;
    private List<Subcategory> subcategoriesList;
    private FragmentServiceAndProductSubcategoryBinding binding;
    private View view;
    private SubcategoryService subcategoryService;

    public ServiceAndProductSubcategoryFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentServiceAndProductSubcategoryBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        subcategoryService = new SubcategoryService(requireContext());
        subcategoriesList = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getSubcategories(new ProductAndServiceCategory());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        serviceAndProductSubcategoryAdapter = new ServiceAndProductSubcategoryAdapter(getActivity(), subcategoriesList, this);
        binding.recyclerView.setAdapter(serviceAndProductSubcategoryAdapter);

        binding.btnNewServiceAndProductSubcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewServiceAndProductSubcategoryFragment editFragment = NewServiceAndProductSubcategoryFragment.newInstance("", "", SubcategoryType.SERVICE, "");

                // Show the fragment for editing
                editFragment.show(getChildFragmentManager(), "New category");
            }
        });

    }

    private void getSubcategories(ProductAndServiceCategory category){
        subcategoryService.getAllSubcategories(category, new OnSubcategoriesLoadedListener() {
            @Override
            public void onSubcategoriesLoaded(List<Subcategory> subcategories) {
                subcategoriesList.clear();
                subcategoriesList.addAll(subcategories);
                serviceAndProductSubcategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });
    }
    @Override
    public void onEditClick(int position) {
        Subcategory selectedSubcategory = subcategoriesList.get(position);
        NewServiceAndProductSubcategoryFragment editFragment = NewServiceAndProductSubcategoryFragment.newInstance(selectedSubcategory.getName(), selectedSubcategory.getDescription(), selectedSubcategory.getSubcategoryType(), selectedSubcategory.getId());

        // Show the fragment for editing
        editFragment.show(getChildFragmentManager(), "Edit subcategory");
    }

    @Override
    public void onDeleteClick(int position) {
        Subcategory selectedSubcategory = subcategoriesList.get(position);
        String subcategoryIdToDelete = selectedSubcategory.getId();
        subcategoryService.deleteSubcategory(subcategoryIdToDelete, new SubcategoryUploadCallback() {
            @Override
            public void onSuccess() {
                // Handle success
                Toast.makeText(getContext(), "Subcategory deleted successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String errorMessage) {
                // Handle failure
                Toast.makeText(getContext(), "Failed to delete subcategory: " + errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBlockClick(int position) {

    }

    @Override
    public void onItemClick(int position) {

    }
}