package com.example.eventplanner.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentCompanyStepRegistrationBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.User;

import java.util.ArrayList;

public class CompanyStepRegistrationFragment extends Fragment {
    private FragmentCompanyStepRegistrationBinding binding;

    public CompanyStepRegistrationFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCompanyStepRegistrationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        User owner1 = new User();
        if(getArguments() != null){
            owner1 = getArguments().getParcelable("owner");
        }
        final User owner = owner1;
        binding.btnCompanyStepNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Company company = new Company(binding.etEmail.getText().toString(), binding.etName.getText().toString(), binding.etDescription.getText().toString(), binding.etPhone.getText().toString(), binding.etStreet.getText().toString(), binding.etCity.getText().toString(), binding.etCountry.getText().toString(), new ArrayList<>());
                Bundle bundle = new Bundle();
                bundle.putParcelable("owner", owner);
                bundle.putParcelable("company", company);
                Navigation.findNavController(view).navigate(R.id.navigateToAdditionalCompanyEntries, bundle);
            }
        });
    }
}