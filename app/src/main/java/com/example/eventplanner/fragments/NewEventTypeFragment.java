package com.example.eventplanner.fragments;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.eventplanner.R;
import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.EventTypeUploadCallback;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventTypeService;

import java.util.ArrayList;


public class NewEventTypeFragment extends DialogFragment {
    EventTypeService eventTypeService;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_new_event_type, null);
        eventTypeService = new EventTypeService(getContext());

        // Find the EditText views
        EditText nameEditText = view.findViewById(R.id.dialogEventTypeName);
        EditText descriptionEditText = view.findViewById(R.id.dialogEventTypeDescription);

        Bundle args = getArguments();
        if (args != null) {
            String name = args.getString("name");
            String description = args.getString("description");
            String id = args.getString("id");
            // Set data to EditText fields
            nameEditText.setText(name);
            descriptionEditText.setText(description);
        }

        AlertDialog alertDialog = new AlertDialog.Builder(requireContext())
                .setView(view)
                .setTitle("Enter Event type Details")
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", (dialog, which) -> {
                    // Handle cancel button click
                })
                .create();

        alertDialog.setOnShowListener(dialogInterface -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
                // Retrieve text from EditText views when the OK button is clicked
                String name = nameEditText.getText().toString();
                String description = descriptionEditText.getText().toString();
                EventType eventType = new EventType(name, description, false, new ArrayList<>());
                if(!args.getString("id").equals("")){
                    eventType.setId(args.getString("id"));
                }
                if(args.getString("name").equals("") && args.getString("description").equals("")){
                    eventTypeService.createEventType(eventType);
                }else{
                    eventTypeService.updateEventType(eventType, new EventTypeUploadCallback() {
                        @Override
                        public void onSuccess() {
                            // Handle success
                            alertDialog.dismiss();
                        }

                        @Override
                        public void onFailure(String errorMessage) {
                            // Handle failure
                            Log.e("EventTypeUpdate", errorMessage);
                            // Optionally show an error message to the user
                        }
                    });
                }

                alertDialog.dismiss();
            });
        });

        return alertDialog;

    }

    public static NewEventTypeFragment newInstance(String name, String description, String id, Boolean blocked) {
        NewEventTypeFragment fragment = new NewEventTypeFragment();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("description", description);
        args.putString("id", id);
        args.putString("blocked", blocked.toString());
        fragment.setArguments(args);
        return fragment;
    }
}