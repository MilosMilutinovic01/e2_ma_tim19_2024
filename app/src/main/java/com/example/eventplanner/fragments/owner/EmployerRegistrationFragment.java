package com.example.eventplanner.fragments.owner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployerRegistrationBinding;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class EmployerRegistrationFragment extends Fragment {
    private FragmentEmployerRegistrationBinding binding;

    private AuthService authService;
    private User owner;
    private Bitmap selectedImageBitmap = null;
    public EmployerRegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentEmployerRegistrationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        owner = new User();
        if(getArguments() != null){
            owner = getArguments().getParcelable("owner");
        }

        binding.btnAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });


        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareData();

                User employer = new User(binding.etEmail.getText().toString(), binding.etPassword.getText().toString(), binding.etName.getText().toString(), binding.etSurname.getText().toString(), binding.etPhone.getText().toString(), binding.etStreet.getText().toString(), binding.etCity.getText().toString(), binding.etCountry.getText().toString(), null, null);
                employer.setCompanyId(owner.getCompanyId());

                Bundle bundle = new Bundle();
                bundle.putParcelable("employer", employer);
                bundle.putParcelable("image", selectedImageBitmap);
                bundle.putParcelable("owner", owner);
                Navigation.findNavController(view).navigate(R.id.navigateToAddWorkingTime, bundle);
            }
        });
    }

    private void prepareData(){
        String email = binding.etEmail.getText().toString();
        String password = binding.etPassword.getText().toString();
        String confirmPassword = binding.etConfirmPassword.getText().toString();

        boolean isValidated = validateData(email, password, confirmPassword);
        if(!isValidated){
            return;
        }

        if(selectedImageBitmap == null){
            selectedImageBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(EmployerRegistrationFragment.this.getResources(), R.drawable.profile), 960, 960, false);
        }
    }

    void imageChooser() {

        // create an instance of the
        // intent of the type image
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        launchSomeActivity.launch(i);
    }

    ActivityResultLauncher<Intent> launchSomeActivity
            = registerForActivityResult(
            new ActivityResultContracts
                    .StartActivityForResult(),
            result -> {
                if (result.getResultCode()
                        == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    // do your operation from here....
                    if (data != null
                            && data.getData() != null) {
                        Uri selectedImageUri = data.getData();
                        try {
                            selectedImageBitmap = MediaStore.Images.Media.getBitmap(
                                    requireActivity().getContentResolver(),
                                    selectedImageUri);
                            binding.imgProfile.setImageBitmap(
                                    selectedImageBitmap);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

    boolean validateData(String email, String password, String confirmPassword){
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            binding.etEmail.setError("Email is invalid!");
            return false;
        }

        if(password.length() < 6){
            binding.etPassword.setError("Password length is invalid!");
            return false;
        }

        if(!password.equals(confirmPassword)){
            binding.etConfirmPassword.setError("Password not matched!");
            return false;
        }

        return true;
    }
}