package com.example.eventplanner.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.adapters.ServiceAndProductCategoryAdapter;
import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentServiceAndProductCategoryBinding;
import com.example.eventplanner.fragments.owner.OwnerHomeViewFragment;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.UserService;

import java.util.ArrayList;
import java.util.List;


public class ServiceAndProductCategoryFragment extends Fragment implements RecyclerViewInterface {
    private ServiceAndProductCategoryAdapter serviceAndProductCategoryAdapter;
    private List<ProductAndServiceCategory> categoriesList;
    private FragmentServiceAndProductCategoryBinding binding;
    private View view;
    private CategoryService categoryService;

    public ServiceAndProductCategoryFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentServiceAndProductCategoryBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        categoryService = new CategoryService(requireContext());
        categoriesList = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCategories();

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        serviceAndProductCategoryAdapter = new ServiceAndProductCategoryAdapter(getActivity(), categoriesList, this);
        binding.recyclerView.setAdapter(serviceAndProductCategoryAdapter);

        binding.btnNewServiceAndProductCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewServiceAndProductCategoryFragment editFragment = NewServiceAndProductCategoryFragment.newInstance("", "", "");

                // Show the fragment for editing
                editFragment.show(getChildFragmentManager(), "New category");
            }
        });

    }

    private void getCategories(){
        categoryService.getAllCategories(new OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded(List<ProductAndServiceCategory> categories) {
                categoriesList.clear();
                categoriesList.addAll(categories);
                serviceAndProductCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        ProductAndServiceCategory selectedCategory = categoriesList.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("category", selectedCategory);
        Navigation.findNavController(requireView()).navigate(R.id.navigateToSubcategoryFragment, bundle);
    }

    @Override
    public void onEditClick(int position) {
        ProductAndServiceCategory selectedCategory = categoriesList.get(position);
        NewServiceAndProductCategoryFragment editFragment = NewServiceAndProductCategoryFragment.newInstance(selectedCategory.getName(), selectedCategory.getDescription(), selectedCategory.getId());

        // Show the fragment for editing
        editFragment.show(getChildFragmentManager(), "Edit category");
    }

    @Override
    public void onDeleteClick(int position) {
        ProductAndServiceCategory selectedCategory = categoriesList.get(position);
        String categoryIdToDelete = selectedCategory.getId();
        categoryService.deleteCategory(categoryIdToDelete, new CategoryUploadCallback() {
            @Override
            public void onSuccess() {
                // Handle success
                Toast.makeText(getContext(), "Category deleted successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String errorMessage) {
                // Handle failure
                Toast.makeText(getContext(), "Failed to delete category: " + errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBlockClick(int position) {

    }
}