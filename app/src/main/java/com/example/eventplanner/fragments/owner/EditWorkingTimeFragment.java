package com.example.eventplanner.fragments.owner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.core.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEditWorkingTimeBinding;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class EditWorkingTimeFragment extends Fragment {
    private FragmentEditWorkingTimeBinding binding;
    TextView datePicker;

    public EditWorkingTimeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentEditWorkingTimeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        datePicker = binding.etDate;
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDialog();
            }
        });
    }

    private void openDatePickerDialog() {
        CalendarConstraints.Builder constraintsBuilder = new CalendarConstraints.Builder();
        long currentMillis = MaterialDatePicker.todayInUtcMilliseconds();

        constraintsBuilder.setStart(currentMillis);

        MaterialDatePicker picker = MaterialDatePicker.Builder.dateRangePicker()
                .setTitleText("Select date range")
                .setCalendarConstraints(constraintsBuilder.build())
                .build();

        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener((w)->{
            Pair<Long, Long> selectedDates = (Pair<Long, Long>) picker.getSelection();


        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault());
        String startDate = dateFormat.format(new Date(selectedDates.first));
        String endDate = dateFormat.format(new Date(selectedDates.second));

        datePicker.setText(startDate + " - " + endDate);
        });
        picker.addOnNegativeButtonClickListener((w)->{
            picker.dismiss();
        });
    }
}