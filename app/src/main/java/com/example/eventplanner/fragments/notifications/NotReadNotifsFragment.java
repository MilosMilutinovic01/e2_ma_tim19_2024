package com.example.eventplanner.fragments.notifications;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.NotificationAdapter;
import com.example.eventplanner.async.OnNotificationsLoadedListener;
import com.example.eventplanner.databinding.FragmentNotReadNotifsBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;


public class NotReadNotifsFragment extends Fragment implements RecyclerViewInterface {

    private FragmentNotReadNotifsBinding binding;

    private NotificationAdapter notificationAdapter;
    private List<Notification> unReadNotifications = new ArrayList<>();
    private NotificationService notificationService;
    private AuthService authService;
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    //private User currentUser;
    private View view;

    public NotReadNotifsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentNotReadNotifsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.view = view;
        authService = new AuthService(requireContext());
        notificationService = new NotificationService(requireContext());

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getNotifs();
    }

    public void getNotifs(){

                    notificationService.getUnReadNotificationsByReceiverId(new OnNotificationsLoadedListener() {
                        @Override
                        public void onNotificationsLoaded(List<Notification> uNotifications) {
                            unReadNotifications = (ArrayList<Notification>) uNotifications;
                            binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            notificationAdapter = new NotificationAdapter(getActivity(), unReadNotifications, NotReadNotifsFragment.this);
                            binding.recyclerView.setAdapter(notificationAdapter);
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e("TAG", "Error getting services: " + e.getMessage());
                        }
                    },currentUser.getUid());

    }

    @Override
    public void onItemClick(int position) {
        Notification notification = unReadNotifications.get(position);
        notification.setRead(true);
        if (notificationService.updateNotificationData(notification)) {
            Toast.makeText(getContext(), "Notif reading initiated!", Toast.LENGTH_SHORT).show();
            unReadNotifications.remove(position);
            notificationAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getContext(), "Failed to read notif!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Refresh the data when the fragment is resumed
        getNotifs();
    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}