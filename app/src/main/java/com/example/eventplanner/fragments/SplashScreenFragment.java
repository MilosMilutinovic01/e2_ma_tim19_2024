package com.example.eventplanner.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentLoginBinding;
import com.example.eventplanner.databinding.FragmentSplashScreenBinding;

import java.util.Timer;
import java.util.TimerTask;


public class SplashScreenFragment extends Fragment {

    public SplashScreenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash_screen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int SPLASH_TIME_OUT = 3000;
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Navigation.findNavController(view).

                                navigate(R.id.navigateToLogin);
                    }
                });
            }
        }, SPLASH_TIME_OUT);

    }

}

