package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.RatingsAdapter;
import com.example.eventplanner.async.OnRatingsLoadedListener;
import com.example.eventplanner.databinding.FragmentCompanyRatingsBinding;
import com.example.eventplanner.fragments.owner.RatingFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.RatingsService;

import java.util.ArrayList;
import java.util.List;

public class CompanyRatingsFragment extends Fragment implements RecyclerViewInterface {
    private FragmentCompanyRatingsBinding binding;
    private Company company;
    private RatingsService ratingsService;
    private List<CompanyRating> ratings;
    private RatingsAdapter ratingsAdapter;
    private AuthService authService;

    public CompanyRatingsFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentCompanyRatingsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ratingsService = new RatingsService(requireContext());
        authService = new AuthService(requireContext());
        ratings = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null ){
            company = getArguments().getParcelable("company");
        }

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ratingsAdapter = new RatingsAdapter(getActivity(), ratings, CompanyRatingsFragment.this, authService);
        binding.recyclerView.setAdapter(ratingsAdapter);

        ratingsService.getAllRatings(company.getId(), new OnRatingsLoadedListener() {
            @Override
            public void onRatingsLoaded(List<CompanyRating> companyRatings) {
                ratings.clear();
                ratings.addAll(companyRatings);
                ratingsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });

        binding.btnRatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("company", company);
                Navigation.findNavController(view).navigate(R.id.navigateToLeaveCompanyRatingFragment, bundle);
            }
        });
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}