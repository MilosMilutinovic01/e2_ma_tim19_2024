package com.example.eventplanner.fragments.owner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.databinding.FragmentProductEditBinding;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.services.ProductService;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

public class ProductEditFragment extends Fragment {
    private FragmentProductEditBinding binding;
    private ProductService productService;
    EditText name, description, price, discount;
    CheckBox available,visible;
    MultiAutoCompleteTextView eventTypes;
    ProductAndServiceCategory selectedCategory;
    Subcategory selectedSubcategory;
    ArrayList<EventType> selectedEventTypes;
    Bitmap selectedImageBitmap;
    Product selectedProduct;
    public ProductEditFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentProductEditBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        productService = new ProductService(requireContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
           selectedProduct = getArguments().getParcelable("product");
            if (getArguments() != null) {
                bindProductDetails(selectedProduct);
            }
        }
        binding.btnEditProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToProducts);
            }
        });
        binding.btnAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        ArrayList<EventType>  ets = new ArrayList<>();
        ets.add(new EventType("Svadbe", "", false, new ArrayList<>()));
        ets.add(new EventType("Rodjendani", "", false, new ArrayList<>()));
        ets.add(new EventType("Krstenja", "", false, new ArrayList<>()));
        ets.add(new EventType("Rodjenja", "", false, new ArrayList<>()));

        eventTypes = binding.etEventTypes;
        ArrayAdapter<EventType> etsAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, ets);
        eventTypes.setAdapter(etsAdapter);
        eventTypes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        selectedEventTypes = selectedProduct.getEventTypes();

        eventTypes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventType selectedEventType = (EventType) parent.getItemAtPosition(position);
                if (!selectedEventTypes.contains(selectedEventType)) {
                    selectedEventTypes.add(selectedEventType);
                }
            }
        });

        name = binding.etProductName;
        description = binding.etDescription;
        price = binding.etPrice;
        discount = binding.etDiscount;
        available = binding.cbAvailable;
        visible = binding.cbVisible;

        Picasso.get()
                .load(selectedProduct.getImage())
                .into(binding.ivProductImg);
        binding.btnEditProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProduct(view);
            }
        });

    }

    void imageChooser() {

        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        launchSomeActivity.launch(i);
    }

    ActivityResultLauncher<Intent> launchSomeActivity
            = registerForActivityResult(
            new ActivityResultContracts
                    .StartActivityForResult(),
            result -> {
                if (result.getResultCode()
                        == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    // do your operation from here....
                    if (data != null
                            && data.getData() != null) {
                        Uri selectedImageUri = data.getData();
                        try {
                            selectedImageBitmap = MediaStore.Images.Media.getBitmap(
                                    requireActivity().getContentResolver(),
                                    selectedImageUri);
                            binding.ivProductImg.setImageBitmap(selectedImageBitmap);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
    void editProduct(View view){
        boolean isValidated = validateData();
        if(!isValidated){
            return;
        }
        double productPrice = Double.parseDouble(price.getText().toString());
        double productDiscount = Double.parseDouble(discount.getText().toString());
        boolean productIsAvailable = available.isChecked();
        boolean productIsVisible = visible.isChecked();

        Product product = new Product(selectedProduct.getId(),selectedCategory,selectedSubcategory,name.getText().toString(),description.getText().toString(),productPrice,productDiscount,selectedEventTypes,selectedProduct.getImage(),productIsAvailable,productIsVisible, selectedProduct.getCompanyId());


        if(productService.editProduct(product, selectedImageBitmap)){
            Navigation.findNavController(requireView()).navigate(R.id.navigateToProducts);
            Toast.makeText(getContext(), "Product edited successfully!", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getContext(), "Failed to edit product!", Toast.LENGTH_SHORT).show();
        }
    }

    boolean validateData(){
        if(binding.etProductName.getText().toString().isEmpty()){
            binding.etProductName.setError("Cannot be empty");
            return false;
        }
        if(binding.etDescription.getText().toString().isEmpty()){
            binding.etDescription.setError("Cannot be empty");
            return false;
        }
        if(binding.etDiscount.getText().toString().isEmpty()){
            binding.etDiscount.setError("Cannot be empty");
            return false;
        }
        if(binding.etPrice.getText().toString().isEmpty()){
            binding.etDiscount.setError("Cannot be empty");
            return false;
        }
        return true;
    }
    private void bindProductDetails(Product product) {
        binding.etProductName.setText(product.getName());
        binding.etDescription.setText(product.getDescription());
        binding.etPrice.setText(String.valueOf(product.getPrice()));
        binding.etDiscount.setText(String.valueOf(product.getDiscount()));
        binding.cbAvailable.setChecked(product.isAvailable());
        binding.cbVisible.setChecked(product.isVisible());
        selectedCategory = product.getCategory();
        selectedSubcategory = product.getSubcategory();
        StringBuilder tokenString = new StringBuilder();
        for (EventType eventType : product.getEventTypes()) {
            tokenString.append(eventType.getName()).append(", ");
        }

        binding.etEventTypes.setText(tokenString.toString());
    }
}

