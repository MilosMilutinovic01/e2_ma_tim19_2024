package com.example.eventplanner.fragments.owner;

import static android.content.ContentValues.TAG;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.adapters.ProductsAdapter;
import com.example.eventplanner.adapters.ServicesAdapter;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentOwnerHomeViewBinding;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.databinding.FragmentServicesBinding;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Employer;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ReservationConfirmationType;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.async.OnServicesLoadedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServicesFragment extends Fragment implements RecyclerViewInterface {
    private ServicesAdapter servicesAdapter;
    private List<Service> services;
    private ArrayList<EventType> eventTypes;
    private FragmentServicesBinding binding;
    private View view;
    private ServiceService serviceService;

    private AuthService authService;


    public ServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentServicesBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.view = view;
        serviceService = new ServiceService(requireContext());
        authService = new AuthService(requireContext());

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceService.getAllServices(new OnServicesLoadedListener() {
            @Override
            public void onServicesLoaded(List<Service> allServices) {
                services = allServices;
                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                servicesAdapter = new ServicesAdapter(getActivity(), services, ServicesFragment.this);
                binding.recyclerView.setAdapter(servicesAdapter);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting services: " + e.getMessage());
            }
        });

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        servicesAdapter = new ServicesAdapter(getActivity(), services, this);
        binding.recyclerView.setAdapter(servicesAdapter);

        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if (user.getRole().equals(Role.OWNER)) {
                        binding.btnAddNew.setVisibility(View.VISIBLE);
                    }
                });
        binding.btnAddNew.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToServiceCreationFragment);
            }
        });
    }
    @Override
    public void onItemClick(int position) {
        Service selectedService = services.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("selectedService", selectedService);
        Navigation.findNavController(this.view).navigate(R.id.navigateToServiceDetails, bundle);
    }

    @Override
    public void onEditClick(int position) {
    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}