package com.example.eventplanner.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentAdditionalCompanyEntriesBinding;
import com.example.eventplanner.databinding.FragmentCompanyWorkTimeBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyWorkingTime;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.AuthService;
import com.google.android.material.textfield.TextInputEditText;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class CompanyWorkTimeFragment extends Fragment {
    private AuthService authService;
    private FragmentCompanyWorkTimeBinding binding;
    TimePickerDialog picker;

    public CompanyWorkTimeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCompanyWorkTimeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        User owner1 = new User();
        Company company1 = new Company();
        ProductAndServiceCategory productAndServiceCategory1 = new ProductAndServiceCategory();
        EventType eventType1 = new EventType();
        if(getArguments() != null){
            owner1 = getArguments().getParcelable("owner");
            company1 = getArguments().getParcelable("company");
            productAndServiceCategory1 = getArguments().getParcelable("productAndServiceCategory");
            eventType1 = getArguments().getParcelable("eventType");
            Log.d("TEST2", owner1.getName() + company1.getName() + productAndServiceCategory1.getName() + eventType1.getName());
        }
        final User owner = owner1;
        final Company company = company1;
        final ProductAndServiceCategory productAndServiceCategory = productAndServiceCategory1;
        final EventType eventType = eventType1;
        List<EventType> eventTypes = new ArrayList<>();
        eventTypes.add(eventType);
        List<ProductAndServiceCategory> productAndServiceCategories = new ArrayList<>();
        productAndServiceCategories.add(productAndServiceCategory);

        binding.btnCompanyFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<WorkingDay> workingDays = new ArrayList<>();
                workingDays.add(new WorkingDay(DayOfWeek.MONDAY, LocalTime.parse(binding.etCompanyMon.getText().toString()), LocalTime.parse(binding.etCompanyMonEnd.getText().toString())));
                workingDays.add(new WorkingDay(DayOfWeek.TUESDAY, LocalTime.parse(binding.etCompanyTue.getText().toString()), LocalTime.parse(binding.etCompanyTueEnd.getText().toString())));
                workingDays.add(new WorkingDay(DayOfWeek.WEDNESDAY, LocalTime.parse(binding.etCompanyWed.getText().toString()), LocalTime.parse(binding.etCompanyWedEnd.getText().toString())));
                workingDays.add(new WorkingDay(DayOfWeek.THURSDAY, LocalTime.parse(binding.etCompanyThu.getText().toString()), LocalTime.parse(binding.etCompanyThuEnd.getText().toString())));
                workingDays.add(new WorkingDay(DayOfWeek.FRIDAY, LocalTime.parse(binding.etCompanyFri.getText().toString()), LocalTime.parse(binding.etCompanyFriEnd.getText().toString())));
                company.setWorkingDays(workingDays);
                if (authService.registerOwner(owner, company, eventTypes, productAndServiceCategories)) {
                    Toast.makeText(getContext(), "Registration initiated!", Toast.LENGTH_SHORT).show();
                    Navigation.findNavController(requireView()).navigate(R.id.navigateToLogin);
                } else {
                    Toast.makeText(getContext(), "Failed to initiate registration!", Toast.LENGTH_SHORT).show();
                }
            }

        });


        binding.etCompanyMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyMon);
            }
        });

        binding.etCompanyMonEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyMonEnd);
            }
        });

        binding.etCompanyTue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyTue);
            }
        });

        binding.etCompanyTueEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyTueEnd);
            }
        });

        binding.etCompanyWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyWed);
            }
        });

        binding.etCompanyWedEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyWedEnd);
            }
        });

        binding.etCompanyThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyThu);
            }
        });

        binding.etCompanyThuEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyThuEnd);
            }
        });

        binding.etCompanyFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyFri);
            }
        });

        binding.etCompanyFriEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etCompanyFriEnd);
            }
        });
    }

    private void openTimePicker(TextInputEditText et){

        final Calendar cldr = Calendar.getInstance();
        int hour = cldr.get(Calendar.HOUR_OF_DAY);
        int minutes = cldr.get(Calendar.MINUTE);

        picker = new TimePickerDialog(getActivity(),
                (tp, sHour, sMinute) ->{
                    String formattedTime = String.format(Locale.getDefault(), "%02d:%02d", sHour, sMinute);
                    et.setText(formattedTime);
                }
                , hour, minutes, true);
        picker.show();

    }
}