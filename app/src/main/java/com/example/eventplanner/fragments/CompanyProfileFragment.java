package com.example.eventplanner.fragments;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentCompanyProfileBinding;
import com.example.eventplanner.databinding.FragmentEmployerDetailsBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.UserReportService;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CompanyProfileFragment extends Fragment {

    private FragmentCompanyProfileBinding binding;
    private Company company;
    private UserReportService userReportService;
    private AuthService authService;
    private UserService userService;

    private View view;

    private NotificationService notificationService;

    public CompanyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCompanyProfileBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        userReportService = new UserReportService(requireContext());
        authService = new AuthService(requireContext());
        userService = new UserService(requireContext());
        notificationService = new NotificationService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){
            company = getArguments().getParcelable("company");
            bindData();
        }
    }
    private void bindData(){
        binding.textName.setText(company.getName());
        binding.textEmail.setText(company.getEmail());
        binding.textAddress.setText(company.getStreet() + ", " + company.getCity() + ", " + company.getCountry());
        binding.textPhone.setText(company.getPhone());
        for(WorkingDay day : company.getWorkingDays()){
            if (day.getDayOfWeek().equals(DayOfWeek.MONDAY)){
                binding.monFrom.setText(day.getStartTime().toString());
                binding.monTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.TUESDAY)){
                binding.tueFrom.setText(day.getStartTime().toString());
                binding.tueTo.setText(day.getEndTime().toString());
            }if (day.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)){
                binding.wedFrom.setText(day.getStartTime().toString());
                binding.wedTo.setText(day.getEndTime().toString());
            }if (day.getDayOfWeek().equals(DayOfWeek.THURSDAY)){
                binding.thuFrom.setText(day.getStartTime().toString());
                binding.thuTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.FRIDAY)){
                binding.friFrom.setText(day.getStartTime().toString());
                binding.friTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.SATURDAY)){
                binding.satFrom.setText(day.getStartTime().toString());
                binding.satTo.setText(day.getEndTime().toString());
            }
            if (day.getDayOfWeek().equals(DayOfWeek.SUNDAY)){
                binding.sunFrom.setText(day.getStartTime().toString());
                binding.sunTo.setText(day.getEndTime().toString());
            }
        }
        binding.btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(v.getContext());
                View dialogView = inflater.inflate(R.layout.dialog_report_reason, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Report Reason")
                        .setView(dialogView)
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EditText editTextReason = dialogView.findViewById(R.id.editTextReason);
                                String reason = editTextReason.getText().toString().trim();

                                authService.getCurrentUser()
                                        .addOnSuccessListener(currentUser -> {
                                            User reporter = currentUser;
                                            userService.getUserByCompanyId(company.getId())
                                                    .addOnSuccessListener(companyOwner -> {
                                                        User reported = companyOwner;

                                                        userService.getAdmin()
                                                                .addOnSuccessListener(admin -> {
                                                                    User notificationReceiver = admin;

                                                                    userReportService.createUserReport(new UserReport(
                                                                            null,
                                                                            reporter.getId(),
                                                                            reported.getId(),
                                                                            reporter,
                                                                            reported,
                                                                            LocalDate.now(),
                                                                            reason,
                                                                            UserReportStatus.REPORTED,
                                                                            ""
                                                                    ));
                                                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                                                    notificationService.createNotification(new Notification(
                                                                            null,
                                                                            "New user report",
                                                                            "User " + reporter.getEmail() + " reported the company " + company.getName() + " " + LocalDateTime.now().format(formatter),
                                                                            admin.getId(),
                                                                            false
                                                                    ));

                                                                    Navigation.findNavController(v).navigate(R.id.navigateToEventOrganizerHomeViewFragment);
                                                                })
                                                                .addOnFailureListener(e -> Log.e(TAG, "Error getting admin: " + e.getMessage()));
                                                    })
                                                    .addOnFailureListener(e -> Log.e(TAG, "Error getting company owner: " + e.getMessage()));
                                        })
                                        .addOnFailureListener(e -> Log.e(TAG, "Error getting current user: " + e.getMessage()));
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create()
                        .show();
            }
        });

        binding.btnRatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("company", company);
                Navigation.findNavController(view).navigate(R.id.navigateToCompanyRatingsFragment, bundle);
            }
        });

    }
}