package com.example.eventplanner.fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentLoginBinding;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.UserReportService;

public class LoginFragment extends Fragment {
    private AuthService authService;
    private UserReportService userReportService;

    EditText emailEditText, passwordEditText;
    Button loginButton;
    private FragmentLoginBinding binding;
    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());
        userReportService = new UserReportService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailEditText = binding.etEmail;
        passwordEditText = binding.etPassword;
        binding.btnSignIn.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.navigateToUserSelection);
            }
        });
        binding.btnLogin.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                loginUser(view);
            }
        });
    }

    void loginUser(View view){
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean isValidated = validateData(email, password);
        if(!isValidated){
            return;
        }
        userReportService.isUserBlocked(email, isBlocked -> {
            if (isBlocked) {
                Toast.makeText(getContext(), "Login failed: account blocked", Toast.LENGTH_SHORT).show();
            }
            else{
                authService.loginUser(email, password)
                        .addOnSuccessListener(user -> {
                            // Handle successful login
                            Toast.makeText(getContext(), "Login successful!", Toast.LENGTH_SHORT).show();
                            if(user.getRole().equals(Role.ADMINISTRATOR)){
                                Navigation.findNavController(requireView()).navigate(R.id.navigateToServiceAndProductCategoryFragment);
                            }
                            if(user.getRole().equals(Role.EVENT_ORGANIZER)){
                                Navigation.findNavController(requireView()).navigate(R.id.navigateToEventOrganizerHomeViewFragment);
                            }
                            if(user.getRole().equals(Role.OWNER)){
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("user", user);
                                Navigation.findNavController(requireView()).navigate(R.id.navigateToOwnerHomeView, bundle);
                            }
                            if(user.getRole().equals(Role.EMPLOYER)){
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("user", user);
                                Navigation.findNavController(requireView()).navigate(R.id.navigateToSchedule, bundle);
                            }
                        })
                        .addOnFailureListener(e -> {
                            // Handle login failure
                            Toast.makeText(getContext(), "Login failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        });
            }
        });
    }

    boolean validateData(String email, String password){
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailEditText.setError("Email is invalid!");
            return false;
        }

        if(password.length() < 6){
            passwordEditText.setError("Password length is invalid!");
            return false;
        }

        return true;
    }
}
