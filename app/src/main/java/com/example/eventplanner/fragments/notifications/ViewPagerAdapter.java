package com.example.eventplanner.fragments.notifications;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.google.android.material.tabs.TabLayout;

public class ViewPagerAdapter extends FragmentStateAdapter {
    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
       switch (position){
           case 0: return new AllNotifsFragment();
           case 1: return new NotReadNotifsFragment();
           case 2: return new ReadNotifsFragment();
           default: return new AllNotifsFragment();
       }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
