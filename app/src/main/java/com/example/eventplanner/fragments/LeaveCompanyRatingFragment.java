package com.example.eventplanner.fragments;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentLeaveCompanyRatingBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.RatingsService;
import com.example.eventplanner.services.UserReportService;
import com.example.eventplanner.services.UserService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class LeaveCompanyRatingFragment extends Fragment {

    private FragmentLeaveCompanyRatingBinding binding;
    private RatingsService ratingsService;
    private Company company;
    private UserService userService;

    private View view;

    private NotificationService notificationService;
    private AuthService authService;

    public LeaveCompanyRatingFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentLeaveCompanyRatingBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        userService = new UserService(requireContext());
        authService = new AuthService(requireContext());
        notificationService = new NotificationService(requireContext());

        ratingsService = new RatingsService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null ){
            company = getArguments().getParcelable("company");
        }

        binding.btnCreate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.etRating.getText().toString().isEmpty()){
                    binding.etRating.setError("Cannot be empty");
                    return;
                }

                if(binding.etComment.getText().toString().isEmpty()){
                    binding.etComment.setError("Cannot be empty");
                    return;
                }

                createRating();
                Navigation.findNavController(view).popBackStack();
            }
        });
    }

    void createRating(){
        if (ratingsService.createCompanyRating(company.getId(), binding.etRating.getText().toString(), binding.etComment.getText().toString())) {
            Toast.makeText(getContext(), "Creation of rating initiated!", Toast.LENGTH_SHORT).show();
            notifyOwner();
        } else {
            Toast.makeText(getContext(), "Failed to initiate creation of rating!", Toast.LENGTH_SHORT).show();
        }
    }

    private void notifyOwner() {
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {

                userService.getUserByCompanyId(company.getId())
                    .addOnSuccessListener(companyOwner -> {
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        notificationService.createNotification(new Notification(
                                null,
                                "New rating",
                                "User " + user.getEmail() + " left a rating for the company "  + LocalDateTime.now().format(formatter),
                                companyOwner.getId(),
                                false
                        ));
                    })
                    .addOnFailureListener(e -> Log.e(TAG, "Error getting company owner: " + e.getMessage()));
        });
    }

}