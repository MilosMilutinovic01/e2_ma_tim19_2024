package com.example.eventplanner.fragments.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EventTypesAdapter;
import com.example.eventplanner.adapters.RegistrationRequestsAdapter;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.async.OnEventTypesLoadedListener;
import com.example.eventplanner.async.OnRegistrationRequestsLoadedListener;
import com.example.eventplanner.databinding.FragmentCompanyWorkTimeBinding;
import com.example.eventplanner.databinding.FragmentRegistrationRequestsBinding;
import com.example.eventplanner.fragments.NewEventTypeFragment;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventTypeService;
import com.example.eventplanner.services.RegistrationRequestService;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RegistrationRequestsFragment extends Fragment implements RecyclerViewInterface {

    private RegistrationRequestsAdapter registrationRequestsAdapter;
    private List<RegistrationRequest> registrationRequests;
    private FragmentRegistrationRequestsBinding binding;
    private View view;
    private SearchView searchView;
    private int dayOfWeek;
    private String date = "";
    private String selectedCategory = "";
    private String selectedEventType = "";


    private RegistrationRequestService registrationRequestService;

    public RegistrationRequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentRegistrationRequestsBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        registrationRequests = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchView = binding.searchView;
        registrationRequestService = new RegistrationRequestService(getContext());
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    registrationRequestsAdapter.setFilteredList(registrationRequests);
                } else {
                    registrationRequestsAdapter.setFilteredList(filterList(newText, selectedCategory, selectedEventType, date));
                }
                return true;
            }
        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Retrieve all registration requests
        registrationRequestService.getAllRegistrationRequests(new OnRegistrationRequestsLoadedListener() {
            @Override
            public void onRequestsLoaded(List<RegistrationRequest> requests) {
                for(RegistrationRequest request: requests){
                    if(request.getApproved() == null){
                        registrationRequests.add(request);
                    }
                }
                registrationRequestsAdapter = new RegistrationRequestsAdapter(getActivity(), registrationRequests, RegistrationRequestsFragment.this);
                binding.recyclerView.setAdapter(registrationRequestsAdapter);
            }

            @Override
            public void onFailure(Exception e) {
                // Handle failure
                Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        binding.etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDialog();
            }
        });

        CategoryService categoryService = new CategoryService(getContext());
        EventTypeService eventTypeService = new EventTypeService(getContext());

        AutoCompleteTextView dropDownCategories = binding.dropDownCategories;
        AutoCompleteTextView dropDownEventTypes = binding.dropDownEventTypes;
        List<String> categoryNames = new ArrayList<>();
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, categoryNames);
        List<String> eventTypeNames = new ArrayList<>();
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, eventTypeNames);

        dropDownCategories.setAdapter(adapter1);
        dropDownEventTypes.setAdapter(adapter2);

        categoryService.getAllCategories(new OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded(List<ProductAndServiceCategory> categories) {
                List<ProductAndServiceCategory> categoriesList = categories;
                categoryNames.clear();

                for (ProductAndServiceCategory category : categories) {
                    categoryNames.add(category.getName());
                }

                adapter1.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting categories: " + e.getMessage());
            }
        });

        eventTypeService.getAllEventTypes(new OnEventTypesLoadedListener() {
            @Override
            public void onEventTypesLoaded(List<EventType> eventTypes) {
                List<EventType> eventTypeList = eventTypes;
                eventTypeNames.clear();

                for (EventType eventType : eventTypes) {
                    eventTypeNames.add(eventType.getName());
                }

                adapter2.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting categories: " + e.getMessage());
            }
        });

        binding.btnApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedCategory = dropDownCategories.getText().toString();
                if(selectedCategory.equals("Categories")){
                    selectedCategory = "";
                }
                selectedEventType = dropDownEventTypes.getText().toString();
                if(selectedEventType.equals("Event type")){
                    selectedEventType = "";
                }
                registrationRequestsAdapter.setFilteredList(filterList("", selectedCategory, selectedEventType, date));
            }
        });

    }

    private List<RegistrationRequest> filterList(String newText, String category, String eventType, String date) {
        List<RegistrationRequest> filteredList = new ArrayList<>();

        // Search by owner name
        filteredList.addAll(registrationRequestService.searchRequests(registrationRequests, newText));

        Log.d("TEST", category + " " + eventType + " " + date);
        List<RegistrationRequest> finalFilteredList = new ArrayList<>();
        for (RegistrationRequest request : filteredList) {
            boolean matchesCategory = category == null || category.equals("") || request.getCategories().contains(category);
            boolean matchesEventType = eventType == null || eventType.equals("") || request.getEventTypes().contains(eventType);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            boolean matchesDate = date == null || date.isEmpty() ||
                    request.getRequestTime().format(formatter).equals(date);

            if (matchesCategory && matchesEventType && matchesDate) {
                finalFilteredList.add(request);
            }
        }

        return finalFilteredList;
    }

    private void openDatePickerDialog() {
        CalendarConstraints.Builder constraintsBuilder = new CalendarConstraints.Builder();
        long currentMillis = MaterialDatePicker.todayInUtcMilliseconds();

        constraintsBuilder.setStart(currentMillis);
        constraintsBuilder.setValidator(DateValidatorPointForward.now());

        MaterialDatePicker picker = MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setCalendarConstraints(constraintsBuilder.build())
                .build();


        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener((w)->{
            Long selectedDate = (Long) picker.getSelection();


            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault());
            String date = dateFormat.format(new Date(selectedDate));

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(selectedDate);

            // Get day of week
            dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            binding.etDate.setText(date);
            this.date = date;
        });
        picker.addOnNegativeButtonClickListener((w)->{
            picker.dismiss();
        });
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onEditClick(int position) {
        RegistrationRequest selectedRequest = registrationRequests.get(position);
        registrationRequestService.approveRequest(selectedRequest.getRequestId());
        registrationRequests.remove(position);
        registrationRequestsAdapter.notifyItemRemoved(position);
        Toast.makeText(getContext(), "Request aproved", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {
        RegistrationRequest selectedRequest = registrationRequests.get(position);
        registrationRequestService.rejectRequest(selectedRequest.getRequestId(), "Jer mi se moze");
        registrationRequests.remove(position);
        registrationRequestsAdapter.notifyItemRemoved(position);
        Toast.makeText(getContext(), "Request rejected", Toast.LENGTH_SHORT).show();
    }
}