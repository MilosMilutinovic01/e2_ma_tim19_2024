package com.example.eventplanner.fragments.owner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.adapters.RatingsAdapter;
import com.example.eventplanner.async.OnRatingsLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentRatingBinding;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.RatingsService;
import com.example.eventplanner.services.UserService;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RatingFragment extends Fragment implements RecyclerViewInterface {

    private FragmentRatingBinding binding;
    private RatingsAdapter ratingsAdapter;
    private List<CompanyRating> ratings;
    private RatingsService ratingsService;
    private AuthService authService;
    private User owner;
    private View view;
    private String date = "";
    public RatingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentRatingBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        ratingsService = new RatingsService(requireContext());
        authService = new AuthService(requireContext());
        ratings = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ratingsAdapter = new RatingsAdapter(getActivity(), ratings, RatingFragment.this, authService);
        binding.recyclerView.setAdapter(ratingsAdapter);


        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    owner = user;

                    ratingsService.getAllRatings(owner.getCompanyId(), new OnRatingsLoadedListener() {
                        @Override
                        public void onRatingsLoaded(List<CompanyRating> companyRatings) {
                            ratings.clear();
                            ratings.addAll(companyRatings);
                            ratingsAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e("TAG", "Error getting users: " + e.getMessage());
                        }
                    });
                });

        binding.etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDialog();
            }
        });

        binding.btnApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingsAdapter.setFilteredList(filterList(date));
            }
        });
    }

    private List<CompanyRating> filterList(String date) {
        List<CompanyRating> filteredList = new ArrayList<>();

        // Search by owner name
        filteredList.addAll(ratings);

        List<CompanyRating> finalFilteredList = new ArrayList<>();
        for (CompanyRating rating : filteredList) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            boolean matchesDate = date == null || date.isEmpty() ||
                    rating.getDate().format(formatter).equals(date);

            if (matchesDate) {
                finalFilteredList.add(rating);
            }
        }

        return finalFilteredList;
    }

    private void openDatePickerDialog() {
        CalendarConstraints.Builder constraintsBuilder = new CalendarConstraints.Builder();
        long currentMillis = MaterialDatePicker.todayInUtcMilliseconds();

        MaterialDatePicker picker = MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setCalendarConstraints(constraintsBuilder.build())
                .build();


        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener((w)->{
            Long selectedDate = (Long) picker.getSelection();


            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault());
            String date = dateFormat.format(new Date(selectedDate));

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(selectedDate);

            // Get day of week
            //dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            binding.etDate.setText(date);
            this.date = date;
        });
        picker.addOnNegativeButtonClickListener((w)->{
            picker.dismiss();
        });
    }

    @Override
    public void onItemClick(int position) {
        CompanyRating rating = ratings.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("rating", rating);
        Navigation.findNavController(view).navigate(R.id.navigateToReportRating, bundle);
    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}