package com.example.eventplanner.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.eventplanner.R;
import com.example.eventplanner.async.OnEmployerEventsLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.UnavailableHours;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.EmployerEventService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.ServiceReservationService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BookingCalendarFragment extends DialogFragment {
    private String eventDate = "";
    private String eventId = "";
    private String serviceId = "";
    private String eventOrganizerId = "";
    private Event event;
    private Service selectedService;
    private User selectedEmployee;

    public BookingCalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_booking_calendar, null);

        List<User> availableWorkers = new ArrayList<>();
        UserService userService = new UserService(requireContext());
        AuthService authService = new AuthService(requireContext());
        EmployerEventService employerEventService = new EmployerEventService(requireContext());
        ServiceService serviceService = new ServiceService(requireContext());

        Bundle args = getArguments();
        if (args != null) {
            eventDate = args.getString("eventDate");
            eventId = args.getString("eventName");
            serviceId = args.getString("serviceId");
            eventOrganizerId = args.getString("eventOrganizerId");
        }

        AutoCompleteTextView spinnerEmployees = view.findViewById(R.id.dropDownEmployees);
        AutoCompleteTextView spinnerAvailableTimes = view.findViewById(R.id.dropDownAvailableTimes);
        Button bookButton = view.findViewById(R.id.button_book);

        // Load all users
        userService.getAllEmployers(new OnUsersLoadedListener() {
            @Override
            public void onUsersLoaded(List<User> users) {
                List<String> workerNames = new ArrayList<>();
                for (User user : users) {
                    if (user.getRole().equals(Role.EMPLOYER)) {
                        availableWorkers.add(user);
                        workerNames.add(user.getName());
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_dropdown_item_1line, workerNames);
                spinnerEmployees.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
                // Handle error
            }
        });

        // Load the event
        EventService eventService = new EventService(requireContext());
        eventService.getAllEvents(new EventService.OnEventsLoadedListener() {
            @Override
            public void onEventsLoaded(List<Event> events) {
                for (Event event1 : events) {
                    if (event1.getEventName().equals(eventId)) {
                        event = event1;
                    }
                }
            }

            @Override
            public void onFailure(Exception e) {
                // Handle error
            }
        });

        // Load the selected service
        serviceService.getAllServices(new OnServicesLoadedListener() {
            @Override
            public void onServicesLoaded(List<Service> services) {
                for (Service service : services) {
                    if (service.getId().equals(serviceId)) {
                        selectedService = service;
                    }
                }
            }

            @Override
            public void onFailure(Exception e) {
                // Handle error
            }
        });

        // Handle employee selection
        spinnerEmployees.setOnItemClickListener((parent, view1, position, id) -> {
            String selectedEmployeeName = (String) parent.getItemAtPosition(position);
            for (User employee : availableWorkers) {
                if (employee.getName().equals(selectedEmployeeName)) {
                    selectedEmployee = employee;
                    loadEmployeeEventsAndCalculateAvailableTimes(selectedEmployee, spinnerAvailableTimes);
                    break;
                }
            }
        });

        AlertDialog alertDialog = new AlertDialog.Builder(requireContext())
                .setView(view)
                .setTitle("Book Service")
                .create();

        alertDialog.setOnShowListener(dialogInterface -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
                // Retrieve text from EditText views when the OK button is clicked
                // Handle booking logic
                alertDialog.dismiss();
            });

            bookButton.setOnClickListener(v -> {
                // Handle booking button click
                // Handle booking logic
                if (selectedEmployee != null && selectedService != null) {
                    String selectedTime = spinnerAvailableTimes.getText().toString();

                    if (!selectedTime.isEmpty()) {
                        LocalDateTime selectedDateTime = LocalDateTime.parse(selectedTime, DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss"));

                        // Create a new ServiceReservation object
                        ServiceReservation reservation = new ServiceReservation(
                                null,
                                eventId,
                                selectedService.getName(),
                                selectedService.getDescription(),
                                selectedEmployee.getId(),
                                event.getEventDate(),
                                selectedDateTime, // Use selected time for service start time
                                selectedDateTime.plusMinutes(selectedService.getDuration() * 60), // Calculate end time based on service duration
                                false,// Notification status initially set to false
                                false,
                                eventOrganizerId
                        );

                        EmployerEvent employerEvent = new EmployerEvent(
                                selectedService.getName(), // Name of the event can be the service name
                                event.getEventDate().toLocalDate(), // Date of the event is the same as the event date
                                selectedDateTime.toLocalTime(), // Start time is the selected time
                                selectedDateTime.plusMinutes(selectedService.getDuration() * 60).toLocalTime(), // End time is calculated end time of the service
                                selectedEmployee.getId() // Employer ID
                        );

                        employerEventService.createEmployerEvent(employerEvent);


                        // Save the reservation in the database
                        ServiceReservationService reservationService = new ServiceReservationService(requireContext());
                        reservationService.saveServiceReservation(reservation);
                        NotificationService notificationService = new NotificationService(requireContext());
                        notificationService.createNotification(new Notification(null,
                                "New service reservation",
                                "New service reservation has been made by a client for the " + event.getEventName(),
                                selectedEmployee.getId(),
                                false));

                        alertDialog.dismiss();
                    } else {
                        // Handle case when no time is selected
                        // Show appropriate error message or dialog
                    }
                } else {
                    // Handle case when either employee or service is not selected
                    // Show appropriate error message or dialog
                }
                alertDialog.dismiss();
            });
        });

        return alertDialog;
    }

    public static BookingCalendarFragment newInstance(Service service, Event event, String eventOrganizerId) {
        BookingCalendarFragment fragment = new BookingCalendarFragment();
        Bundle args = new Bundle();
        args.putString("serviceId", service.getId().toString());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
        args.putString("eventDate", event.getEventDate().format(formatter));
        args.putString("eventName", event.getEventName());
        args.putString("eventOrganizerId", eventOrganizerId);
        fragment.setArguments(args);
        return fragment;
    }

    private void loadEmployeeEventsAndCalculateAvailableTimes(User employee, AutoCompleteTextView spinnerAvailableTimes) {
        EmployerEventService employerEventService = new EmployerEventService(requireContext());
        employerEventService.getAll(new OnEmployerEventsLoadedListener() {
            @Override
            public void onEventsLoaded(List<EmployerEvent> events) {
                List<EmployerEvent> employerEvents = new ArrayList<>();
                for (EmployerEvent employerEvent : events) {
                    if (employerEvent.getEmployerId().equals(employee.getId())) {
                        employerEvents.add(employerEvent);
                    }
                }
                employee.setEvents(employerEvents);

                List<LocalDateTime> availableTimes = getAvailableDates(event, employee, selectedService);
                List<String> availableTimesString = new ArrayList<>();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
                for (LocalDateTime dateTime : availableTimes) {
                    availableTimesString.add(dateTime.format(formatter));
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_dropdown_item_1line, availableTimesString);
                spinnerAvailableTimes.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
                // Handle error
            }
        });
    }

    private List<LocalDateTime> getAvailableDates(Event event, User employee, Service service) {
        List<LocalDateTime> availableTimes = new ArrayList<>();
        LocalDateTime eventDate = event.getEventDate();

        List<UnavailableHours> unavailableHoursList = employee.calculateUnavailableHours();

        for (WorkingDay workingDay : employee.getWorkingDays()) {
            if (eventDate.getDayOfWeek().equals(workingDay.getDayOfWeek())) {
                LocalTime startTime = workingDay.getStartTime();
                LocalTime endTime = workingDay.getEndTime();

                while (!startTime.plusMinutes(service.getDuration()*60).isAfter(endTime)) {
                    LocalDateTime potentialStart = eventDate.withHour(startTime.getHour()).withMinute(startTime.getMinute());
                    LocalDateTime potentialEnd = potentialStart.plusMinutes(service.getDuration()*60);
                    boolean isAvailable = true;

                    for (UnavailableHours unavailableHour : unavailableHoursList) {
                        LocalDateTime unavailableFrom = unavailableHour.getDateFrom();
                        LocalDateTime unavailableTo = unavailableHour.getDateTo();

                        if ((potentialStart.isAfter(unavailableFrom) || potentialStart.isEqual(unavailableFrom)) && potentialStart.isBefore(unavailableTo) ||
                                (potentialEnd.isAfter(unavailableFrom) || potentialEnd.isEqual(unavailableFrom)) && potentialEnd.isBefore(unavailableTo)) {
                            isAvailable = false;
                            break;
                        }
                    }

                    if (isAvailable) {
                        availableTimes.add(potentialStart);
                    }

                    startTime = startTime.plusMinutes(service.getDuration()*60);
                }
            }
        }

        return availableTimes;
    }
}
