package com.example.eventplanner.fragments.employer;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.async.OnEmployerEventsLoadedListener;
import com.example.eventplanner.databinding.FragmentAddCalendarEventBinding;
import com.example.eventplanner.databinding.FragmentAddEmployerCalendarEventBinding;
import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.EmployerEventService;
import com.example.eventplanner.services.NotificationService;
import com.google.android.material.textfield.TextInputEditText;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class AddEmployerCalendarEventFragment extends Fragment {

    private FragmentAddEmployerCalendarEventBinding binding;
    private EmployerEventService service;
    private TimePickerDialog picker;
    private List<EmployerEvent> events;
    private String date;
    private EmployerEvent event;
    private User employer;

    private LocalTime startTime;
    private LocalTime endTime;
    private DayOfWeek dayOfWeek;
    private boolean isValid;

    private NotificationService notifService;

    public AddEmployerCalendarEventFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAddEmployerCalendarEventBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        service = new EmployerEventService(requireContext());
        notifService = new NotificationService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if(getArguments() != null){
            date = getArguments().getString("date");
            binding.date.setText(date);

            int day = getArguments().getInt("dayOfWeek");
            dayOfWeek = getDayOfWeekString(day);

            employer = getArguments().getParcelable("employer");
        }

        binding.etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etTime);
            }
        });

        binding.etT0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etT0);
            }
        });

        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = LocalTime.parse(binding.etTime.getText().toString());
                endTime = LocalTime.parse(binding.etT0.getText().toString());
                getEmployerEvents();
                if(isValid) {
                    event = new EmployerEvent(binding.etName.getText().toString(),
                            LocalDate.parse(binding.date.getText().toString(), DateTimeFormatter.ofPattern("dd.MM.yyyy.")),
                            startTime,
                            endTime,
                            employer.getId().toString());

                    if (addEvent()) {
                        Navigation.findNavController(v).popBackStack();
                    }
                }
            }
        });
    }

    private void openTimePicker(TextInputEditText et){

        final Calendar cldr = Calendar.getInstance();
        int hour = cldr.get(Calendar.HOUR_OF_DAY);
        int minutes = cldr.get(Calendar.MINUTE);

        picker = new TimePickerDialog(getActivity(),
                (tp, sHour, sMinute) ->{
                    String formattedTime = String.format(Locale.getDefault(), "%02d:%02d", sHour, sMinute);
                    et.setText(formattedTime);
                }
                , hour, minutes, true);
        picker.show();
    }

    private void validateEvent(){
        isValid = true;
        for(EmployerEvent event : events){
            if (event.getStartTime().isAfter(startTime) && event.getStartTime().isBefore(endTime)
                    || event.getEndTime().isAfter(startTime) && event.getEndTime().isBefore(endTime)
                    || startTime.isAfter(event.getStartTime()) && startTime.isBefore(event.getEndTime())
                    || endTime.isAfter(event.getStartTime()) && endTime.isBefore(event.getEndTime())){
                Toast.makeText(getContext(), "The event in that time already exists!", Toast.LENGTH_SHORT).show();
                isValid = false;
                break;
            }
        }

        if(employer.getWorkingDays().stream()
                .noneMatch(d -> d.getDayOfWeek().equals(dayOfWeek))){
            Toast.makeText(getContext(), "Employer is not working then!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        else {
            WorkingDay workingDay = employer.getWorkingDays()
                    .stream()
                    .filter(d -> d.getDayOfWeek().equals(dayOfWeek))
                    .findFirst()
                    .get();


            if (startTime.isAfter(workingDay.getEndTime())
                    || startTime.isBefore(workingDay.getStartTime())
                    || endTime.isAfter(workingDay.getEndTime())
                    || endTime.isBefore(workingDay.getStartTime())) {
                Toast.makeText(getContext(), "Employer is not working then!", Toast.LENGTH_SHORT).show();
                isValid = false;
            }
        }
    }

    private DayOfWeek getDayOfWeekString(int dayOfWeek) {
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                return DayOfWeek.SUNDAY;
            case Calendar.MONDAY:
                return DayOfWeek.MONDAY;
            case Calendar.TUESDAY:
                return DayOfWeek.TUESDAY;
            case Calendar.WEDNESDAY:
                return DayOfWeek.WEDNESDAY;
            case Calendar.THURSDAY:
                return DayOfWeek.THURSDAY;
            case Calendar.FRIDAY:
                return DayOfWeek.FRIDAY;
            default:
                return DayOfWeek.SATURDAY;
        }
    }

    private void getEmployerEvents(){
        events = new ArrayList<EmployerEvent>();
        service.getAllEmployerEvents(employer.getId(), date, new OnEmployerEventsLoadedListener() {
            @Override
            public void onEventsLoaded(List<EmployerEvent> employerEvents) {
                events = employerEvents;
                validateEvent();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting events: " + e.getMessage());
            }
        });
    }
    private boolean addEvent(){
        if (service.createEmployerEvent(event)) {
            Toast.makeText(getContext(), "Event created!", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(getContext(), "Failed to create event!", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}