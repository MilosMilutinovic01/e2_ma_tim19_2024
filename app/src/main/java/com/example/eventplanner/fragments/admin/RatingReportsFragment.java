package com.example.eventplanner.fragments.admin;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.RatingReportsAdapter;
import com.example.eventplanner.adapters.RatingsAdapter;
import com.example.eventplanner.async.OnRatingsLoadedListener;
import com.example.eventplanner.async.OnReportEditedListener;
import com.example.eventplanner.async.OnReportsLoadedListener;
import com.example.eventplanner.databinding.FragmentRatingReportsBinding;
import com.example.eventplanner.fragments.owner.RatingFragment;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.RatingReport;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.RatingsService;
import com.example.eventplanner.services.UserService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class RatingReportsFragment extends Fragment implements RecyclerViewInterface {

    private FragmentRatingReportsBinding binding;

    private RatingReportsAdapter reportsAdapter;
    private List<RatingReport> reports;
    private RatingsService ratingsService;
    private UserService userService;
    private NotificationService notificationService;
    private View view;

    public RatingReportsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentRatingReportsBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        ratingsService = new RatingsService(requireContext());
        userService = new UserService(requireContext());
        notificationService = new NotificationService(requireContext());
        reports = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        reportsAdapter = new RatingReportsAdapter(getActivity(), reports, RatingReportsFragment.this,
                userService, notificationService);
        binding.recyclerView.setAdapter(reportsAdapter);
        getReports();

    }

    private void getReports(){
        ratingsService.getAllReports(new OnReportsLoadedListener() {
            @Override
            public void onReportsLoaded(List<RatingReport> ratingReports) {
                reports.clear();
                reports.addAll(ratingReports);
                reportsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });
    }

    private void updateReport(RatingReport report){
        ratingsService.updateReport(report, new OnReportEditedListener() {
            @Override
            public void onReportEdited(RatingReport ratingReport) {
                getReports();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error editing report: " + e.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        RatingReport report = reports.get(position);

        userService.getUserById(report.getReporterId())
                .addOnSuccessListener(user -> {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("user", user);
                    Navigation.findNavController(this.view).navigate(R.id.navigateToUserProfile, bundle);
                })
                .addOnFailureListener(e -> Log.e(TAG, "Error getting user: " + e.getMessage()));

    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {
        RatingReport report = reports.get(position);
        if (ratingsService.deleteRating(report.getRatingId())) {
            Toast.makeText(getContext(), "Deletion of rating initiated!", Toast.LENGTH_SHORT).show();
            report.setStatus(RatingReport.Status.ACCEPTED);
            updateReport(report);
        } else {
            Toast.makeText(getContext(), "Failed to delete rating!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBlockClick(int position) {
        RatingReport report = reports.get(position);
        report.setStatus(RatingReport.Status.REJECTED);
        updateReport(report);

        Bundle bundle = new Bundle();
        bundle.putParcelable("report", report);
        Navigation.findNavController(this.view).navigate(R.id.navigateToRejectReportFragment, bundle);
    }
}