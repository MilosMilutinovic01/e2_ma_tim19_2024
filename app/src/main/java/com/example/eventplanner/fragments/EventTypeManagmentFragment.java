package com.example.eventplanner.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EventTypesAdapter;
import com.example.eventplanner.adapters.ServiceAndProductCategoryAdapter;
import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.EventTypeUploadCallback;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.async.OnEventTypesLoadedListener;
import com.example.eventplanner.databinding.FragmentEventTypeManagmentBinding;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventTypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventTypeManagmentFragment extends Fragment implements RecyclerViewInterface {

    private EventTypesAdapter eventTypesAdapter;
    private List<EventType> eventTYpesList;
    private FragmentEventTypeManagmentBinding binding;
    private View view;
    private EventTypeService eventTypeService;

    public EventTypeManagmentFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentEventTypeManagmentBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        eventTypeService = new EventTypeService(getContext());
        eventTYpesList = new ArrayList<>();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getEventTypes();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        eventTypesAdapter = new EventTypesAdapter(getActivity(), eventTYpesList, this);
        binding.recyclerView.setAdapter(eventTypesAdapter);

        binding.btnNewEventType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewEventTypeFragment editFragment = NewEventTypeFragment.newInstance("", "", "", false);

                // Show the fragment for editing
                editFragment.show(getChildFragmentManager(), "New event type");
            }
        });
    }

    private void getEventTypes(){
        eventTypeService.getAllEventTypes(new OnEventTypesLoadedListener() {
            @Override
            public void onEventTypesLoaded(List<EventType> eventTypes) {
                eventTYpesList.clear();
                eventTYpesList.addAll(eventTypes);
                eventTypesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting event types: " + e.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onEditClick(int position) {
        EventType selectedEventType = eventTYpesList.get(position);
        NewEventTypeFragment editFragment = NewEventTypeFragment.newInstance(selectedEventType.getName(), selectedEventType.getDescription(), selectedEventType.getId(), selectedEventType.isBlocked());

        // Show the fragment for editing
        editFragment.show(getChildFragmentManager(), "Edit event type");
    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {
        EventType selectedEventType = eventTYpesList.get(position);
        if(selectedEventType.isBlocked()){
            Toast.makeText(getContext(), "Event type already blocked", Toast.LENGTH_SHORT).show();
        }else{
            selectedEventType.setBlocked(true);
            String eventTypeIdToBlock = selectedEventType.getId();
            eventTypeService.updateEventType(selectedEventType, new EventTypeUploadCallback() {
                @Override
                public void onSuccess() {
                    // Handle success
                    Toast.makeText(getContext(), "Event type blocked successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(String errorMessage) {
                    // Handle failure
                    Toast.makeText(getContext(), "Failed to block event type: " + errorMessage, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}