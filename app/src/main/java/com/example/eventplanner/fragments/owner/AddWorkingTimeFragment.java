package com.example.eventplanner.fragments.owner;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.async.CompanyLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentAddWorkingTimeBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.CompanyService;
import com.google.android.material.textfield.TextInputEditText;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AddWorkingTimeFragment extends Fragment {
    private FragmentAddWorkingTimeBinding binding;
    TimePickerDialog picker;
    private AuthService authService;
    private CompanyService companyService;
    private Bitmap image;
    User employer;
    private User owner;
    public AddWorkingTimeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAddWorkingTimeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());
        companyService = new CompanyService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        employer = new User();
        owner = new User();
        if(getArguments() != null){
            employer = getArguments().getParcelable("employer");
            image = getArguments().getParcelable("image");
            owner = getArguments().getParcelable("owner");
        }

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateData()) {
                    setWorkingTime();
                    if (registerUser(v)) {
                        //Navigation.findNavController(view).popBackStack();
                        //Navigation.findNavController(view).popBackStack();
                        Navigation.findNavController(view).navigate(R.id.navigateToOwnerHomeView);
                    }
                }
            }
        });

        binding.btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCompanyWorkingTime();
                if(registerUser(v)) {
                    Navigation.findNavController(view).navigate(R.id.navigateToOwnerHomeView);
                }
            }
        });


        binding.etMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etMon);
            }
        });

        binding.etMonEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etMonEnd);
            }
        });

        binding.etTue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etTue);
            }
        });

        binding.etTueEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etTueEnd);
            }
        });

        binding.etWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etWed);
            }
        });

        binding.etWedEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etWedEnd);
            }
        });

        binding.etThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etThu);
            }
        });

        binding.etThuEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etThuEnd);
            }
        });

        binding.etFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etFri);
            }
        });

        binding.etFriEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etFriEnd);
            }
        });

        binding.etSat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etSat);
            }
        });
        binding.etSatEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etSatEnd);
            }
        });

        binding.etSun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etSun);
            }
        });

        binding.etSunEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(binding.etSunEnd);
            }
        });
    }

    private void setWorkingTime(){
        List<WorkingDay> workingDays = new ArrayList<WorkingDay>();
        if(!binding.etMon.getText().toString().isEmpty() && !binding.etMonEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.MONDAY, LocalTime.parse(binding.etMon.getText().toString()), LocalTime.parse(binding.etMonEnd.getText().toString())));
        if(!binding.etTue.getText().toString().isEmpty() && !binding.etTueEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.TUESDAY, LocalTime.parse(binding.etTue.getText().toString()), LocalTime.parse(binding.etTueEnd.getText().toString())));
        if(!binding.etWed.getText().toString().isEmpty() && !binding.etWedEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.WEDNESDAY, LocalTime.parse(binding.etWed.getText().toString()), LocalTime.parse(binding.etWedEnd.getText().toString())));
        if(!binding.etThu.getText().toString().isEmpty() && !binding.etThuEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.THURSDAY, LocalTime.parse(binding.etThu.getText().toString()), LocalTime.parse(binding.etThuEnd.getText().toString())));
        if(!binding.etFri.getText().toString().isEmpty() && !binding.etFriEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.FRIDAY, LocalTime.parse(binding.etFri.getText().toString()), LocalTime.parse(binding.etFriEnd.getText().toString())));
        if(!binding.etSat.getText().toString().isEmpty() && !binding.etSatEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.SATURDAY, LocalTime.parse(binding.etSat.getText().toString()), LocalTime.parse(binding.etSatEnd.getText().toString())));
        if(!binding.etSun.getText().toString().isEmpty() && !binding.etSunEnd.getText().toString().isEmpty())
            workingDays.add(new WorkingDay(DayOfWeek.SUNDAY, LocalTime.parse(binding.etSun.getText().toString()), LocalTime.parse(binding.etSunEnd.getText().toString())));

        employer.setWorkingDays(workingDays);
    }

    private void setCompanyWorkingTime(){
        companyService.getCompanyById(owner.getCompanyId(), new CompanyLoadedListener() {
            @Override
            public void onCompanyLoaded(Company company) {
                employer.setWorkingDays(company.getWorkingDays());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting company: " + e.getMessage());
            }
        });
    }

    boolean validateData(){
        if(!binding.etMonEnd.getText().toString().isEmpty()
        && binding.etMon.getText().toString().isEmpty()){
            binding.etMon.setError("Cannot be empty");
            return false;
        }

        if(!binding.etMon.getText().toString().isEmpty() &&
                binding.etMonEnd.getText().toString().isEmpty()){
            binding.etMonEnd.setError("Cannot be empty");
            return false;
        }

        if(!binding.etTueEnd.getText().toString().isEmpty()
        && binding.etTue.getText().toString().isEmpty()){
            binding.etTue.setError("Cannot be empty");
            return false;
        }

        if(!binding.etTue.getText().toString().isEmpty()
        && binding.etTueEnd.getText().toString().isEmpty()){
            binding.etTueEnd.setError("Cannot be empty");
            return false;
        }
        if(!binding.etWedEnd.getText().toString().isEmpty()
        && binding.etWed.getText().toString().isEmpty()){
            binding.etWed.setError("Cannot be empty");
            return false;
        }

        if(!binding.etWed.getText().toString().isEmpty()
        && binding.etWedEnd.getText().toString().isEmpty()){
            binding.etWedEnd.setError("Cannot be empty");
            return false;
        }
        if(!binding.etThuEnd.getText().toString().isEmpty()
        && binding.etThu.getText().toString().isEmpty()){
            binding.etThu.setError("Cannot be empty");
            return false;
        }

        if(!binding.etThu.getText().toString().isEmpty()
        && binding.etThuEnd.getText().toString().isEmpty()){
            binding.etThuEnd.setError("Cannot be empty");
            return false;
        }

        if(!binding.etFriEnd.getText().toString().isEmpty()
        && binding.etFri.getText().toString().isEmpty()){
            binding.etFri.setError("Cannot be empty");
            return false;
        }

        if(!binding.etFri.getText().toString().isEmpty() &&
                binding.etFriEnd.getText().toString().isEmpty()){
            binding.etFriEnd.setError("Cannot be empty");
            return false;
        }

        if(!binding.etSatEnd.getText().toString().isEmpty()
        && binding.etSat.getText().toString().isEmpty()){
            binding.etSat.setError("Cannot be empty");
            return false;
        }


        if(!binding.etSat.getText().toString().isEmpty() &&
                binding.etSatEnd.getText().toString().isEmpty()){
            binding.etSatEnd.setError("Cannot be empty");
            return false;
        }

        if(!binding.etSunEnd.getText().toString().isEmpty()
        && binding.etSun.getText().toString().isEmpty()){
            binding.etSun.setError("Cannot be empty");
            return false;
        }

        if(!binding.etSun.getText().toString().isEmpty() &&
                binding.etSunEnd.getText().toString().isEmpty()){
            binding.etSunEnd.setError("Cannot be empty");
            return false;
        }

        return true;
    }

    private void openTimePicker(TextInputEditText et){

        final Calendar cldr = Calendar.getInstance();
        int hour = cldr.get(Calendar.HOUR_OF_DAY);
        int minutes = cldr.get(Calendar.MINUTE);

        picker = new TimePickerDialog(getActivity(),
                (tp, sHour, sMinute) ->{
                    String formattedTime = String.format(Locale.getDefault(), "%02d:%02d", sHour, sMinute);
                    et.setText(formattedTime);
                }
                , hour, minutes, true);
        picker.show();
    }

    boolean registerUser(View view){
        if (authService.registerEmployer(employer, image)) {
            Toast.makeText(getContext(), "Registration initiated!", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(getContext(), "Failed to initiate registration!", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}