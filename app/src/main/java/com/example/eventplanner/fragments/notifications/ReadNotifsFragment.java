package com.example.eventplanner.fragments.notifications;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.NotificationAdapter;
import com.example.eventplanner.async.OnNotificationsLoadedListener;
import com.example.eventplanner.databinding.FragmentReadNotifsBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;

import java.util.ArrayList;
import java.util.List;

public class ReadNotifsFragment extends Fragment implements RecyclerViewInterface {
    private FragmentReadNotifsBinding binding;
    private NotificationAdapter notificationAdapter;
    private List<Notification> readNotifications = new ArrayList<>();
    private NotificationService notificationService;
    private AuthService authService;
    private User currentUser;
    private View view;

    public ReadNotifsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentReadNotifsBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        authService = new AuthService(requireContext());
        notificationService = new NotificationService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getNotifs();
    }

    public void getNotifs(){
        authService.getCurrentUser()
                .addOnSuccessListener(currentUser -> {
                    User user = currentUser;
                    notificationService.getReadNotificationsByReceiverId(new OnNotificationsLoadedListener() {
                        @Override
                        public void onNotificationsLoaded(List<Notification> uNotifications) {
                            readNotifications = (ArrayList<Notification>) uNotifications;
                            binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            notificationAdapter = new NotificationAdapter(getActivity(), readNotifications, ReadNotifsFragment.this);
                            binding.recyclerView.setAdapter(notificationAdapter);
                        }
                        @Override
                        public void onFailure(Exception e) {
                            Log.e("TAG", "Error getting services: " + e.getMessage());
                        }
                    },currentUser.getId());
                })
                .addOnFailureListener(e -> Log.e(TAG, "Error getting current user: " + e.getMessage()));
    }

    @Override
    public void onResume() {
        super.onResume();
        // Refresh the data when the fragment is resumed
        getNotifs();
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}