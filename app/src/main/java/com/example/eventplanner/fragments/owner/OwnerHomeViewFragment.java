package com.example.eventplanner.fragments.owner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.async.OnUserLoadedListener;
import com.example.eventplanner.databinding.FragmentOwnerHomeViewBinding;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.UserService;
import com.example.eventplanner.async.OnUsersLoadedListener;

import java.util.List;

public class OwnerHomeViewFragment extends Fragment implements RecyclerViewInterface {
    private EmployersAdapter employersAdapter;
    private List<User> employers;
    private FragmentOwnerHomeViewBinding binding;
    private View view;
    private User owner;
    private UserService userService;


    public OwnerHomeViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentOwnerHomeViewBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.view = view;
        owner = new User();

        userService = new UserService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null && !getArguments().containsKey("owner")){
            owner = getArguments().getParcelable("user");
        }

        userService.getAllEmployers(new OnUsersLoadedListener() {
            @Override
            public void onUsersLoaded(List<User> users) {
                employers = users;
                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                employersAdapter = new EmployersAdapter(getActivity(), employers, OwnerHomeViewFragment.this, userService);
                binding.recyclerView.setAdapter(employersAdapter);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });


        binding.btnRegister.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("owner", owner);
                Navigation.findNavController(view).navigate(R.id.navigateToEmployerRegistration, bundle);
            }
        });

        binding.btnRatings.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("owner", owner);
                Navigation.findNavController(view).navigate(R.id.navigateToRatingFragment, bundle);
            }
        });

        binding.btnChats.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToChatsFragment);
            }
        });

    }

    @Override
    public void onItemClick(int position) {
        User employer = employers.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("employer", employer);
        Navigation.findNavController(this.view).navigate(R.id.navigateToEmployerDetails, bundle);
    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}