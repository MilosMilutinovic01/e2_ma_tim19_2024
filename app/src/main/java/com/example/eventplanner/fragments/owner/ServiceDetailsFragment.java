package com.example.eventplanner.fragments.owner;

import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.example.eventplanner.R;
import com.example.eventplanner.async.CompanyLoadedListener;
import com.example.eventplanner.databinding.FragmentProductDetailsBinding;
import com.example.eventplanner.databinding.FragmentServiceDetailsBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.fragments.BookingCalendarFragment;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.squareup.picasso.Picasso;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ServiceDetailsFragment extends Fragment {

    private FragmentServiceDetailsBinding binding;
    private Service service;
    private ServiceService serviceService;
    private EventService eventService;
    private AuthService authService;
    private CompanyService companyService;
    public ServiceDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentServiceDetailsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        serviceService = new ServiceService(requireContext());
        authService = new AuthService(requireContext());
        companyService = new CompanyService(requireContext());
        eventService = new EventService(requireContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //eventService.createMockedEventsInDatabase();
        if (getArguments() != null) {
            this.service = getArguments().getParcelable("selectedService");
            if (getArguments() != null) {
                bindServiceDetails(this.service);
            }
        }

        //binding.spinnerService = eventService.
        AutoCompleteTextView eventsDropDown = binding.dropDownEvents;
        List<String> eventNames = new ArrayList<>();
        List<Event> allEvents = new ArrayList<>();
        eventService.getAllEvents(new EventService.OnEventsLoadedListener() {
            @Override
            public void onEventsLoaded(List<Event> events) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
                for(Event event: events){
                    if(LocalDateTime.parse(service.getBookingDeadline(),formatter).isAfter(event.getEventDate())){
                        eventNames.add(event.getEventName());
                        allEvents.add(event);
                    }
                }
            }

            @Override
            public void onFailure(Exception e) {

            }
        });
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, eventNames);
        eventsDropDown.setAdapter(adapter1);

        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if (user.getRole().equals(Role.OWNER)) {
                        binding.btnEditService.setVisibility(View.VISIBLE);
                        binding.btnDeleteService.setVisibility(View.VISIBLE);
                        binding.tvVisibility.setVisibility(View.VISIBLE);
                    } else if (user.getRole().equals(Role.EMPLOYER)){
                        binding.tvVisibility.setVisibility(View.VISIBLE);
                    } else if (user.getRole().equals(Role.EVENT_ORGANIZER)){
                        if(service.isAvailable()){
                            binding.btnBookService.setVisibility(View.VISIBLE);
                        }
                        binding.btnViewCompany.setVisibility(View.VISIBLE);
                    }
                });
        binding.btnEditService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("service",service);
                Navigation.findNavController(view).navigate(R.id.navigateToEditService,bundle);
            }
        });
        binding.btnDeleteService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceService.deleteService(service.getId());
                Navigation.findNavController(view).navigate(R.id.navigateToServices);
            }
        });

        binding.btnViewCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                companyService.getCompanyById(service.getCompanyId(), new CompanyLoadedListener() {
                    @Override
                    public void onCompanyLoaded(Company company) {
                        bundle.putParcelable("company",company);
                        Navigation.findNavController(view).navigate(R.id.navigateToCompanyProfile, bundle);
                    }
                    @Override
                    public void onFailure(Exception e) {
                        Log.e("TAG", "Error getting company: " + e.getMessage());
                    }
                });
            }
        });


        binding.btnBookService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBookingCalendarDialog(service, allEvents);
            }
        });
    }
    private void bindServiceDetails(Service service) {
        binding.tvName.setText(service.getName());
        binding.tvDescription.setText(service.getDescription());
        binding.tvOldPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", service.getPrice()));
        String durationString = String.format(Locale.getDefault(), "Duration: %d hours", service.getDuration());
        binding.tvDuration.setText(durationString);
        binding.tvSpecificities.setText(service.getSpecificities());
        binding.tvCancellationDeadline.setText("Cancellation deadline :" + service.getCancellationDeadline());
        binding.tvReservationDeadline.setText("Reservation deadline: " + service.getBookingDeadline());
        binding.tvSpecificities.setText(service.getSpecificities());
        if (service.getDiscount() != 0.0) {
            double newPrice = service.getPrice() - (service.getPrice() * (service.getDiscount() / 100));
            binding.tvNewPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
            binding.tvOldPrice.setTextColor(ContextCompat.getColor(getContext(), android.R.color.darker_gray));
            binding.tvOldPrice.setPaintFlags(binding.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.tvNewPrice.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        } else {
            binding.tvNewPrice.setVisibility(View.GONE);
        }

        binding.tvCategory.setText(service.getCategory().getName() + ", " + service.getSubcategory().getName());

        StringBuilder eventTypesText = new StringBuilder();
        for (EventType eventType : service.getEventTypes()) {
            eventTypesText.append(eventType.getName()).append(", ");
        }
        eventTypesText.setLength(eventTypesText.length() - 2);
        binding.tvEventTypes.setText(eventTypesText.toString());

        StringBuilder employersText = new StringBuilder();
        for (User user : service.getEmployers()) {
            employersText.append(user.getName() + " " + user.getSurname()).append(", ");
        }
        employersText.setLength(employersText.length() - 2);
        binding.tvEmployers.setText(employersText.toString());

        if (service.isAvailable()) {
            binding.tvAvailable.setText("AVAILABLE");
            binding.tvAvailable.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark));
        } else {
            binding.tvAvailable.setText("NOT AVAILABLE");
            binding.tvAvailable.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        }

        if (service.isVisible()) {
            binding.tvVisibility.setText("VISIBLE");
        } else {
            binding.tvVisibility.setText("NOT VISIBLE");
        }

        Picasso.get()
                .load(service.getImage())
                .into(binding.ivProductImg);
    }

    private void showBookingCalendarDialog(Service service, List<Event> allEvents) {
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    String event = binding.dropDownEvents.getText().toString();
                    Event event2 = new Event();
                    for(Event event1: allEvents){
                        if(event1.getEventName().equals(event)){
                            event2 = event1;
                        }
                    }
                    BookingCalendarFragment bookingCalendarFragment = BookingCalendarFragment.newInstance(service, event2, user.getId());
                    FragmentManager fragmentManager = getParentFragmentManager();
                    bookingCalendarFragment.show(fragmentManager, "BookingCalendarFragment");
                });
    }
}