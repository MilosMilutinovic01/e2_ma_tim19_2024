package com.example.eventplanner.fragments.admin;

import static android.content.ContentValues.TAG;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.adapters.ServicesAdapter;
import com.example.eventplanner.adapters.UserReportsAdapter;
import com.example.eventplanner.async.OnProductsLoadedListener;
import com.example.eventplanner.async.OnServiceReservationsLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.async.OnUserReportsLoadedListener;
import com.example.eventplanner.databinding.FragmentUserReportsBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.ServiceReservationService;
import com.example.eventplanner.services.UserReportService;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserReportsFragment extends Fragment implements RecyclerViewInterface {
    private UserReportsAdapter userReportsAdapter;
    private ArrayList<UserReport> userReports;
    private View view;
    private UserReportService userReportService;
    private ServiceReservationService serviceReservationService;
    private ProductService productService;
    private ServiceService serviceService;
    private NotificationService notificationService;
    private FragmentUserReportsBinding binding;

    public UserReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentUserReportsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.view = view;
        userReportService = new UserReportService(requireContext());
        productService = new ProductService(requireContext());
        serviceService = new ServiceService(requireContext());
        notificationService = new NotificationService(requireContext());
        serviceReservationService = new ServiceReservationService(requireContext());

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userReportService.getAllUserReports(new OnUserReportsLoadedListener() {
            @Override
            public void onUserReportsLoaded(List<UserReport> allReports) {
                userReports = (ArrayList<UserReport>) allReports;
                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                userReportsAdapter = new UserReportsAdapter(getActivity(), userReports, UserReportsFragment.this);
                binding.recyclerView.setAdapter(userReportsAdapter);
            }
            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting services: " + e.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        UserReport selectedReport = userReports.get(position);
        User reported = selectedReport.getReported();
        if (reported != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("user", reported);

            Navigation.findNavController(view).navigate(R.id.navigateToUserProfile, bundle);
        }
    }

    @Override
    public void onEditClick(int position) {
        UserReport selectedReport = userReports.get(position);
        User reporter = selectedReport.getReporter();
        if (reporter != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("user", reporter);

            Navigation.findNavController(view).navigate(R.id.navigateToUserProfile, bundle);
        }
    }

    @Override
    public void onDeleteClick(int position) {
        UserReport selectedReport = userReports.get(position);
        selectedReport.setStatus(UserReportStatus.ACCEPTED);
        userReportService.updateUserReportData(selectedReport);

        if (selectedReport.getReported().getRole().equals(Role.OWNER)) {
            productService.getProductsByCompanyId(new OnProductsLoadedListener() {
                @Override
                public void onProductsLoaded(List<Product> allProducts) {
                    for (Product p : allProducts) {
                        p.setAvailable(false);
                        productService.updateProductData(p);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    Log.e(TAG, "Error getting products: " + e.getMessage());
                }
            }, selectedReport.getReported().getCompanyId());

            serviceService.getServicesByCompanyId(new OnServicesLoadedListener() {
                @Override
                public void onServicesLoaded(List<Service> allServices) {
                    for (Service s : allServices) {
                        s.setAvailable(false);
                        serviceService.updateServiceData(s);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    Log.e(TAG, "Error getting services: " + e.getMessage());
                }
            }, selectedReport.getReported().getCompanyId());

        }

        if (selectedReport.getReported().getRole().equals(Role.EVENT_ORGANIZER)) {
            serviceReservationService.getActiveByEventOrganizerId(new OnServiceReservationsLoadedListener() {
                @Override
                public void OnServiceReservationsLoaded(List<ServiceReservation> allReservations) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                    for (ServiceReservation p : allReservations) {
                        p.setRejected(true);
                        serviceReservationService.updateServiceReservationData(p);
                        notificationService.createNotification(new Notification(
                                null,
                                "Reservation rejected",
                                "Event organizer is blocked, reservation for " + p.getServiceName() + " has been rejected.      " + LocalDateTime.now().format(formatter),
                                 p.getEmployeeId(),
                                false
                        ));
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    Log.e(TAG, "Error getting products: " + e.getMessage());
                }
            }, selectedReport.getReported().getId());



        }
    }

    @Override
    public void onBlockClick(int position) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        UserReport selectedReport = userReports.get(position);
        selectedReport.setStatus(UserReportStatus.REJECTED);
        userReportService.updateUserReportData(selectedReport);
        notificationService.createNotification(new Notification(
                null,
                "Report rejected",
                "Your report on user " + selectedReport.getReported().getEmail() + " has been rejected. Reason: " + selectedReport.getRejectReason() + " " + LocalDateTime.now().format(formatter),
                selectedReport.getReporterId(),
                false
        ));
    }
}