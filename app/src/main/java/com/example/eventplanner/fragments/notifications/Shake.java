package com.example.eventplanner.fragments.notifications;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Shake implements SensorEventListener {

    private OnShakeListener listener;
    private long shakeTimestamp;
    private int shakeCount;

    public interface OnShakeListener {
        void onShake(int count);
    }

    public Shake(OnShakeListener listener) {
        this.listener = listener;
    }

    public void onSensorChanged(SensorEvent event) {
        if (listener != null) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            float gx = x / SensorManager.GRAVITY_EARTH;
            float gy = y / SensorManager.GRAVITY_EARTH;
            float gz = z / SensorManager.GRAVITY_EARTH;

            float gravityForce = (float)Math.sqrt(gx*gx + gy*gy + gz*gz);

            if (gravityForce > 2.7F) {
                final long currentTime = System.currentTimeMillis();
                if (shakeTimestamp + 3000 < currentTime) {
                    shakeCount = 0;
                }
                if (shakeTimestamp + 500> currentTime) {
                    return;
                }
                shakeTimestamp = currentTime;
                shakeCount++;
                listener.onShake(shakeCount);
            }
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

}
