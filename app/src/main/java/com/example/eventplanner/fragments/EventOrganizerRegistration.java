package com.example.eventplanner.fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEventOrganizerRegistrationBinding;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.services.AuthService;


public class EventOrganizerRegistration extends Fragment {
    private AuthService authService;

    EditText emailEditText, passwordEditText, confirmPasswordEditText, nameEditText, surnameEditText, numberEditText, streetEditText, cityEditText, countryEditText;
    private FragmentEventOrganizerRegistrationBinding binding;
    public EventOrganizerRegistration() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentEventOrganizerRegistrationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailEditText = binding.etEmail;
        passwordEditText = binding.etPassword;
        confirmPasswordEditText = binding.etConfirmPassword;
        nameEditText = binding.etName;
        surnameEditText = binding.etSurname;
        numberEditText = binding.etPhone;
        streetEditText = binding.etStreet;
        cityEditText = binding.etCity;
        countryEditText = binding.etCountry;
        binding.btnEventOrganizerRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser(view);
            }
        });
    }

    void registerUser(View view){
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();

        boolean isValidated = validateData(email, password, confirmPassword);
        if(!isValidated){
            return;
        }

        if (authService.registerUser(email, password, cityEditText.getText().toString(), countryEditText.getText().toString(),
                nameEditText.getText().toString(), numberEditText.getText().toString(), streetEditText.getText().toString(),
                surnameEditText.getText().toString(), Role.EVENT_ORGANIZER)) {
            Toast.makeText(getContext(), "Registration initiated!", Toast.LENGTH_SHORT).show();
            Navigation.findNavController(requireView()).navigate(R.id.navigateToLoginFragment);
        } else {
            Toast.makeText(getContext(), "Failed to initiate registration!", Toast.LENGTH_SHORT).show();
        }
    }

    boolean validateData(String email, String password, String confirmPassword){
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailEditText.setError("Email is invalid!");
            return false;
        }

        if(password.length() < 6){
            passwordEditText.setError("Password length is invalid!");
            return false;
        }

        if(!password.equals(confirmPassword)){
            confirmPasswordEditText.setError("Password not matched!");
            return false;
        }

        return true;
    }
}