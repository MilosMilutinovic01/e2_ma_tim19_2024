package com.example.eventplanner.fragments.admin;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentRejectReportBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.RatingReport;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.RatingsService;
import com.example.eventplanner.services.UserService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class RejectReportFragment extends Fragment {
    private FragmentRejectReportBinding binding;
    private RatingReport report;

    private UserService userService;

    private View view;

    private NotificationService notificationService;
    private AuthService authService;

    public RejectReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRejectReportBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        userService = new UserService(requireContext());
        authService = new AuthService(requireContext());
        notificationService = new NotificationService(requireContext());


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){
            report = getArguments().getParcelable("report");
        }

        binding.btnCreate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.etReason.getText().toString().isEmpty()){
                    binding.etReason.setError("Cannot be empty");
                    return;
                }
                sendNotification();
                Navigation.findNavController(view).popBackStack();
            }
        });
    }

    private void sendNotification(){
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {

                    userService.getUserById(report.getReporterId())
                            .addOnSuccessListener(companyOwner -> {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                notificationService.createNotification(new Notification(
                                        null,
                                        "Rating rejected",
                                        "Admin rejected your rating report. Reason of rejection: " + report.getReportReason() + ", on date :"  + LocalDateTime.now().format(formatter),
                                        companyOwner.getId(),
                                        false
                                ));
                            })
                            .addOnFailureListener(e -> Log.e(TAG, "Error getting user: " + e.getMessage()));
                });
    }
}