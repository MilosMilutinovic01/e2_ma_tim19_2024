package com.example.eventplanner.fragments;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployerDetailsBinding;
import com.example.eventplanner.databinding.FragmentUserProfileBinding;
import com.example.eventplanner.databinding.FragmentUserReportsBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.UserReportService;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserProfileFragment extends Fragment {

    private FragmentUserProfileBinding binding;
    private User user;
    private AuthService authService;
    private UserService userService;
    private NotificationService notificationService;
    private UserReportService userReportService;
    public UserProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentUserProfileBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());
        userReportService = new UserReportService(requireContext());
        userService = new UserService(requireContext());
        notificationService = new NotificationService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if (user.getRole().equals(Role.OWNER)) {
                        binding.btnReportUser.setVisibility(View.VISIBLE);
                    }
                });
        if(getArguments() != null){
            user = getArguments().getParcelable("user");
            bindData();
        }
        binding.btnReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(v.getContext());
                View dialogView = inflater.inflate(R.layout.dialog_report_reason, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Report Reason")
                        .setView(dialogView)
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EditText editTextReason = dialogView.findViewById(R.id.editTextReason);
                                String reason = editTextReason.getText().toString().trim();

                                authService.getCurrentUser()
                                        .addOnSuccessListener(currentUser -> {
                                            User reporter = currentUser;
                                            User reported = user;
                                            userService.getAdmin()
                                                    .addOnSuccessListener(admin -> {
                                                        User notificationReceiver = admin;

                                                        userReportService.createUserReport(new UserReport(
                                                                null,
                                                                reporter.getId(),
                                                                reported.getId(),
                                                                reporter,
                                                                reported,
                                                                LocalDate.now(),
                                                                reason,
                                                                UserReportStatus.REPORTED,
                                                                ""
                                                        ));
                                                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

                                                        notificationService.createNotification(new Notification(
                                                                null,
                                                                "New user report",
                                                                "User " + reporter.getEmail() + " reported " + reported.getEmail() + " " + LocalDateTime.now().format(formatter),
                                                                admin.getId(),
                                                                false
                                                        ));
                                                        Navigation.findNavController(v).navigate(R.id.navigateToOwnerHomeView);
                                                    })
                                                    .addOnFailureListener(e -> Log.e(TAG, "Error getting admin: " + e.getMessage()));

                                        })
                                        .addOnFailureListener(e -> Log.e(TAG, "Error getting current user: " + e.getMessage()));
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create()
                        .show();
            }
        });
    }
    private void bindData(){
        binding.textName.setText(user.getName() + " " + user.getSurname());
        binding.textEmail.setText(user.getEmail());
        binding.textAddress.setText(user.getStreet() + ", " + user.getCity() + ", " + user.getCountry());
        binding.textPhone.setText(user.getPhone());
        Picasso.get().load(user.getProfileImage()).into(binding.ivProfile);
    }
}