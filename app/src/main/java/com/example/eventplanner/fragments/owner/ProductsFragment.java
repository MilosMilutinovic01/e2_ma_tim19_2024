package com.example.eventplanner.fragments.owner;

import static android.content.ContentValues.TAG;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.model.Role;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.adapters.ProductsAdapter;
import com.example.eventplanner.databinding.FragmentOwnerHomeViewBinding;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Employer;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.ProductService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Arrays;

public class ProductsFragment extends Fragment implements RecyclerViewInterface {
    private ProductsAdapter productsAdapter;
    private ArrayList<Product> products;
    private ArrayList<EventType> eventTypes;
    private FragmentProductsBinding binding;

    private View view;
    private ProductService productService;
    private AuthService authService;


    public ProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProductsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.view = view;
        productService = new ProductService(requireContext());
        authService = new AuthService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getProducts();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        productsAdapter = new ProductsAdapter(getActivity(), products,this);
        binding.recyclerView.setAdapter(productsAdapter);

        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if (user.getRole().equals(Role.OWNER)) {
                        binding.btnAddNew.setVisibility(View.VISIBLE);
                    }
                });

        binding.btnAddNew.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToProductCreationFragment);
            }
        });


    }
    private void getProducts() {
        productService.getProducts()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        products = task.getResult();
                        productsAdapter.setProducts(products);
                    } else {
                        Log.e(TAG, "Error getting products", task.getException());
                        Toast.makeText(requireContext(), "Error getting products", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onItemClick(int position) {
        Product selectedProduct = products.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("selectedProduct", selectedProduct);
        Navigation.findNavController(this.view).navigate(R.id.navigateToProductDetails, bundle);
    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}