package com.example.eventplanner.fragments;

import static android.content.ContentValues.TAG;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.Manifest;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.PriceListAdapter;
import com.example.eventplanner.async.OnBundlesLoadedListener;
import com.example.eventplanner.async.OnProductLoadedListener;
import com.example.eventplanner.async.OnProductsLoadedListener;
import com.example.eventplanner.async.OnServiceLoadedListener;

import com.example.eventplanner.databinding.FragmentPriceListBinding;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.BundleService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.async.OnServicesLoadedListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class PriceListFragment extends Fragment implements RecyclerViewInterface {
    private PriceListAdapter priceListAdapter;
    private List<Product> products;
    private List<Service> services;
    private List<com.example.eventplanner.model.Bundle> bundles;
    List<Object> items;
    private FragmentPriceListBinding binding;
    private View view;
    private ProductService productService;
    private ServiceService serviceService;
    private BundleService bundleService;
    private AuthService authService;
    private User user;
    private boolean productsLoaded = false;
    private boolean servicesLoaded = false;
    private boolean bundlesLoaded = false;


    public PriceListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPriceListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.view = view;
        productService = new ProductService(requireContext());
        serviceService = new ServiceService(requireContext());
        authService = new AuthService(requireContext());
        bundleService = new BundleService(requireContext());
        items = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        priceListAdapter = new PriceListAdapter(getActivity(), items, this);
        binding.recyclerView.setAdapter(priceListAdapter);

        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    this.user = user;
                    fetchServices(user.getCompanyId());
                    fetchProducts(user.getCompanyId());
                    fetchBundles(user.getCompanyId());
                })
                .addOnFailureListener(e -> Log.e(TAG, "Error getting user: " + e.getMessage()));

        binding.btnPDF.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PdfDocument pdfDocument = new PdfDocument();
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                Canvas canvas = page.getCanvas();
                Paint paint = new Paint();

                int startX = 10, startY = 25, lineHeight = 20;

                // Drawing header
                paint.setTextSize(10);
                canvas.drawText("PRICE LIST", startX, startY, paint);
                startY += lineHeight + 10;

                for (Object item : items) {
                    if (item instanceof Product) {
                        Product product = (Product) item;
                        canvas.drawText(product.getName() + "  " + product.getPrice() + "RSD" +  "  discount: " + product.getDiscount() + "%", startX, startY, paint);
                        startY += lineHeight + 10;
                    } else if (item instanceof Service) {
                        Service service = (Service) item;
                        canvas.drawText(service.getName() + "  " + service.getPrice() + "RSD" +  "  discount: " + service.getDiscount() + "%", startX, startY, paint);
                        startY += lineHeight + 10;
                    }
                    else if (item instanceof com.example.eventplanner.model.Bundle) {
                        com.example.eventplanner.model.Bundle bundle = (com.example.eventplanner.model.Bundle) item;
                        canvas.drawText(bundle.getName() + "  " + bundle.getPrice() + "RSD" +  "  discount: " + bundle.getDiscount() + "%", startX, startY, paint);
                        startY += lineHeight + 10;
                    }
                }

                pdfDocument.finishPage(page);
                // Saving the PDF document
                File pdfFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "PriceList.pdf");
                try {
                    pdfDocument.writeTo(new FileOutputStream(pdfFile));
                    Toast.makeText(getContext(), "PDF saved to Downloads", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Log.e("PDF", "Error writing PDF", e);
                    Toast.makeText(getContext(), "Failed to save PDF", Toast.LENGTH_LONG).show();
                } finally {
                    pdfDocument.close();
                }
            }
        });
    }

    private void fetchProducts(String companyId) {
        productService.getProductsByCompanyId(new OnProductsLoadedListener() {
            @Override
            public void onProductsLoaded(List<Product> allProducts) {
                products = allProducts;
                items.addAll(products);
                productsLoaded = true;
                checkDataLoaded();
            }
            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting products: " + e.getMessage());
                productsLoaded = true;
                checkDataLoaded();
            }
        }, companyId);
    }

    private void fetchServices(String companyId) {
        serviceService.getServicesByCompanyId(new OnServicesLoadedListener() {
            @Override
            public void onServicesLoaded(List<Service> allServices) {
                services = allServices;
                items.addAll(services);
                servicesLoaded = true;
                checkDataLoaded();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting services: " + e.getMessage());
                servicesLoaded = true;
                checkDataLoaded();
            }
        }, companyId);
    }

    private void fetchBundles(String companyId) {

        bundleService.getBundlesByCompanyId(new OnBundlesLoadedListener() {
            @Override
            public void onBundlesLoaded(List<com.example.eventplanner.model.Bundle> allBundles) {
                bundles = allBundles;
                items.addAll(bundles);
                for (com.example.eventplanner.model.Bundle bundle : bundles) {
                    List<String> productIds = bundle.getProductIds();
                    List<String> serviceIds = bundle.getServiceIds();
                    ArrayList<Product> bundleProducts = new ArrayList<>();
                    ArrayList<Service> bundleServices = new ArrayList<>();

                    AtomicInteger productFetchCount = new AtomicInteger(0);
                    AtomicInteger serviceFetchCount = new AtomicInteger(0);

                    for (String productId : productIds) {
                        productService.getProductById(productId, new OnProductLoadedListener() {
                            @Override
                            public void onProductLoaded(Product product) {
                                bundleProducts.add(product);
                                if (productFetchCount.incrementAndGet() == productIds.size()) {
                                    bundle.setProducts(bundleProducts);
                                    checkAllDataLoaded();
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Error loading product: " + e.getMessage());
                                checkAllDataLoaded();
                            }
                        });
                    }

                    for (String serviceId : serviceIds) {
                        serviceService.getServiceById(serviceId, new OnServiceLoadedListener() {
                            @Override
                            public void onServiceLoaded(Service service) {
                                bundleServices.add(service);
                                if (serviceFetchCount.incrementAndGet() == serviceIds.size()) {
                                    bundle.setServices(bundleServices);
                                    checkAllDataLoaded();
                                }
                            }
                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Error loading service: " + e.getMessage());
                                checkAllDataLoaded();
                            }
                        });
                    }
                }
                checkAllDataLoaded();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting bundles: " + e.getMessage());
                checkAllDataLoaded();
            }
        }, companyId);
    }

    private synchronized void checkAllDataLoaded() {
        for (com.example.eventplanner.model.Bundle bundle : bundles) {
            if (bundle.getProducts() != null && bundle.getServices() != null) {
                bundlesLoaded = true;
                checkDataLoaded();
            }
        }
    }

    private void checkDataLoaded() {
        if (productsLoaded && servicesLoaded && bundlesLoaded) {
            for(com.example.eventplanner.model.Bundle b: bundles){
                double totalPrice = 0.0;
                for (Service service : b.getServices()) {
                    totalPrice += service.getPrice() * (1 - service.getDiscount() / 100.0);
                }
                for (Product product : b.getProducts()) {
                    totalPrice += product.getPrice() * (1 - product.getDiscount() / 100.0);
                }
                b.setPrice(totalPrice);
            }
            priceListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(int position) {
        if(this.user.getRole().equals(Role.OWNER)){
            Object selectedItem = items.get(position);
            Bundle bundle = new Bundle();
            bundle.putParcelable("selectedItem", (Parcelable) selectedItem);
            Navigation.findNavController(this.view).navigate(R.id.navigateToEditPrice, bundle);
        }
    }
    @Override
    public void onEditClick(int position) {
    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}