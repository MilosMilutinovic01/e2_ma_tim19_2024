package com.example.eventplanner.fragments;

import android.health.connect.datatypes.units.Length;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.async.OnEventTypesLoadedListener;
import com.example.eventplanner.databinding.FragmentAdditionalCompanyEntriesBinding;
import com.example.eventplanner.databinding.FragmentCompanyStepRegistrationBinding;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventTypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AdditionalCompanyEntriesFragment extends Fragment {
    private FragmentAdditionalCompanyEntriesBinding binding;
    private List<ProductAndServiceCategory> categoriesList;
    private List<EventType> eventTypeList;
    private List<ProductAndServiceCategory> selectedCategories;
    private List<EventType> selectedEventTypes;

    public AdditionalCompanyEntriesFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAdditionalCompanyEntriesBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectedCategories = new ArrayList<>();
        selectedEventTypes = new ArrayList<>();
        User owner1 = new User();
        Company company1 = new Company();
        if(getArguments() != null){
            owner1 = getArguments().getParcelable("owner");
            company1 = getArguments().getParcelable("company");
        }
        final User owner = owner1;
        final Company company = company1;

        CategoryService categoryService = new CategoryService(getContext());
        EventTypeService eventTypeService = new EventTypeService(getContext());
        AutoCompleteTextView dropDownCategories = binding.dropDownCategories;
        AutoCompleteTextView dropDownEventTypes = binding.dropDownEventTypes;
        List<String> categoryNames = new ArrayList<>();
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, categoryNames);
        List<String> eventTypeNames = new ArrayList<>();
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, eventTypeNames);

        dropDownCategories.setAdapter(adapter1);
        dropDownEventTypes.setAdapter(adapter2);

        categoryService.getAllCategories(new OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded(List<ProductAndServiceCategory> categories) {
                categoriesList = categories;
                categoryNames.clear();

                for (ProductAndServiceCategory category : categories) {
                    categoryNames.add(category.getName());
                }

                adapter1.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting categories: " + e.getMessage());
            }
        });

        eventTypeService.getAllEventTypes(new OnEventTypesLoadedListener() {
            @Override
            public void onEventTypesLoaded(List<EventType> eventTypes) {
                eventTypeList = eventTypes;
                eventTypeNames.clear();

                for (EventType eventType : eventTypes) {
                    eventTypeNames.add(eventType.getName());
                }

                adapter2.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting categories: " + e.getMessage());
            }
        });


        binding.btnCompanyAdditionalNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedCategories.size() > 0){
                    String selectedCategoryName = binding.dropDownCategories.getText().toString();
                    ProductAndServiceCategory selectedCategory = null;
                    EventType selectedEventType = null;
                    for (ProductAndServiceCategory category : categoriesList) {
                        if (category.getName().equals(selectedCategoryName)) {
                            selectedCategory = category;
                            break;
                        }
                    }
                    if(selectedEventType != null){
                        for (EventType eventType : eventTypeList) {
                            if (eventType.getName().equals(selectedEventType)) {
                                selectedEventType = eventType;
                                break;
                            }
                        }
                    }

                    Bundle bundle = new Bundle();
                    bundle.putParcelable("owner", owner);
                    bundle.putParcelable("company", company);
                    bundle.putParcelable("productAndServiceCategory", selectedCategory);
                    if(selectedEventType != null){
                        bundle.putParcelable("eventType", selectedEventType);
                    }else{
                        bundle.putParcelable("eventType", new EventType());
                    }
                    Navigation.findNavController(view).navigate(R.id.navigateToCompanyWorkTime, bundle);
                }else{
                    String message = "Must add at least one category!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.btnAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!binding.dropDownCategories.getText().toString().equals("Categories")){
                    String selectedCategoryName = binding.dropDownCategories.getText().toString();
                    ProductAndServiceCategory selectedCategory = null;
                    for (ProductAndServiceCategory category : categoriesList) {
                        if (category.getName().equals(selectedCategoryName)) {
                            selectedCategory = category;
                            break;
                        }
                    }
                    selectedCategories.add(selectedCategory);
                    String message = "Category \"" + selectedCategory.getName() + "\" added successfully!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }else{
                    String message = "You must select category!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.btnAddEventType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEST",binding.dropDownEventTypes.getText().toString() );
                if(!binding.dropDownEventTypes.getText().toString().equals("Event type")){
                    String selectedEventTypeName = binding.dropDownEventTypes.getText().toString();
                    EventType selectedEventType = null;
                    for (EventType eventType : eventTypeList) {
                        if (eventType.getName().equals(selectedEventTypeName)) {
                            selectedEventType = eventType;
                            break;
                        }
                    }
                    selectedEventTypes.add(selectedEventType);
                    String message = "Event type \"" + selectedEventType.getName() + "\" added successfully!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }else{
                    String message = "You must select event type!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}