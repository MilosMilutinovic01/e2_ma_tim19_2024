package com.example.eventplanner.fragments.owner;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.BundlesAdapter;
import com.example.eventplanner.databinding.FragmentBundlesBinding;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.ReservationConfirmationType;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.services.AuthService;


import java.util.ArrayList;
import java.util.Arrays;

public class BundlesFragment extends Fragment {
    private BundlesAdapter bundlesAdapter;
    private ArrayList<com.example.eventplanner.model.Bundle> bundles;
    private ArrayList<EventType> eventTypes;
    private FragmentBundlesBinding binding;
    private AuthService authService;


    public BundlesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             android.os.Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentBundlesBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());

        binding.btnAddNew.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToBundleCreationFragment);
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBundles();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        bundlesAdapter = new BundlesAdapter(getActivity(), bundles);
        binding.recyclerView.setAdapter(bundlesAdapter);
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if (user.getRole().equals(Role.OWNER)) {
                        binding.btnAddNew.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void getBundles() {
        ProductAndServiceCategory category = new ProductAndServiceCategory("Foto i video", "Profesionalno fotografisanje i snimanje događaja");
        eventTypes = new ArrayList<>();
        eventTypes.add(new EventType("Svadbe", "", false, new ArrayList<>()));
        eventTypes.add(new EventType("Rodjendani", "", false, new ArrayList<>()));
        eventTypes.add(new EventType("Krstenja", "", false, new ArrayList<>()));
        eventTypes.add(new EventType("Rodjenja", "", false, new ArrayList<>()));
        bundles = new ArrayList<>();
        bundles.add(new com.example.eventplanner.model.Bundle("12345", category,"Snimanje dogadjaja", "Ovo je paket potreban za vase vencanje",3000.0, 0.0,null,null,null,null, eventTypes,"12 meseci pre termina" ,"2 dana pre termina","https://firebasestorage.googleapis.com/v0/b/eventplanner-91de1.appspot.com/o/productImages%2FAlbum%20sa%2050%20fotografija.jpg?alt=media&token=3fb9ce43-2462-4d38-bf9b-8bdecd19e48f", true,true, ReservationConfirmationType.MANUAL, null));
    }
}