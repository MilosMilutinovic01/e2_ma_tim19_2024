package com.example.eventplanner.fragments.owner;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.async.OnRatingsLoadedListener;
import com.example.eventplanner.databinding.FragmentCreateReportBinding;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.RatingsService;
import com.example.eventplanner.services.UserService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class CreateReportFragment extends Fragment {
    private FragmentCreateReportBinding binding;
    private RatingsService ratingsService;
    private AuthService authService;
    private CompanyRating rating;
    private User owner;

    private UserService userService;

    private View view;

    private NotificationService notificationService;

    public CreateReportFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentCreateReportBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        userService = new UserService(requireContext());
        notificationService = new NotificationService(requireContext());
        ratingsService = new RatingsService(requireContext());
        authService = new AuthService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){
            rating = getArguments().getParcelable("rating");
        }

        binding.btnCreate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.etReason.getText().toString().isEmpty()){
                    binding.etReason.setError("Cannot be empty");
                    return;
                }
                createReport();
                Navigation.findNavController(view).popBackStack();
            }
        });
    }

    void createReport(){
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    owner = user;

                    boolean result = ratingsService.createRatingReport(
                            rating.getId(),
                            binding.etReason.getText().toString(),
                            owner.getId()
                    );
                    if(result) {
                        notifyAdmin();
                    }
                    showToast(result ? "Creation of report initiated!" : "Failed to create report!");
                })
                .addOnFailureListener(e -> showToast("Failed to retrieve current user!"));

    }

    private void showToast(String message) {
        if (getContext() != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    private void notifyAdmin() {
        authService.getCurrentUser()
                .addOnSuccessListener(user -> {

                    userService.getAdmin()
                            .addOnSuccessListener(admin -> {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                notificationService.createNotification(new Notification(
                                        null,
                                        "New report",
                                        "User " + user.getEmail() + " left a report for the rating "  + LocalDateTime.now().format(formatter),
                                        admin.getId(),
                                        false
                                ));

                            })
                            .addOnFailureListener(e -> Log.e(TAG, "Error getting admin: " + e.getMessage()));
                });
    }
}