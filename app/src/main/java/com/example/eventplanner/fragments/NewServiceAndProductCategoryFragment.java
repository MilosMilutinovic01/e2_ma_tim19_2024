package com.example.eventplanner.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.eventplanner.R;
import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.services.CategoryService;

public class NewServiceAndProductCategoryFragment extends DialogFragment {
    private CategoryService categoryService;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_new_service_and_product_category, null);
        categoryService = new CategoryService(getContext());

        // Find the EditText views
        EditText nameEditText = view.findViewById(R.id.dialogName);
        EditText descriptionEditText = view.findViewById(R.id.dialogDescription);

        // Retrieve arguments
        Bundle args = getArguments();
        if (args != null) {
            String name = args.getString("name");
            String description = args.getString("description");
            String id = args.getString("id");
            // Set data to EditText fields
            nameEditText.setText(name);
            descriptionEditText.setText(description);
        }

        AlertDialog alertDialog = new AlertDialog.Builder(requireContext())
                .setView(view)
                .setTitle("Enter Category Details")
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", (dialog, which) -> {
                    // Handle cancel button click
                })
                .create();

        alertDialog.setOnShowListener(dialogInterface -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
                // Retrieve text from EditText views when the OK button is clicked
                String name = nameEditText.getText().toString();
                String description = descriptionEditText.getText().toString();
                ProductAndServiceCategory productAndServiceCategory = new ProductAndServiceCategory(name, description);
                if(!args.getString("id").equals("")){
                    productAndServiceCategory.setId(args.getString("id"));
                }
                if(args.getString("name").equals("") && args.getString("description").equals("")){
                    categoryService.createCategory(productAndServiceCategory);
                }else{
                    categoryService.updateCategory(productAndServiceCategory, new CategoryUploadCallback() {
                        @Override
                        public void onSuccess() {
                            // Handle success
                            alertDialog.dismiss();
                        }

                        @Override
                        public void onFailure(String errorMessage) {
                            // Handle failure
                            Log.e("CategoryUpdate", errorMessage);
                            // Optionally show an error message to the user
                        }
                    });
                }

                alertDialog.dismiss();
            });
        });

        return alertDialog;
    }


    public static NewServiceAndProductCategoryFragment newInstance(String name, String description, String id) {
        NewServiceAndProductCategoryFragment fragment = new NewServiceAndProductCategoryFragment();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("description", description);
        args.putString("id", id);
        fragment.setArguments(args);
        return fragment;
    }
}
