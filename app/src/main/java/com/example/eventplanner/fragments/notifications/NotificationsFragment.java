package com.example.eventplanner.fragments.notifications;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.adapters.NotificationAdapter;
import com.example.eventplanner.async.OnNotificationsLoadedListener;
import com.example.eventplanner.databinding.FragmentNotificationsBinding;
import com.example.eventplanner.databinding.FragmentUserReportsBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.AuthService;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class NotificationsFragment extends Fragment implements RecyclerViewInterface, Shake.OnShakeListener {

    private FragmentNotificationsBinding binding;
    private SensorManager sensorManager;
    private Sensor sensor;
    private Shake shakeDetector;


    private TabLayout tabLayout;
    private ViewPager2 viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = binding.tabLayout;
        viewPager = binding.viewPager;
        viewPagerAdapter = new ViewPagerAdapter(getActivity());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.getTabAt(position).select();
            }
        });

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        shakeDetector = new Shake(this);
    }

    @Override
    public void onItemClick(int position) {
        // Implement item click logic
    }

    @Override
    public void onEditClick(int position) {
        // Implement edit click logic
    }

    @Override
    public void onDeleteClick(int position) {
        // Implement delete click logic
    }

    @Override
    public void onBlockClick(int position) {
        // Implement block click logic
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(shakeDetector, sensor, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(shakeDetector);
    }

    @Override
    public void onShake(int count) {
        int currentItemIndex = viewPager.getCurrentItem();
        int nextItemIndex = (currentItemIndex + 1) % viewPagerAdapter.getItemCount();
        viewPager.setCurrentItem(nextItemIndex);
    }

}
