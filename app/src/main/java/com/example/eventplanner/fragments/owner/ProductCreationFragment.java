package com.example.eventplanner.fragments.owner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.databinding.FragmentEmployerRegistrationBinding;
import com.example.eventplanner.databinding.FragmentProductCreationBinding;
import com.example.eventplanner.databinding.FragmentServicesBinding;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.ProductService;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

public class ProductCreationFragment extends Fragment {
    private FragmentProductCreationBinding binding;
    private ProductService productService;
    private AuthService authService;
    EditText name, description, price, discount;
    CheckBox available,visible;
    MultiAutoCompleteTextView eventTypes;
    AutoCompleteTextView categoriesTv, subcategoriesTv;
    ProductAndServiceCategory selectedCategory;
    Subcategory selectedSubcategory;
    ArrayList<EventType> selectedEventTypes;
    Bitmap selectedImageBitmap;
    public ProductCreationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentProductCreationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        productService = new ProductService(requireContext());
        authService = new AuthService(requireContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        // Sample list of categories (replace it with your actual list)
        ArrayList<ProductAndServiceCategory> categories = new ArrayList<>();
        ProductAndServiceCategory c1 = new ProductAndServiceCategory("Foto i video", "Profesionalno fotografisanje i snimanje događaja");
        ProductAndServiceCategory c2 = new ProductAndServiceCategory("Smestaj", "Profesionalno fotografisanje i snimanje događaja");
        ProductAndServiceCategory c3 = new ProductAndServiceCategory("Muzika i zabava ", "Profesionalno fotografisanje i snimanje događaja");
        categories.add(c1);
        categories.add(c2);
        categories.add(c3);
        ArrayList<Subcategory> subcategories = new ArrayList<>();
        Subcategory s1 = new Subcategory("Fotografije i Albumi", "Ispis fotografija u različitim formatima. Dizajn i izrada personalizovanih fotoknjiga i albuma", SubcategoryType.PRODUCT, c1);
        Subcategory s2 = new Subcategory("Videografija", "Ispis fotografija u različitim formatima. Dizajn i izrada personalizovanih fotoknjiga i albuma", SubcategoryType.PRODUCT, c2);
        Subcategory s3 = new Subcategory("Dron", "Ispis fotografija u različitim formatima. Dizajn i izrada personalizovanih fotoknjiga i albuma", SubcategoryType.PRODUCT, c3);
        subcategories.add(s1);
        subcategories.add(s2);
        subcategories.add(s3);

        ArrayList<EventType>  ets = new ArrayList<>();
        ets.add(new EventType("Svadbe", "", false, new ArrayList<>()));
        ets.add(new EventType("Rodjendani", "", false, new ArrayList<>()));
        ets.add(new EventType("Krstenja", "", false, new ArrayList<>()));
        ets.add(new EventType("Rodjenja", "", false, new ArrayList<>()));

        categoriesTv = binding.etCategory;
        ArrayAdapter<ProductAndServiceCategory> categoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoriesTv.setAdapter(categoryAdapter);
        categoriesTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedCategory = (ProductAndServiceCategory) parent.getItemAtPosition(position);
            }
        });


        subcategoriesTv = binding.etSubcategory;
        ArrayAdapter<Subcategory> subcategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategories);
        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subcategoriesTv.setAdapter(subcategoryAdapter);
        subcategoriesTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedSubcategory = (Subcategory) parent.getItemAtPosition(position);
            }
        });


        eventTypes = binding.etEventTypes;
        ArrayAdapter<EventType> etsAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, ets);
        eventTypes.setAdapter(etsAdapter);
        eventTypes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        selectedEventTypes = new ArrayList<>();
        eventTypes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventType selectedEventType = (EventType) parent.getItemAtPosition(position);
                if (!selectedEventTypes.contains(selectedEventType)) {
                    selectedEventTypes.add(selectedEventType);
                }
            }
        });

        name = binding.etProductName;
        description = binding.etDescription;
        price = binding.etPrice;
        discount = binding.etDiscount;
        available = binding.cbAvailable;
        visible = binding.cbVisible;
        binding.btnCreateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createProduct(view);
            }
        });

    }

    void imageChooser() {

        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        launchSomeActivity.launch(i);
    }

    ActivityResultLauncher<Intent> launchSomeActivity
            = registerForActivityResult(
            new ActivityResultContracts
                    .StartActivityForResult(),
            result -> {
                if (result.getResultCode()
                        == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    // do your operation from here....
                    if (data != null
                            && data.getData() != null) {
                        Uri selectedImageUri = data.getData();
                        try {
                            selectedImageBitmap = MediaStore.Images.Media.getBitmap(
                                    requireActivity().getContentResolver(),
                                    selectedImageUri);
                            binding.ownerImgProfile.setImageBitmap(selectedImageBitmap);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
    void createProduct(View view){
        if(!validateData()){
            return;
        }
            double productPrice = Double.parseDouble(price.getText().toString());
            double productDiscount = Double.parseDouble(discount.getText().toString());
            boolean productIsAvailable = available.isChecked();
            boolean productIsVisible = visible.isChecked();

            Product product = new Product(null,selectedCategory,selectedSubcategory,name.getText().toString(),description.getText().toString(),productPrice,productDiscount,selectedEventTypes,null,productIsAvailable,productIsVisible, null);
            authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    product.setCompanyId(user.getCompanyId());
                });

            if(productService.createProduct(product, selectedImageBitmap)){
                Navigation.findNavController(requireView()).navigate(R.id.navigateToProducts);
                Toast.makeText(getContext(), "Product added successfully!", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getContext(), "Failed to create product!", Toast.LENGTH_SHORT).show();
            }
        }

    boolean validateData(){
        if(binding.etProductName.getText().toString().isEmpty()){
            binding.etProductName.setError("Cannot be empty");
            return false;
        }
        if(binding.etDescription.getText().toString().isEmpty()){
            binding.etDescription.setError("Cannot be empty");
            return false;
        }
        if(binding.etDiscount.getText().toString().isEmpty()){
            binding.etDiscount.setError("Cannot be empty");
            return false;
        }
        if(binding.etPrice.getText().toString().isEmpty()){
            binding.etDiscount.setError("Cannot be empty");
            return false;
        }
        if(binding.etSubcategory.getText().toString().isEmpty()){
            binding.etSubcategory.setError("Cannot be empty");
            return false;
        }
        if(binding.etCategory.getText().toString().isEmpty()){
            binding.etCategory.setError("Cannot be empty");
            return false;
        }
        return true;
    }
    }

