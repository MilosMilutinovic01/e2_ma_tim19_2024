package com.example.eventplanner.fragments.owner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentServiceEditBinding;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.ReservationConfirmationType;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ServiceEditFragment extends Fragment {
    private FragmentServiceEditBinding binding;
    private ServiceService serviceService;
    private UserService userService;
    EditText name, description, price, discount, duration, specificities, cancellationDeadline, reservationDeadline;
    CheckBox available,visible;
    MultiAutoCompleteTextView eventTypes, employers;
    ProductAndServiceCategory selectedCategory;
    Subcategory selectedSubcategory;
    ArrayList<EventType> selectedEventTypes;
    ArrayList<User> selectedEmployers;
    Bitmap selectedImageBitmap;
    Service selectedService;
    public ServiceEditFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentServiceEditBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        serviceService = new ServiceService(requireContext());
        userService = new UserService(requireContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            selectedService = getArguments().getParcelable("service");
            if (getArguments() != null) {
                bindServiceDetails(selectedService);
            }
        }
        binding.btnEditService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateToServices);
            }
        });
        binding.btnAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        ArrayList<EventType>  ets = new ArrayList<>();
        ets.add(new EventType("Svadbe", "", false, new ArrayList<>()));
        ets.add(new EventType("Rodjendani", "", false, new ArrayList<>()));
        ets.add(new EventType("Krstenja", "", false, new ArrayList<>()));
        ets.add(new EventType("Rodjenja", "", false, new ArrayList<>()));
        employers = binding.etEmployers;
        ArrayList<User> allEmployers = new ArrayList<>();
        ArrayAdapter<User> employersAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, allEmployers);
        userService.getAllUsers(new OnUsersLoadedListener() {
            @Override
            public void onUsersLoaded(List<User> users) {
                allEmployers.addAll(users);
                // Update the adapter after adding all users to the list
                employersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });
        employers.setAdapter(employersAdapter);
        employers.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        selectedEmployers = selectedService.getEmployers();
        employers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedEmployer = (User) parent.getItemAtPosition(position);
                if (!selectedEmployers.contains(selectedEmployer)) {
                    selectedEmployers.add(selectedEmployer);
                }
            }
        });

        eventTypes = binding.etEventTypes;
        ArrayAdapter<EventType> etsAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, ets);
        eventTypes.setAdapter(etsAdapter);
        eventTypes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        selectedEventTypes = selectedService.getEventTypes();
        eventTypes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventType selectedEventType = (EventType) parent.getItemAtPosition(position);
                if (!selectedEventTypes.contains(selectedEventType)) {
                    selectedEventTypes.add(selectedEventType);
                }
            }
        });

        name = binding.etServiceName;
        description = binding.etDescription;
        price = binding.etPrice;
        discount = binding.etDiscount;
        available = binding.cbAvailable;
        visible = binding.cbVisible;
        duration = binding.etDuration;
        specificities = binding.etSpecificities;
        cancellationDeadline = binding.etCancellationDeadline;
        reservationDeadline = binding.etBookingDeadline;

        Picasso.get()
                .load(selectedService.getImage())
                .into(binding.ownerImgProfile);
        binding.btnEditService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editService(view);
            }
        });

    }

    void imageChooser() {

        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        launchSomeActivity.launch(i);
    }

    ActivityResultLauncher<Intent> launchSomeActivity
            = registerForActivityResult(
            new ActivityResultContracts
                    .StartActivityForResult(),
            result -> {
                if (result.getResultCode()
                        == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    // do your operation from here....
                    if (data != null
                            && data.getData() != null) {
                        Uri selectedImageUri = data.getData();
                        try {
                            selectedImageBitmap = MediaStore.Images.Media.getBitmap(
                                    requireActivity().getContentResolver(),
                                    selectedImageUri);
                            binding.ownerImgProfile.setImageBitmap(selectedImageBitmap);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
    void editService(View view){
        boolean isValidated = validateData();
        if(!isValidated){
            return;
        }
        double productPrice = Double.parseDouble(price.getText().toString());
        double productDiscount = Double.parseDouble(discount.getText().toString());
        boolean productIsAvailable = available.isChecked();
        boolean productIsVisible = visible.isChecked();
        long serviceDuration = Long.parseLong(duration.getText().toString());
        Service service = new Service(selectedService.getId(),selectedService.getCategory(),selectedService.getSubcategory(),name.getText().toString(),description.getText().toString(),specificities.getText().toString(),productPrice,productDiscount,serviceDuration,selectedEmployers,selectedEventTypes,cancellationDeadline.getText().toString(),reservationDeadline.getText().toString(),selectedService.getImage(),productIsAvailable,productIsVisible, ReservationConfirmationType.AUTOMATIC, selectedService.getCompanyId());

        if(serviceService.editService(service, selectedImageBitmap)){
            Navigation.findNavController(requireView()).navigate(R.id.navigateToServices);
            Toast.makeText(getContext(), "Service edited successfully!", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getContext(), "Failed to edit service!", Toast.LENGTH_SHORT).show();
        }
    }

    boolean validateData(){
        if(binding.etServiceName.getText().toString().isEmpty()){
            binding.etServiceName.setError("Cannot be empty");
            return false;
        }
        if(binding.etDescription.getText().toString().isEmpty()){
            binding.etDescription.setError("Cannot be empty");
            return false;
        }
        if(binding.etDiscount.getText().toString().isEmpty()){
            binding.etDiscount.setError("Cannot be empty");
            return false;
        }
        if(binding.etPrice.getText().toString().isEmpty()){
            binding.etPrice.setError("Cannot be empty");
            return false;
        }
        if(binding.etSpecificities.getText().toString().isEmpty()){
            binding.etSpecificities.setError("Cannot be empty");
            return false;
        }
        if(binding.etBookingDeadline.getText().toString().isEmpty()){
            binding.etBookingDeadline.setError("Cannot be empty");
            return false;
        }
        if(binding.etCancellationDeadline.getText().toString().isEmpty()){
            binding.etCancellationDeadline.setError("Cannot be empty");
            return false;
        }
        if(binding.etDuration.getText().toString().isEmpty()){
            binding.etDuration.setError("Cannot be empty");
            return false;
        }
        return true;
    }
    private void bindServiceDetails(Service service) {
        binding.etServiceName.setText(service.getName());
        binding.etDescription.setText(service.getDescription());
        binding.etPrice.setText(String.valueOf(service.getPrice()));
        binding.etDiscount.setText(String.valueOf(service.getDiscount()));
        binding.cbAvailable.setChecked(service.isAvailable());
        binding.cbVisible.setChecked(service.isVisible());
        StringBuilder tokenString = new StringBuilder();
        StringBuilder etokenString = new StringBuilder();
        for (EventType eventType : service.getEventTypes()) {
            tokenString.append(eventType.getName()).append(", ");
        }
        for (User employer : service.getEmployers()) {
            etokenString.append(employer.getName() + " " + employer.getSurname()).append(", ");
        }
        binding.etEventTypes.setText(tokenString.toString());
        binding.etEmployers.setText(etokenString.toString());
        binding.etSpecificities.setText(service.getSpecificities());
        binding.etBookingDeadline.setText(service.getBookingDeadline());
        binding.etCancellationDeadline.setText(service.getCancellationDeadline());
        binding.etDuration.setText(String.valueOf(service.getDuration()));
        //rb
    }
}

