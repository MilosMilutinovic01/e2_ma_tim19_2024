package com.example.eventplanner.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.eventplanner.R;
import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.SubcategoryUploadCallback;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.services.SubcategoryService;


public class NewServiceAndProductSubcategoryFragment extends DialogFragment {
    private SubcategoryService subcategoryService;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_new_service_and_product_subcategory, null);
        subcategoryService = new SubcategoryService(getContext());

        // Find the EditText views
        EditText nameEditText = view.findViewById(R.id.dialogSubName);
        EditText descriptionEditText = view.findViewById(R.id.dialogSubDescription);
        EditText typeEditText = view.findViewById(R.id.dialogSubType);

        Bundle args = getArguments();
        if (args != null) {
            String name = args.getString("name");
            String description = args.getString("description");
            String type = args.getString("type");
            String id = args.getString("id");
            // Set data to EditText fields
            nameEditText.setText(name);
            descriptionEditText.setText(description);
            typeEditText.setText(type);
        }

        AlertDialog alertDialog = new AlertDialog.Builder(requireContext())
                .setView(view)
                .setTitle("Enter subcategory Details")
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", (dialog, which) -> {
                    // Handle cancel button click
                })
                .create();

        alertDialog.setOnShowListener(dialogInterface -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
                // Retrieve text from EditText views when the OK button is clicked
                String name = nameEditText.getText().toString();
                String description = descriptionEditText.getText().toString();
                SubcategoryType type = SubcategoryType.valueOf(typeEditText.getText().toString());
                Subcategory subcategory = new Subcategory(name, description, type, new ProductAndServiceCategory());
                if(!args.getString("id").equals("")){
                    subcategory.setId(args.getString("id"));
                }
                if(args.getString("name").equals("") && args.getString("description").equals("")){
                    subcategoryService.createSubcategory(subcategory);
                }else{
                    subcategoryService.updateSubcategory(subcategory, new SubcategoryUploadCallback() {
                        @Override
                        public void onSuccess() {
                            // Handle success
                            alertDialog.dismiss();
                        }

                        @Override
                        public void onFailure(String errorMessage) {
                            // Handle failure
                            Log.e("CategoryUpdate", errorMessage);
                            // Optionally show an error message to the user
                        }
                    });
                }

                alertDialog.dismiss();
            });
        });

        return alertDialog;

    }

    public static NewServiceAndProductSubcategoryFragment newInstance(String name, String description, SubcategoryType type, String id) {
        NewServiceAndProductSubcategoryFragment fragment = new NewServiceAndProductSubcategoryFragment();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("description", description);
        args.putString("type", type.toString());
        args.putString("id", id);
        fragment.setArguments(args);
        return fragment;
    }
}