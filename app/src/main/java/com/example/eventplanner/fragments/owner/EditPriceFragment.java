package com.example.eventplanner.fragments.owner;

import static android.view.View.VISIBLE;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentEditPriceBinding;
import com.example.eventplanner.databinding.FragmentServiceEditBinding;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.ReservationConfirmationType;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.BundleService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EditPriceFragment extends Fragment {
    private FragmentEditPriceBinding binding;
    private ServiceService serviceService;
    private ProductService productService;
    private BundleService bundleService;
    EditText price, discount;
    private Object selectedItem;
    public EditPriceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =  FragmentEditPriceBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        serviceService = new ServiceService(requireContext());
        productService = new ProductService(requireContext());
        bundleService = new BundleService(requireContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            selectedItem = getArguments().getParcelable("selectedItem");
            if (getArguments() != null) {
                bindItemDetails(selectedItem);
            }
        }
        binding.btnEditPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPrice(view);
            }
        });

        //name = binding.etServiceName;
        price = binding.etPrice;
        discount = binding.etDiscount;

    }
    void editPrice(View view){
        boolean isValidated = validateData();
        if(!isValidated){
            return;
        }
        double itemPrice = Double.parseDouble(price.getText().toString());
        double itemDiscount = Double.parseDouble(discount.getText().toString());

        if (selectedItem != null) {
            if (selectedItem instanceof Product) {
                Product product = (Product) selectedItem;
                product.setPrice(itemPrice);
                product.setDiscount(itemDiscount);
                productService.updateProductData(product);
                Navigation.findNavController(requireView()).navigate(R.id.navigateToPriceList);
                Toast.makeText(getContext(), "Product price edited successfully!", Toast.LENGTH_SHORT).show();
            } else if (selectedItem instanceof Service) {
                Service service = (Service) selectedItem;
                service.setPrice(itemPrice);
                service.setDiscount(itemDiscount);
                serviceService.updateServiceData(service);
                Navigation.findNavController(requireView()).navigate(R.id.navigateToPriceList);
                Toast.makeText(getContext(), "Service price edited successfully!", Toast.LENGTH_SHORT).show();

            } else if (selectedItem instanceof com.example.eventplanner.model.Bundle) {
                com.example.eventplanner.model.Bundle bundle = (com.example.eventplanner.model.Bundle) selectedItem;
                bundle.setDiscount(itemDiscount);
                bundleService.updateBundleData(bundle);
                Navigation.findNavController(requireView()).navigate(R.id.navigateToPriceList);
                Toast.makeText(getContext(), "Bundle price edited successfully!", Toast.LENGTH_SHORT).show();

            }
        }
    }

    boolean validateData(){
        if(binding.etDiscount.getText().toString().isEmpty()){
            binding.etDiscount.setError("Cannot be empty");
            return false;
        }
        if(binding.etPrice.getText().toString().isEmpty()){
            binding.etPrice.setError("Cannot be empty");
            return false;
        }
        return true;
    }
    private void bindItemDetails(Object item) {
        //binding.etServiceName.setText(service.getName());
        if (item != null) {
            if (item instanceof Product) {
                Product product = (Product) item;
                binding.tvName.setText(product.getName());
                binding.etPrice.setText(String.valueOf(product.getPrice()));
                binding.etDiscount.setText(String.valueOf(product.getDiscount()));
                binding.tvBundlePrice.setText("");
            } else if (item instanceof Service) {
                Service service = (Service) item;
                binding.tvName.setText(service.getName());
                binding.tvBundlePrice.setText("");
                binding.etPrice.setText(String.valueOf(service.getPrice()));
                binding.etDiscount.setText(String.valueOf(service.getDiscount()));
            } else if (item instanceof com.example.eventplanner.model.Bundle) {
                com.example.eventplanner.model.Bundle bundle = (com.example.eventplanner.model.Bundle) item;
                binding.tvName.setText(bundle.getName());
                String bundlePrice = "";
                for (Product p : bundle.getProducts()) {
                    bundlePrice += p.getName() + ": " + p.getPrice() + "RSD (Discount: " + p.getDiscount() + "%)\n\n";
                }
                for (Service s : bundle.getServices()) {
                    bundlePrice += s.getName() + ": " + s.getPrice() + "RSD (Discount: " + s.getDiscount() + "%)\n\n";
                }
                binding.tvBundlePrice.setText(bundlePrice);
                binding.etDiscount.setText(String.valueOf(bundle.getDiscount()));
                binding.etPrice.setText(String.valueOf(bundle.getPrice()));
                binding.etPrice.setEnabled(false);
            }
        }
    }
}

