package com.example.eventplanner.fragments.employer;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.adapters.EmployerEventsAdapter;
import com.example.eventplanner.async.OnEmployerEventsLoadedListener;
import com.example.eventplanner.databinding.FragmentEmployerScheduleBinding;
import com.example.eventplanner.databinding.FragmentScheduleBinding;
import com.example.eventplanner.fragments.owner.EmployerScheduleFragment;
import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.EmployerEventService;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ScheduleFragment extends Fragment implements RecyclerViewInterface {
    private FragmentScheduleBinding binding;
    private EmployerEventsAdapter eventsAdapter;
    private EmployerEventService eventService;
    private List<EmployerEvent> events;
    private int dayOfWeek;
    private String date = "";
    private User employer;

    public ScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentScheduleBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        eventService = new EmployerEventService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){

            employer = getArguments().getParcelable("user");
        }
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("employer", employer);
                bundle.putString("date", date);
                bundle.putInt("dayOfWeek", dayOfWeek);
                Navigation.findNavController(view).navigate(R.id.navigateToEmployerCalendarEvent, bundle);
            }
        });

        binding.etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDialog();
            }
        });
    }

    private void openDatePickerDialog() {
        CalendarConstraints.Builder constraintsBuilder = new CalendarConstraints.Builder();
        long currentMillis = MaterialDatePicker.todayInUtcMilliseconds();

        constraintsBuilder.setStart(currentMillis);
        constraintsBuilder.setValidator(DateValidatorPointForward.now());

        MaterialDatePicker picker = MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setCalendarConstraints(constraintsBuilder.build())
                .build();


        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener((w)->{
            Long selectedDate = (Long) picker.getSelection();


            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault());
            String date = dateFormat.format(new Date(selectedDate));

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(selectedDate);

            // Get day of week
            dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            //this.dayOfWeek = getDayOfWeekString(dayOfWeek);

            binding.etDate.setText(date);
            this.date = date;
            findEvents();
        });
        picker.addOnNegativeButtonClickListener((w)->{
            picker.dismiss();
        });
    }

    private void findEvents(){
        eventService.getAllEmployerEvents(employer.getId(), date, new OnEmployerEventsLoadedListener() {
            @Override
            public void onEventsLoaded(List<EmployerEvent> employerEvents) {
                events = employerEvents;
                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                eventsAdapter = new EmployerEventsAdapter(getActivity(), events, ScheduleFragment.this);
                binding.recyclerView.setAdapter(eventsAdapter);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting events: " + e.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }

    @Override
    public void onBlockClick(int position) {

    }
}