package com.example.eventplanner.fragments.owner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployersAdapter;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.databinding.FragmentChatsBinding;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

public class ChatsFragment extends Fragment {
    private FragmentChatsBinding binding;
    private List<User> eventOrganizers;
    private User user1;
    private User user2;
    private AuthService authService;
    private UserService userService;
    public ChatsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentChatsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        authService = new AuthService(requireContext());
        userService = new UserService(requireContext());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set click listener for the profile image
        binding.ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileImageClicked(v);
            }
        });

        // Fetch data and update UI
        userService.getAllEventOrganizers(new OnUsersLoadedListener() {
            @Override
            public void onUsersLoaded(List<User> users) {
                eventOrganizers = users;
                bindData();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("TAG", "Error getting users: " + e.getMessage());
            }
        });
    }
    private void bindData(){
        Optional<User> user = eventOrganizers.stream()
                .filter(organizer -> organizer.getId().equals("qkYlD7jAuJcouD5IiTQkTNqvIMa2"))
                .findFirst();

        if(user.isPresent()){
            binding.tvName.setText(user.get().getName() + " " + user.get().getSurname());
            binding.tvLastMessage.setText("You: U redu      09/06/2024 11:37");
            Picasso.get().load(user.get().getProfileImage()).into(binding.ivProfile);
        }
    }

    public void onProfileImageClicked(View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("user",eventOrganizers.get(1));
        Navigation.findNavController(view).navigate(R.id.navigateToUserProfile, bundle);
    }
}