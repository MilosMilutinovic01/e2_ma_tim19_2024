package com.example.eventplanner.async;

public interface SubcategoryUploadCallback {
    void onSuccess();

    void onFailure(String errorMessage);
}
