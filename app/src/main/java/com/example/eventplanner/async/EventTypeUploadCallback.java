package com.example.eventplanner.async;

public interface EventTypeUploadCallback {
    void onSuccess();

    void onFailure(String errorMessage);
}
