package com.example.eventplanner.async;

import com.example.eventplanner.model.Product;

import java.util.List;

public interface OnProductsLoadedListener {
    void onProductsLoaded(List<Product> products);
    void onFailure(Exception e);
}
