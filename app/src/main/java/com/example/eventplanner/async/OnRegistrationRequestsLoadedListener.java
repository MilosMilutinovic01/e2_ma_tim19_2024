package com.example.eventplanner.async;

import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.Service;

import java.util.List;

public interface OnRegistrationRequestsLoadedListener {
    void onRequestsLoaded(List<RegistrationRequest> requests);
    void onFailure(Exception e);
}
