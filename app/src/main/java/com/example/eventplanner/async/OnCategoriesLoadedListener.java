package com.example.eventplanner.async;

import com.example.eventplanner.model.ProductAndServiceCategory;

import java.util.List;

public interface OnCategoriesLoadedListener {
    void onCategoriesLoaded(List<ProductAndServiceCategory> categories);
    void onFailure(Exception e);
}
