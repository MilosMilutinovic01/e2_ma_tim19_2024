package com.example.eventplanner.async;

import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;

import java.util.List;

public interface OnEventTypesLoadedListener {
    void onEventTypesLoaded(List<EventType> eventTypes);
    void onFailure(Exception e);
}
