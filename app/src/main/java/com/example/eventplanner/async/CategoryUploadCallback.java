package com.example.eventplanner.async;

public interface CategoryUploadCallback {
    void onSuccess();

    void onFailure(String errorMessage);
}
