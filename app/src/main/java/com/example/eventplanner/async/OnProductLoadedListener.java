package com.example.eventplanner.async;

import com.example.eventplanner.model.Product;

import java.util.List;

public interface OnProductLoadedListener {
    void onProductLoaded(Product product);
    void onFailure(Exception e);
}