package com.example.eventplanner.async;

import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.RatingReport;

import java.util.List;

public interface OnReportsLoadedListener {
    void onReportsLoaded(List<RatingReport> reports);
    void onFailure(Exception e);
}
