package com.example.eventplanner.async;

import com.example.eventplanner.model.ServiceReservation;

import java.util.List;

public interface OnServiceReservationsLoadedListener {
    void OnServiceReservationsLoaded(List<ServiceReservation> products);
    void onFailure(Exception e);
}
