package com.example.eventplanner.async;

import com.example.eventplanner.model.Budget;

import java.util.List;

public interface OnBudgetsLoadedListener {
    void onBudgetsLoaded(List<Budget> budgets);
    void onBudgetLoaded(Budget budgets);
    void onFailure(Exception e);
}