package com.example.eventplanner.async;

public interface ImageUploadCallback {
    void onImageUploadSuccess(String imageURL);
    void onImageUploadFailure(String errorMessage);
}
