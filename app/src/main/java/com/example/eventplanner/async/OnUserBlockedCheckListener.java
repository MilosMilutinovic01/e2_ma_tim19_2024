package com.example.eventplanner.async;

public interface OnUserBlockedCheckListener {
    void onUserBlockedCheck(boolean isBlocked);
}
