package com.example.eventplanner.async;

import com.example.eventplanner.model.Bundle;

import java.util.List;

public interface OnBundlesLoadedListener {
    void onBundlesLoaded(List<Bundle> bundles);
    void onFailure(Exception e);
}
