package com.example.eventplanner.async;

import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.User;

import java.util.List;

public interface OnEmployerEventsLoadedListener {
    void onEventsLoaded(List<EmployerEvent> events);
    void onFailure(Exception e);
}
