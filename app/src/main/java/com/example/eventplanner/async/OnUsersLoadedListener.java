package com.example.eventplanner.async;

import com.example.eventplanner.model.User;

import java.util.List;

public interface OnUsersLoadedListener {
    void onUsersLoaded(List<User> users);
    void onFailure(Exception e);
}
