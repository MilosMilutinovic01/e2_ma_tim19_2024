package com.example.eventplanner.async;

import com.example.eventplanner.model.Service;

public interface OnServiceLoadedListener {
    void onServiceLoaded(Service service);
    void onFailure(Exception e);
}