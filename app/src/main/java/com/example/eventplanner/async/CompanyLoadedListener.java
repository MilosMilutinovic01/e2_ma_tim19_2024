package com.example.eventplanner.async;

import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.EmployerEvent;

import java.util.List;

public interface CompanyLoadedListener {
    void onCompanyLoaded(Company company);
    void onFailure(Exception e);
}
