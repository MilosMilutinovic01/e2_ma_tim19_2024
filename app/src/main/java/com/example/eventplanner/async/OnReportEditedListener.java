package com.example.eventplanner.async;

import com.example.eventplanner.model.RatingReport;

import java.util.List;

public interface OnReportEditedListener {
    void onReportEdited(RatingReport report);
    void onFailure(Exception e);
}
