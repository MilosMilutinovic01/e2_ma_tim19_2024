package com.example.eventplanner.async;

import com.example.eventplanner.model.Notification;

import java.util.List;

public interface OnNotificationsLoadedListener {
    void onNotificationsLoaded(List<Notification> notifications);
    void onFailure(Exception e);
}
