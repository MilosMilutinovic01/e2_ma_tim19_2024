package com.example.eventplanner.async;

import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;

import java.util.List;

public interface OnSubcategoriesLoadedListener {
    void onSubcategoriesLoaded(List<Subcategory> subcategories);
    void onFailure(Exception e);
}
