package com.example.eventplanner.async;

import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;

import java.util.List;

public interface OnServicesLoadedListener {
    void onServicesLoaded(List<Service> services);
    void onFailure(Exception e);
}
