package com.example.eventplanner.async;

import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;

import java.util.List;

public interface OnUserReportsLoadedListener {
    void onUserReportsLoaded(List<UserReport> userReports);
    void onFailure(Exception e);
}
