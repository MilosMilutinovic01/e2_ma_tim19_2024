package com.example.eventplanner.async;

import com.example.eventplanner.model.User;

import java.util.List;

public interface OnUserLoadedListener {
    void onUserLoaded(User user);
    void onFailure(Exception e);
}
