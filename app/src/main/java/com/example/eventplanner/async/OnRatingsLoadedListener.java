package com.example.eventplanner.async;

import com.example.eventplanner.model.CompanyRating;

import java.util.List;

public interface OnRatingsLoadedListener {
    void onRatingsLoaded(List<CompanyRating> ratings);
    void onFailure(Exception e);
}
