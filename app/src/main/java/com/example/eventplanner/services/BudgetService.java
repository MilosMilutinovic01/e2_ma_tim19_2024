package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.async.OnBudgetsLoadedListener;
import com.example.eventplanner.model.Budget;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class BudgetService {
    private final FirebaseFirestore firestore;
    private final Context context;

    public BudgetService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
    }

    public void updateBudget(Budget budget) {
        CollectionReference budgetCollection = firestore.collection("budgets");
        budgetCollection
                .document(budget.getId())
                .set(budget.createBudgetMap())
                .addOnSuccessListener(aVoid -> Log.d("BudgetService", "Budget successfully updated!"))
                .addOnFailureListener(e -> Log.w("BudgetService", "Error updating budget", e));
    }

    public void saveBudget(Budget budget) {
        CollectionReference budgetCollection = firestore.collection("budgets");
        budgetCollection.add(budget.createBudgetMap())
                .addOnSuccessListener(documentReference -> {
                    budget.setId(documentReference.getId());
                    updateBudget(budget); // Save the ID to the budget document
                    Log.d("BudgetService", "Budget added with ID: " + documentReference.getId());
                })
                .addOnFailureListener(e -> {
                    Log.e("BudgetService", "Error adding budget", e);
                });
    }

    public void getBudgetByEventId(String eventId, OnBudgetsLoadedListener listener) {
        CollectionReference budgetCollection = firestore.collection("budgets");
        budgetCollection
                .whereEqualTo("eventId", eventId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            for (QueryDocumentSnapshot document : querySnapshot) {
                                Budget budget = document.toObject(Budget.class);
                                budget.setId(document.getId()); // Set the document ID
                                listener.onBudgetLoaded(budget);
                                return;
                            }
                        } else {
                            listener.onFailure(new Exception("No budget found for the given event ID"));
                        }
                    } else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getAllBudgets(String eventId, OnBudgetsLoadedListener listener) {
        CollectionReference budgetCollection = firestore.collection("budgets");
        budgetCollection.whereEqualTo("eventId", eventId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<Budget> budgets = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Budget budget = Budget.createBudgetFromMap(document.getData());
                            budget.setId(document.getId()); // Set the document ID
                            budgets.add(budget);
                        }
                        if (listener != null) {
                            listener.onBudgetsLoaded(budgets);
                        }
                    } else {
                        Log.e("BudgetService", "Error getting budgets", task.getException());
                        if (listener != null) {
                            listener.onFailure(task.getException());
                        }
                    }
                });
    }
}
