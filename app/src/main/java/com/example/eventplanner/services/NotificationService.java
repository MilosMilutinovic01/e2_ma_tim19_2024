package com.example.eventplanner.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.async.OnNotificationsLoadedListener;
import com.example.eventplanner.async.OnProductsLoadedListener;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NotificationService{

    private Context context;
    private NotificationManager notificationManager;
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;

    public NotificationService(Context context) {
        this.context = context;
        this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }
/*
    public void showNotification() {
        Intent activityIntent = new Intent(context, MainActivity.class);
        PendingIntent activityPendingIntent = PendingIntent.getActivity(
                context,
                1,
                activityIntent,
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0
        );


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notif)
                .setContentTitle("Event planner")
                .setContentText("Dodat je novi dogadjaj na kalendar");

        notificationManager.notify(1, notificationBuilder.build());
    }

    public static final String CHANNEL_ID = "notifikacije";*/

    public boolean createNotification(Notification notification) {
        try{
            firestore.collection("notifications").add(notification)
                    .addOnSuccessListener(documentReference -> {
                        notification.setId(documentReference.getId());
                        firestore.collection("notifications").document(documentReference.getId()).set(notification);
                    })
                    .addOnFailureListener(e -> {
                        Log.e("NotificationService", "Error adding Notification", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getAllNotificationsByReceiverId(OnNotificationsLoadedListener listener, String receiverId){
        List<Notification> notifications = new ArrayList<>();
        firestore.collection("notifications").whereEqualTo("receiverId", receiverId)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> data = document.getData();
                            Notification notification = createNotificationFromMap(data);

                            notification.setId(document.getId());
                            notifications.add(notification);

                        }
                        listener.onNotificationsLoaded(notifications);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });


    }

    public void getReadNotificationsByReceiverId(OnNotificationsLoadedListener listener, String receiverId){
        List<Notification> notifications = new ArrayList<>();
        firestore.collection("notifications").whereEqualTo("receiverId", receiverId).whereEqualTo("read",true)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> data = document.getData();
                            Notification notification = createNotificationFromMap(data);

                            notification.setId(document.getId());
                            notifications.add(notification);

                        }
                        listener.onNotificationsLoaded(notifications);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });


    }
    public void getUnReadNotificationsByReceiverId(OnNotificationsLoadedListener listener, String receiverId){
        List<Notification> notifications = new ArrayList<>();
        firestore.collection("notifications").whereEqualTo("receiverId", receiverId).whereEqualTo("read",false)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> data = document.getData();
                            Notification notification = createNotificationFromMap(data);

                            notification.setId(document.getId());
                            notifications.add(notification);

                        }
                        listener.onNotificationsLoaded(notifications);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }
    private Notification createNotificationFromMap(Map<String, Object> data) {
        Notification notification = new Notification();

        String title = (String) data.get("title");
        String body = (String) data.get("body");
        String receiverId = (String) data.get("receiverId");
        boolean isRead = (boolean) data.get("read");

        notification.setBody(body);
        notification.setRead(isRead);
        notification.setTitle(title);
        notification.setReceiverId(receiverId);

        return notification;
    }
    public boolean updateNotificationData(Notification notification) {
        try{
        firestore.collection("notifications").document(notification.getId())
                .set(notification)
                .addOnSuccessListener(aVoid -> Log.d("Notification service", "notification updated successfully"))
                .addOnFailureListener(e -> Log.e("Notification service", "Error updating notification", e));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
