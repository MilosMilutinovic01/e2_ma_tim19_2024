package com.example.eventplanner.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.async.OnProductLoadedListener;
import com.example.eventplanner.async.OnProductsLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class ProductService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public ProductService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }
    public boolean createProduct(Product product,  Bitmap image) {
        try{
            saveImage(product.getName(), image, new ImageUploadCallback() {
                @Override
                public void onImageUploadSuccess(String imageURL) {
                    product.setImage(imageURL);
                    firestore.collection("products").add(product)
                            .addOnSuccessListener(documentReference -> {
                                Log.d("ProductService", "Product added with ID: " + documentReference.getId());
                            })
                            .addOnFailureListener(e -> {
                                Log.e("ProductService", "Error adding product", e);
                            });
                }
                @Override
                public void onImageUploadFailure(String errorMessage) {
                    Toast.makeText(context, "Error uploading image: " + errorMessage, Toast.LENGTH_SHORT).show();
                }

            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private String saveImage(String productName, Bitmap image, ImageUploadCallback callback){
        AtomicReference<String> imageURL = new AtomicReference<>(null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        // Create a reference to the image file in Firebase Storage
        StorageReference imageRef = storageRef.child("productImages/"+ productName +".jpg");

        // Upload the byte array to Firebase Storage
        UploadTask uploadTask = imageRef.putBytes(imageBytes);

        // Listen for upload success/failure
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            // Upload successful
            Log.d("TAG", "Image uploaded successfully!");
            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
            result.addOnSuccessListener(uri -> {
                callback.onImageUploadSuccess(uri.toString());
            });
        }).addOnFailureListener(exception -> {
            // Upload failed
            Log.e("TAG", "Image upload failed: " + exception.getMessage());
            callback.onImageUploadFailure(exception.getMessage());
        });

        return imageURL.get();
    }
    public Task<ArrayList<Product>> getProducts() {
        TaskCompletionSource<ArrayList<Product>> taskCompletionSource = new TaskCompletionSource<>();
        firestore.collection("products").get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ArrayList<Product> products = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String productId = document.getId();
                            Product product = document.toObject(Product.class);
                            product.setId(productId);
                            products.add(product);
                        }
                        taskCompletionSource.setResult(products);
                    } else {
                        taskCompletionSource.setException(task.getException());
                    }
                });
        return taskCompletionSource.getTask();
    }

    public boolean editProduct(Product product, Bitmap image) {
        try {
            if (image != null) {
                // If a new image is provided, upload it and update the product image URL
                saveImage(product.getName(), image, new ImageUploadCallback() {
                    @Override
                    public void onImageUploadSuccess(String imageURL) {
                        product.setImage(imageURL);
                        updateProductData(product);
                    }

                    @Override
                    public void onImageUploadFailure(String errorMessage) {
                        Toast.makeText(context, "Error uploading image: " + errorMessage, Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                // If no new image is provided, update the product data without changing the image
                updateProductData(product);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateProductData(Product product) {
        firestore.collection("products").document(product.getId())
                .set(product)
                .addOnSuccessListener(aVoid -> Log.d("ProductService", "Product updated successfully"))
                .addOnFailureListener(e -> Log.e("ProductService", "Error updating product", e));
    }

    public void deleteProduct(String productId) {
        if (productId != null) {
            firestore.collection("products").document(productId)
                    .delete()
                    .addOnSuccessListener(aVoid -> Log.d("ProductService", "Product deleted successfully"))
                    .addOnFailureListener(e -> Log.e("ProductService", "Error deleting product", e));
        } else {
            Log.e("ProductService", "Product ID is null. Unable to delete product.");
        }
    }

    public void getProductsByCompanyId(OnProductsLoadedListener listener, String companyId){
        List<Product> products = new ArrayList<>();
        firestore.collection("products").whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> productData = document.getData();
                            Product product = createProductFromMap(productData);

                            product.setId(document.getId());
                            products.add(product);

                        }
                        listener.onProductsLoaded(products);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    private Product createProductFromMap(Map<String, Object> productData) {
        Product product = new Product();
        String name = (String) productData.get("name");
        String description = (String) productData.get("description");
        double price = (double) productData.get("price");
        double discount = (double) productData.get("discount");
        String image = (String) productData.get("image");
        boolean available = (boolean) productData.get("available");
        boolean visible = (boolean) productData.get("visible");
        String companyId = (String) productData.get("companyId");

        Map<String, Object> categoryMap = (Map<String, Object>) productData.get("category");
        ProductAndServiceCategory category = createProductAndServiceCategoryFromMap(categoryMap); // Assuming a method createCategoryFromMap exists

        Map<String, Object> subcategoryMap = (Map<String, Object>) productData.get("subcategory");
        Subcategory subcategory = createSubcategoryFromMap(subcategoryMap); // Assuming a method createSubcategoryFromMap exists


        List<Map<String, Object>> eventTypeMaps = (List<Map<String, Object>>) productData.get("eventTypes");
        ArrayList<EventType> eventTypes = new ArrayList<>();
        for (Map<String, Object> eventTypeMap : eventTypeMaps) {
            EventType eventType = createEventTypeFromMap(eventTypeMap); // Assuming a method createEventTypeFromMap exists
            eventTypes.add(eventType);
        }

        // Populate the service object with extracted data
        product.setName(name);
        product.setDescription(description);
        product.setPrice(price);
        product.setDiscount(discount);
        product.setImage(image);
        product.setAvailable(available);
        product.setVisible(visible);
        product.setCategory(category);
        product.setSubcategory(subcategory);
        product.setEventTypes(eventTypes);
        product.setCompanyId(companyId);

        return product;
    }
    public static Subcategory createSubcategoryFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        SubcategoryType subcategoryType = SubcategoryType.valueOf((String) map.get("subcategoryType"));
        ProductAndServiceCategory category = createProductAndServiceCategoryFromMap((Map<String, Object>) map.get("category"));
        return new Subcategory(name, description, subcategoryType, category);
    }
    public static ProductAndServiceCategory createProductAndServiceCategoryFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        return new ProductAndServiceCategory(name, description);
    }

    public static EventType createEventTypeFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        boolean blocked = (boolean) map.get("blocked");
        List<String> subcategories = (List<String>) map.get("subcategories");
        return new EventType(name, description, blocked, subcategories);
    }
    public void getProductById(String productId, final OnProductLoadedListener listener) {
        firestore.collection("products").document(productId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Map<String, Object> data = document.getData();
                            Product product = createProductFromMap(data);
                            product.setId(document.getId()); // Assuming Service model has an setId method.
                            listener.onProductLoaded(product);
                        } else {
                            listener.onFailure(new Exception("No product found with ID: " + productId));
                        }
                    } else {
                        listener.onFailure(task.getException());
                    }
                });
    }
}
