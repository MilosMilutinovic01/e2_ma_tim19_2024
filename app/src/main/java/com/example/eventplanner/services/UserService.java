package com.example.eventplanner.services;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.async.OnUserLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserService {
    private FirebaseFirestore firestore;
    private Context context;

    public UserService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
    }

    public void getAllEmployers(OnUsersLoadedListener listener){
        List<User> users = new ArrayList<>();
        firestore.collection("usersCollection")
                .whereEqualTo("role", "EMPLOYER")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> userData = document.getData();
                            User user = createUserFromMap(userData);
                            //User user = document.toObject(User.class);
                            user.setId(document.getId());
                            users.add(user);

                        }
                        listener.onUsersLoaded(users);
                    }
                 else {
                    listener.onFailure(task.getException());
                }
    });
    }

    public void getAllEventOrganizers(OnUsersLoadedListener listener){
        List<User> users = new ArrayList<>();
        firestore.collection("usersCollection")
                .whereEqualTo("role", "EVENT_ORGANIZER")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> userData = document.getData();
                            User user = createUserFromMap(userData);
                            //User user = document.toObject(User.class);
                            user.setId(document.getId());
                            users.add(user);

                        }
                        listener.onUsersLoaded(users);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getAllUsers(OnUsersLoadedListener listener){
        List<User> users = new ArrayList<>();
        firestore.collection("usersCollection")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> userData = document.getData();
                            User user = createUserFromMap(userData);
                            user.setId(document.getId());
                            users.add(user);

                        }
                        listener.onUsersLoaded(users);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }
    private User createUserFromMap(Map<String, Object> userData) {
        String email = (String) userData.get("email");
        String password = (String) userData.get("password");
        String name = (String) userData.get("name");
        String surname = (String) userData.get("surname");
        String phone = (String) userData.get("phone");
        String street = (String) userData.get("street");
        String city = (String) userData.get("city");
        String country = (String) userData.get("country");
        String image = (String) userData.get("profileImage");
        String role = (String) userData.get("role");

        // Deserialize working days
        List<WorkingDay> workingDays = null;
        if (userData.containsKey("workingDays")) {
            List<Map<String, Object>> workingDaysData = (List<Map<String, Object>>) userData.get("workingDays");
            if (workingDaysData != null) {
                workingDays = new ArrayList<>();
                for (Map<String, Object> day : workingDaysData) {
                    WorkingDay workingDay = new WorkingDay();
                    workingDay.deserialize(day);
                    workingDays.add(workingDay);
                }
            }
        }
        User user = new User(email, password, name, surname, phone, street, city, country, image, workingDays);
        user.setRole(Role.valueOf(role));
        return user;
    }

    public Task<User> getUserByCompanyId(String companyId) {
        return firestore.collection("usersCollection")
                .whereEqualTo("role","OWNER").whereEqualTo("companyId", companyId)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            User user= document.toObject(User.class);
                            user.setId(document.getId());
                            return user;
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                    }
                    return null;
                });
    }

    public Task<User> getUserById(String id) {
        return firestore.collection("usersCollection")
                .whereEqualTo("uid", id)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            User user= document.toObject(User.class);
                            user.setId(document.getId());
                            return user;
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                    }
                    return null;
                });
    }


    public Task<User> getAdmin() {
        return firestore.collection("usersCollection")
                .whereEqualTo("role","ADMINISTRATOR")
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            User user= document.toObject(User.class);
                            user.setId(document.getId());
                            return user;
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                    }
                    return null;
                });
    }
}
