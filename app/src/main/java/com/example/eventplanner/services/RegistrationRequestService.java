package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.eventplanner.async.OnRegistrationRequestsLoadedListener;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.RegistrationRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class RegistrationRequestService {
    private FirebaseFirestore firestore;
    private Context context;

    private FirebaseAuth firebaseAuth;

    public RegistrationRequestService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void getAllRegistrationRequests(OnRegistrationRequestsLoadedListener listener) {
        List<RegistrationRequest> requests = new ArrayList<>();
        firestore.collection("registrationRequests")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Map<String, Object> requestData = document.getData();
                            RegistrationRequest request = createRequestFromMap(requestData);
                            requests.add(request);
                        }
                        listener.onRequestsLoaded(requests);
                    } else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void approveRequest(String requestId) {
        firestore.collection("registrationRequests").document(requestId)
                .update("approved", true)
                .addOnSuccessListener(aVoid -> sendActivationEmail(requestId))
                .addOnFailureListener(e -> Log.e("RegistrationRequestService", "Error approving request", e));
    }

    public void rejectRequest(String requestId, String reason) {
        firestore.collection("registrationRequests").document(requestId)
                .update("approved", false, "rejectionReason", reason)
                .addOnSuccessListener(aVoid -> sendRejectionEmail(requestId, reason))
                .addOnFailureListener(e -> Log.e("RegistrationRequestService", "Error rejecting request", e));
    }

    private RegistrationRequest createRequestFromMap(Map<String, Object> requestData) {
        String requestId = (String) requestData.get("requestId");
        String companyName = (String) requestData.get("companyName");
        String ownerFirstName = (String) requestData.get("ownerFirstName");
        String ownerLastName = (String) requestData.get("ownerLastName");
        String companyEmail = (String) requestData.get("companyEmail");
        String ownerEmail = (String) requestData.get("ownerEmail");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
        LocalDateTime requestTime = LocalDateTime.parse((String) requestData.get("requestTime"), formatter);
        List<String> categories =  (List<String>) requestData.get("categories");
        List<String> eventTypes =  (List<String>) requestData.get("eventTypes");
        Boolean approved = (Boolean) requestData.get("approved");

        RegistrationRequest request = new RegistrationRequest(requestId, companyName, ownerFirstName, ownerLastName, companyEmail, ownerEmail, requestTime, categories, eventTypes);
        request.setApproved(approved);
        return request;
    }

    // Dummy methods for sending emails
    private void sendActivationEmail(String requestId) {
    }

    private void sendRejectionEmail(String requestId, String reason) {
    }

    // Additional methods to search/filter registration requests
    public List<RegistrationRequest> searchRequests(List<RegistrationRequest> requests, String query) {
        List<RegistrationRequest> result = new ArrayList<>();
        for (RegistrationRequest request : requests) {
            // Check if the query matches any part of the company name, owner name, or email
            if (request.getCompanyName().toLowerCase().contains(query.toLowerCase()) ||
                    (request.getOwnerFirstName().toLowerCase().contains(query.toLowerCase()) &&
                            request.getOwnerLastName().toLowerCase().contains(query.toLowerCase())) ||
                    request.getCompanyEmail().toLowerCase().contains(query.toLowerCase()) ||
                    request.getOwnerEmail().toLowerCase().contains(query.toLowerCase())) {
                result.add(request);
            }
        }
        return result;
    }
}
