package com.example.eventplanner.services;

import android.content.Context;

import com.example.eventplanner.async.CompanyLoadedListener;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.WorkingDay;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class CompanyService {
    private FirebaseFirestore firestore;
    private Context context;

    public CompanyService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
    }

    public void getCompanyById(String companyId, CompanyLoadedListener listener){
        AtomicReference<Company> foundCompany = new AtomicReference<>(new Company());
        firestore.collection("companiesCollection")
                .whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            // Since we expect only one document, get the first one
                            QueryDocumentSnapshot document = (QueryDocumentSnapshot) querySnapshot.getDocuments().get(0);
                            Map<String, Object> companyData = document.getData();
                            Company company = createCompanyFromMap(companyData);
                            company.setId(document.getId());
                            listener.onCompanyLoaded(company);
                        } else {
                            // No company found with the provided ID
                            listener.onFailure(new Exception("Company not found"));
                        }
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    private Company createCompanyFromMap(Map<String, Object> companyData) {
        String email = (String) companyData.get("email");
        String name = (String) companyData.get("name");
        String description = (String) companyData.get("description");
        String phone = (String) companyData.get("phone");
        String street = (String) companyData.get("street");
        String city = (String) companyData.get("city");
        String country = (String) companyData.get("country");
        String uid = (String) companyData.get("uid");

        // Deserialize working days
        List<WorkingDay> workingDays = null;
        if (companyData.containsKey("workingDays")) {
            List<Map<String, Object>> workingDaysData = (List<Map<String, Object>>) companyData.get("workingDays");
            if (workingDaysData != null) {
                workingDays = new ArrayList<>();
                for (Map<String, Object> day : workingDaysData) {
                    WorkingDay workingDay = new WorkingDay();
                    workingDay.deserialize(day);
                    workingDays.add(workingDay);
                }
            }
        }
        Company company = new Company(email, name, description, phone, street, city, country, workingDays);
        company.setId(uid);

        return company;
    }
}
