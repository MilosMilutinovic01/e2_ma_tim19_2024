package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.eventplanner.async.EventTypeUploadCallback;
import com.example.eventplanner.async.OnRatingsLoadedListener;
import com.example.eventplanner.async.OnReportEditedListener;
import com.example.eventplanner.async.OnReportsLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.RatingReport;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RatingsService {
    private FirebaseFirestore firestore;
    private Context context;

    public RatingsService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
    }

    public boolean createCompanyRating(String companyId, String rating, String comment){
        try{
            Map<String, Object> ratingData = new HashMap<>();
            ratingData.put("companyId", companyId);
            ratingData.put("rating", rating);
            ratingData.put("comment", comment);


            Date input = new Date();
            LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            String formattedDate = date.format(formatter);
            ratingData.put("date", formattedDate);


            firestore.collection("ratingsCollection").add(ratingData)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("Ratings service", "Rating added with ID: " + documentReference.getId());
                        ratingData.put("id", documentReference.getId());

                        // Update the document with the custom ID
                        firestore.collection("ratingsCollection").document(documentReference.getId()).set(ratingData)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d("Ratings Service", "Rating updated with custom ID: " + documentReference.getId());
                                })
                                .addOnFailureListener(e -> {
                                    Log.e("Ratings Service", "Error updating Rating with custom ID", e);
                                });
                    })
                    .addOnFailureListener(e -> {
                        Log.e("Ratings Service", "Error adding Rating", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getAllRatings(String companyId, OnRatingsLoadedListener listener){
        List<CompanyRating> ratings = new ArrayList<>();
        firestore.collection("ratingsCollection")
                .whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> userData = document.getData();
                            CompanyRating rating = createRatingFromMap(userData);
                            //rating.setId(document.getId());
                            ratings.add(rating);

                        }
                        listener.onRatingsLoaded(ratings);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    private CompanyRating createRatingFromMap(Map<String, Object> ratingData) {
        try {
            int rating = Integer.valueOf((String)ratingData.get("rating"));
            String id = (String) ratingData.get("id");
            String comment = (String) ratingData.get("comment");
            String companyId = (String) ratingData.get("companyId");

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            LocalDate date = LocalDate.parse((String)ratingData.get("date"), formatter);
            CompanyRating companyRating = new CompanyRating(rating, comment, date, companyId);
            companyRating.setId(id);
            return companyRating;
        } catch (DateTimeParseException e) {
            System.err.println("Invalid date format: " + (String)ratingData.get("date"));
            return null;
        }
    }


    public boolean createRatingReport(String ratingId, String reportReason, String reporterId){

        try{
            Map<String, Object> reportData = new HashMap<>();
            reportData.put("ratingId", ratingId);
            reportData.put("reporterId", reporterId);
            reportData.put("reportReason", reportReason);


            Date input = new Date();
            LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            String formattedDate = date.format(formatter);
            reportData.put("reportDate", formattedDate);

            reportData.put("status", RatingReport.Status.REPORTED);

            firestore.collection("reportsCollection").add(reportData)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("Ratings service", "Report added with ID: " + documentReference.getId());
                        reportData.put("id", documentReference.getId());

                        // Update the document with the custom ID
                        firestore.collection("reportsCollection").document(documentReference.getId()).set(reportData)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d("Ratings Service", "Report updated with custom ID: " + documentReference.getId());
                                })
                                .addOnFailureListener(e -> {
                                    Log.e("Ratings Service", "Error updating Report with custom ID", e);
                                });
                    })
                    .addOnFailureListener(e -> {
                        Log.e("Ratings Service", "Error adding Report", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getAllReports(OnReportsLoadedListener listener){
        List<RatingReport> reports = new ArrayList<>();
        firestore.collection("reportsCollection")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> reportData = document.getData();
                            RatingReport report = createReportFromMap(reportData);
                            //rating.setId(document.getId());
                            reports.add(report);

                        }
                        listener.onReportsLoaded(reports);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    private RatingReport createReportFromMap(Map<String, Object> reportData) {
        try {
            String id = (String) reportData.get("id");
            String ratingId = (String) reportData.get("ratingId");
            String reportReason = (String) reportData.get("reportReason");
            RatingReport.Status status = RatingReport.Status.valueOf((String) reportData.get("status"));
            String reporterId = (String) reportData.get("reporterId");

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            LocalDate date = LocalDate.parse((String)reportData.get("reportDate"), formatter);
            RatingReport report = new RatingReport(ratingId, reportReason, date, status, reporterId);
            report.setId(id);
            return report;
        } catch (DateTimeParseException e) {
            System.err.println("Invalid date format: " + (String)reportData.get("date"));
            return null;
        }
    }

    public boolean deleteRating(String ratingId) {
        try {
            firestore.collection("ratingsCollection").document(ratingId)
                    .delete()
                    .addOnSuccessListener(aVoid -> {
                        Log.d("RatingService", "Rating deleted successfully with ID: " + ratingId);
                    })
                    .addOnFailureListener(e -> {
                        Log.e("RatingService", "Error deleting Rating", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateReport(RatingReport report, OnReportEditedListener listener) {
        try {
            // Get the ID of the category
            String reportId = report.getId();
            if (reportId == null) {
                Log.e("RatingService", "Report ID is null. Cannot update.");
                return;
            }

            Map<String, Object> reportData = new HashMap<>();
            reportData.put("ratingId", report.getRatingId());
            reportData.put("reporterId", report.getReporterId());
            reportData.put("reportReason", report.getReportReason());
            reportData.put("status", report.getStatus());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
            String formattedDate = report.getReportDate().format(formatter);
            reportData.put("reportDate", formattedDate);


            // Update the category document in Firestore
            firestore.collection("reportsCollection").document(reportId)
                    .set(reportData)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()) {
                            listener.onReportEdited(report);
                        }
                        else {
                            listener.onFailure(task.getException());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
