package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public CategoryService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }

    public boolean createCategory(ProductAndServiceCategory category) {
        try{
            firestore.collection("productAndServiceCategories").add(category)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("CategoryService", "Category added with ID: " + documentReference.getId());
                        category.setId(documentReference.getId());

                        // Update the document with the custom ID
                        firestore.collection("productAndServiceCategories").document(documentReference.getId()).set(category)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d("CategoryService", "Category updated with custom ID: " + documentReference.getId());
                                })
                                .addOnFailureListener(e -> {
                                    Log.e("CategoryService", "Error updating category with custom ID", e);
                                });
                    })
                    .addOnFailureListener(e -> {
                        Log.e("CategoryService", "Error adding category", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getAllCategories(OnCategoriesLoadedListener listener){
        List<ProductAndServiceCategory> categories = new ArrayList<>();
        firestore.collection("productAndServiceCategories")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> categoryData = document.getData();
                            ProductAndServiceCategory productAndServiceCategory = createCategoryFromMap(categoryData);

                            productAndServiceCategory.setId(document.getId());
                            categories.add(productAndServiceCategory);

                        }
                        listener.onCategoriesLoaded(categories);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void updateCategory(ProductAndServiceCategory category, CategoryUploadCallback callback) {
        try {
            // Get the ID of the category
            String categoryId = category.getId();
            if (categoryId == null) {
                Log.e("CategoryService", "Category ID is null. Cannot update.");
                return;
            }

            // Update the category document in Firestore
            firestore.collection("productAndServiceCategories").document(categoryId)
                    .set(category)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("CategoryService", "Category updated successfully with ID: " + categoryId);
                        callback.onSuccess();
                    })
                    .addOnFailureListener(e -> {
                        Log.e("CategoryService", "Error updating category", e);
                        callback.onFailure(e.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.getMessage());
        }
    }

    public void deleteCategory(String categoryId, CategoryUploadCallback callback) {
        try {
            // Delete the category document from Firestore
            firestore.collection("productAndServiceCategories").document(categoryId)
                    .delete()
                    .addOnSuccessListener(aVoid -> {
                        Log.d("CategoryService", "Category deleted successfully with ID: " + categoryId);
                        callback.onSuccess();
                    })
                    .addOnFailureListener(e -> {
                        Log.e("CategoryService", "Error deleting category", e);
                        callback.onFailure(e.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.getMessage());
        }
    }


    private ProductAndServiceCategory createCategoryFromMap(Map<String, Object> categoryData) {
        String description = (String) categoryData.get("description");
        String name = (String) categoryData.get("name");

        return new ProductAndServiceCategory(name, description);
    }
}
