package com.example.eventplanner.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.async.OnProductLoadedListener;
import com.example.eventplanner.async.OnServiceLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class ServiceService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public ServiceService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }
    public boolean createService(Service service,  Bitmap image) {
        try{
            saveImage(service.getName(), image, new ImageUploadCallback() {
                @Override
                public void onImageUploadSuccess(String imageURL) {
                    service.setImage(imageURL);
                    firestore.collection("services").add(service)
                            .addOnSuccessListener(documentReference -> {
                                Log.d("ServiceService", "Service added with ID: " + documentReference.getId());
                            })
                            .addOnFailureListener(e -> {
                                Log.e("ServiceService", "Error adding service", e);
                            });
                }
                @Override
                public void onImageUploadFailure(String errorMessage) {
                    Toast.makeText(context, "Error uploading image: " + errorMessage, Toast.LENGTH_SHORT).show();
                }

            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private String saveImage(String productName, Bitmap image, ImageUploadCallback callback){
        AtomicReference<String> imageURL = new AtomicReference<>(null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        // Create a reference to the image file in Firebase Storage
        StorageReference imageRef = storageRef.child("serviceImages/"+ productName +".jpg");

        // Upload the byte array to Firebase Storage
        UploadTask uploadTask = imageRef.putBytes(imageBytes);

        // Listen for upload success/failure
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            // Upload successful
            Log.d("TAG", "Image uploaded successfully!");
            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
            result.addOnSuccessListener(uri -> {
                callback.onImageUploadSuccess(uri.toString());
            });
        }).addOnFailureListener(exception -> {
            // Upload failed
            Log.e("TAG", "Image upload failed: " + exception.getMessage());
            callback.onImageUploadFailure(exception.getMessage());
        });

        return imageURL.get();
    }

    public boolean editService(Service service, Bitmap image) {
        try {
            if (image != null) {
                // If a new image is provided, upload it and update the product image URL
                saveImage(service.getName(), image, new ImageUploadCallback() {
                    @Override
                    public void onImageUploadSuccess(String imageURL) {
                        service.setImage(imageURL);
                        updateServiceData(service);
                    }

                    @Override
                    public void onImageUploadFailure(String errorMessage) {
                        Toast.makeText(context, "Error uploading image: " + errorMessage, Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                // If no new image is provided, update the product data without changing the image
                updateServiceData(service);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public void updateServiceData(Service service) {
        firestore.collection("services").document(service.getId())
                .set(service)
                .addOnSuccessListener(aVoid -> Log.d("ServiceService", "Service updated successfully"))
                .addOnFailureListener(e -> Log.e("ServiceService", "Error updating service", e));
    }

    public void deleteService(String serviceId) {
        if (serviceId != null) {
            firestore.collection("services").document(serviceId)
                    .delete()
                    .addOnSuccessListener(aVoid -> Log.d("ServiceService", "Service deleted successfully"))
                    .addOnFailureListener(e -> Log.e("ServiceService", "Error deleting service", e));
        } else {
            Log.e("ServiceService", "Service ID is null. Unable to delete services.");
        }
    }
    public void getAllServices(OnServicesLoadedListener listener){
        List<Service> services = new ArrayList<>();
        firestore.collection("services")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> serviceData = document.getData();
                            Service service = createServiceFromMap(serviceData);

                            service.setId(document.getId());
                            services.add(service);

                        }
                        listener.onServicesLoaded(services);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    private Service createServiceFromMap(Map<String, Object> serviceData) {
        Service service = new Service();
        String name = (String) serviceData.get("name");
        String description = (String) serviceData.get("description");
        String specificities = (String) serviceData.get("specificities");
        double price = (double) serviceData.get("price");
        double discount = (double) serviceData.get("discount");
        long duration = (long) serviceData.get("duration");
        String cancellationDeadline = (String) serviceData.get("cancellationDeadline");
        String bookingDeadline = (String) serviceData.get("bookingDeadline");
        String image = (String) serviceData.get("image");
        boolean available = (boolean) serviceData.get("available");
        boolean visible = (boolean) serviceData.get("visible");
        String companyId = (String) serviceData.get("companyId");

        Map<String, Object> categoryMap = (Map<String, Object>) serviceData.get("category");
        ProductAndServiceCategory category = createProductAndServiceCategoryFromMap(categoryMap); // Assuming a method createCategoryFromMap exists

        Map<String, Object> subcategoryMap = (Map<String, Object>) serviceData.get("subcategory");
        Subcategory subcategory = createSubcategoryFromMap(subcategoryMap); // Assuming a method createSubcategoryFromMap exists

        // Extracting employers and eventTypes fields
        List<Map<String, Object>> employerMaps = (List<Map<String, Object>>) serviceData.get("employers");
        ArrayList<User> employers = new ArrayList<>();
        for (Map<String, Object> employerMap : employerMaps) {
            User employer = createUserFromMap(employerMap); // Assuming a method createUserFromMap exists
            employers.add(employer);
        }

        List<Map<String, Object>> eventTypeMaps = (List<Map<String, Object>>) serviceData.get("eventTypes");
        ArrayList<EventType> eventTypes = new ArrayList<>();
        for (Map<String, Object> eventTypeMap : eventTypeMaps) {
            EventType eventType = createEventTypeFromMap(eventTypeMap); // Assuming a method createEventTypeFromMap exists
            eventTypes.add(eventType);
        }

        // Populate the service object with extracted data
        service.setName(name);
        service.setDescription(description);
        service.setSpecificities(specificities);
        service.setPrice(price);
        service.setDiscount(discount);
        service.setDuration(duration);
        service.setCancellationDeadline(cancellationDeadline);
        service.setBookingDeadline(bookingDeadline);
        service.setImage(image);
        service.setAvailable(available);
        service.setVisible(visible);
        service.setCategory(category);
        service.setSubcategory(subcategory);
        service.setEmployers(employers);
        service.setEventTypes(eventTypes);
        service.setCompanyId(companyId);

        return service;
    }
    public static Subcategory createSubcategoryFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        SubcategoryType subcategoryType = SubcategoryType.valueOf((String) map.get("subcategoryType"));
        ProductAndServiceCategory category = createProductAndServiceCategoryFromMap((Map<String, Object>) map.get("category"));
        return new Subcategory(name, description, subcategoryType, category);
    }
    public static ProductAndServiceCategory createProductAndServiceCategoryFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        return new ProductAndServiceCategory(name, description);
    }

    public static EventType createEventTypeFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        boolean blocked = (boolean) map.get("blocked");
        List<String> subcategories = (List<String>) map.get("subcategories");
        return new EventType(name, description, blocked, subcategories);
    }
    private User createUserFromMap(Map<String, Object> userData) {
        String email = (String) userData.get("email");
        String password = (String) userData.get("password");
        String name = (String) userData.get("name");
        String surname = (String) userData.get("surname");
        String phone = (String) userData.get("phone");
        String street = (String) userData.get("street");
        String city = (String) userData.get("city");
        String country = (String) userData.get("country");
        String image = (String) userData.get("profileImage");

        // Deserialize working days
        List<WorkingDay> workingDays = null;
        return new User(email, password, name, surname, phone, street, city, country, image, workingDays);
    }
    public void getServicesByCompanyId(OnServicesLoadedListener listener, String companyId){
        List<Service> services = new ArrayList<>();
        firestore.collection("services").whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> serviceData = document.getData();
                            Service service = createServiceFromMap(serviceData);

                            service.setId(document.getId());
                            services.add(service);

                        }
                        listener.onServicesLoaded(services);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getServiceById(String productId, final OnServiceLoadedListener listener) {
        firestore.collection("services").document(productId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Map<String, Object> data = document.getData();
                            Service service = createServiceFromMap(data);
                            service.setId(document.getId()); // Assuming Service model has an setId method.
                            listener.onServiceLoaded(service);
                        } else {
                            listener.onFailure(new Exception("No product found with ID: " + productId));
                        }
                    } else {
                        listener.onFailure(task.getException());
                    }
                });
    }


}
