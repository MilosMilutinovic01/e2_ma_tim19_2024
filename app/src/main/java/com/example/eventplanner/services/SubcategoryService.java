package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.async.OnSubcategoriesLoadedListener;
import com.example.eventplanner.async.SubcategoryUploadCallback;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubcategoryService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public SubcategoryService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }

    public boolean createSubcategory(Subcategory subcategory) {
        try{
            firestore.collection("productAndServiceSubcategories").add(subcategory)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("SubcategoryService", "Subcategory added with ID: " + documentReference.getId());
                        subcategory.setId(documentReference.getId());

                        // Update the document with the custom ID
                        firestore.collection("productAndServiceSubcategories").document(documentReference.getId()).set(subcategory)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d("SubcategoryService", "Subcategory updated with custom ID: " + documentReference.getId());
                                })
                                .addOnFailureListener(e -> {
                                    Log.e("SubcategoryService", "Error updating Subcategory with custom ID", e);
                                });
                    })
                    .addOnFailureListener(e -> {
                        Log.e("SubcategoryService", "Error adding Subcategory", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getAllSubcategories(ProductAndServiceCategory category, OnSubcategoriesLoadedListener listener){
        List<Subcategory> subcategories = new ArrayList<>();
        firestore.collection("productAndServiceSubcategories")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> SubcategoryData = document.getData();
                            Subcategory subcategory = createSubcategoryFromMap(SubcategoryData, category);

                            subcategory.setId(document.getId());
                            subcategories.add(subcategory);

                        }
                        listener.onSubcategoriesLoaded(subcategories);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void updateSubcategory(Subcategory Subcategory, SubcategoryUploadCallback callback) {
        try {
            // Get the ID of the Subcategory
            String SubcategoryId = Subcategory.getId();
            if (SubcategoryId == null) {
                Log.e("SubcategoryService", "Subcategory ID is null. Cannot update.");
                return;
            }

            // Update the Subcategory document in Firestore
            firestore.collection("productAndServiceSubcategories").document(SubcategoryId)
                    .set(Subcategory)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("SubcategoryService", "Subcategory updated successfully with ID: " + SubcategoryId);
                        callback.onSuccess();
                    })
                    .addOnFailureListener(e -> {
                        Log.e("SubcategoryService", "Error updating Subcategory", e);
                        callback.onFailure(e.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.getMessage());
        }
    }

    public void deleteSubcategory(String SubcategoryId, SubcategoryUploadCallback callback) {
        try {
            // Delete the Subcategory document from Firestore
            firestore.collection("productAndServiceSubcategories").document(SubcategoryId)
                    .delete()
                    .addOnSuccessListener(aVoid -> {
                        Log.d("SubcategoryService", "Subcategory deleted successfully with ID: " + SubcategoryId);
                        callback.onSuccess();
                    })
                    .addOnFailureListener(e -> {
                        Log.e("SubcategoryService", "Error deleting Subcategory", e);
                        callback.onFailure(e.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.getMessage());
        }
    }


    private Subcategory createSubcategoryFromMap(Map<String, Object> SubcategoryData, ProductAndServiceCategory category) {
        String description = (String) SubcategoryData.get("description");
        String name = (String) SubcategoryData.get("name");
        SubcategoryType type = SubcategoryType.valueOf((String) SubcategoryData.get("subcategoryType"));

        return new Subcategory(name, description, type, category);
    }
}
