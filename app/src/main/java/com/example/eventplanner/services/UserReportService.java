package com.example.eventplanner.services;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.async.OnBundlesLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.async.OnUserBlockedCheckListener;
import com.example.eventplanner.async.OnUserReportsLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.Bundle;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.example.eventplanner.model.WorkingDay;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class UserReportService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public UserReportService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }
    private UserReport createReportFromMap(Map<String, Object> reportData) {
        UserReport userReport = new UserReport();
        String reporterId = (String) reportData.get("reporterId");
        String reportedId = (String) reportData.get("reportedId");
        String reason = (String) reportData.get("reason");
        String statusString = (String) reportData.get("status");
        UserReportStatus statusEnum = UserReportStatus.valueOf(statusString);

        Map<String, Object> dateMap = (Map<String, Object>) reportData.get("date");
        int year = Integer.parseInt(dateMap.get("year").toString());
        int month = Integer.parseInt(dateMap.get("monthValue").toString());
        int day = Integer.parseInt(dateMap.get("dayOfMonth").toString());
        LocalDate reportDate = LocalDate.of(year, month, day);

        String rejectReason = (String) reportData.get("rejectReason");

        Map<String, Object> reporterMap = (Map<String, Object>) reportData.get("reporter");
        User reporter = createUserFromMap(reporterMap);

        Map<String, Object> reportedMap = (Map<String, Object>) reportData.get("reported");
        User reported = createUserFromMap(reportedMap);

        userReport.setReportedId(reportedId);
        userReport.setReporterId(reporterId);
        userReport.setReported(reported);
        userReport.setReporter(reporter);
        userReport.setReason(reason);
        userReport.setStatus(statusEnum);
        userReport.setDate(reportDate);
        userReport.setRejectReason(rejectReason);

        return userReport;
    }

    public void updateUserReportData(UserReport report) {
        firestore.collection("userReports").document(report.getId())
                .set(report)
                .addOnSuccessListener(aVoid -> Log.d("UserReportService", "UserReport updated successfully"))
                .addOnFailureListener(e -> Log.e("UserReportService", "Error updating UserReport", e));
    }

    public boolean createUserReport(UserReport userReport) {
        try{
            firestore.collection("userReports").add(userReport)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("UserReportService", "UserReport added with ID: " + documentReference.getId());
                    })
                    .addOnFailureListener(e -> {
                        Log.e("UserReportService", "Error adding UserReport", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public void getAllUserReports(OnUserReportsLoadedListener listener){
        List<UserReport> userReports = new ArrayList<>();
        firestore.collection("userReports")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> data = document.getData();
                            UserReport userReport = createReportFromMap(data);

                            userReport.setId(document.getId());
                            userReports.add(userReport);

                        }
                        listener.onUserReportsLoaded(userReports);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    private User createUserFromMap(Map<String, Object> userData) {
        String email = (String) userData.get("email");
        String password = (String) userData.get("password");
        String name = (String) userData.get("name");
        String surname = (String) userData.get("surname");
        String phone = (String) userData.get("phone");
        String street = (String) userData.get("street");
        String city = (String) userData.get("city");
        String country = (String) userData.get("country");
        String image = (String) userData.get("profileImage");
        String role = (String) userData.get("role");
        String companyId = (String) userData.get("companyId");
        String id = (String) userData.get("id");

        // Deserialize working days
        List<WorkingDay> workingDays = null;
        if (userData.containsKey("workingDays")) {
            List<Map<String, Object>> workingDaysData = (List<Map<String, Object>>) userData.get("workingDays");
            if (workingDaysData != null) {
                workingDays = new ArrayList<>();
                for (Map<String, Object> day : workingDaysData) {
                    WorkingDay workingDay = new WorkingDay();
                    workingDay.deserialize(day);
                    workingDays.add(workingDay);
                }
            }
        }
        User user = new User(email, password, name, surname, phone, street, city, country, image, workingDays);
        user.setRole(Role.valueOf(role));
        user.setCompanyId(companyId);
        user.setId(id);
        return user;
    }
    public void isUserBlocked(String reportedEmail, OnUserBlockedCheckListener listener) {
        firestore.collection("userReports")
                .whereEqualTo("reported.email", reportedEmail)
                .whereEqualTo("status", "ACCEPTED")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null && !task.getResult().isEmpty()) {
                        // If we find at least one document, the user is blocked
                        listener.onUserBlockedCheck(true);
                    } else {
                        if (task.getException() != null) {
                            Log.e("UserReportService", "Error getting documents: ", task.getException());
                        }
                        // No document found or an error occurred
                        listener.onUserBlockedCheck(false);
                    }
                });
    }


}
