package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.async.CategoryUploadCallback;
import com.example.eventplanner.async.EventTypeUploadCallback;
import com.example.eventplanner.async.OnCategoriesLoadedListener;
import com.example.eventplanner.async.OnEventTypesLoadedListener;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EventTypeService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public EventTypeService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }

    public boolean createEventType(EventType eventType) {
        try{
            firestore.collection("eventTypesCollection").add(eventType)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("CategoryService", "Category added with ID: " + documentReference.getId());
                        eventType.setId(documentReference.getId());

                        // Update the document with the custom ID
                        firestore.collection("eventTypesCollection").document(documentReference.getId()).set(eventType)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d("CategoryService", "Category updated with custom ID: " + documentReference.getId());
                                })
                                .addOnFailureListener(e -> {
                                    Log.e("CategoryService", "Error updating category with custom ID", e);
                                });
                    })
                    .addOnFailureListener(e -> {
                        Log.e("CategoryService", "Error adding category", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getAllEventTypes(OnEventTypesLoadedListener listener){
        List<EventType> eventTypes = new ArrayList<>();
        firestore.collection("eventTypesCollection")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> eventTypeData = document.getData();
                            EventType eventType = createEventTypeFromMap(eventTypeData);

                            eventType.setId(document.getId());
                            eventTypes.add(eventType);
                        }
                        listener.onEventTypesLoaded(eventTypes);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void updateEventType(EventType eventType, EventTypeUploadCallback callback) {
        try {
            // Get the ID of the category
            String eventTypeId = eventType.getId();
            if (eventTypeId == null) {
                Log.e("CategoryService", "Category ID is null. Cannot update.");
                return;
            }

            // Update the category document in Firestore
            firestore.collection("eventTypesCollection").document(eventTypeId)
                    .set(eventType)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("CategoryService", "Category updated successfully with ID: " + eventTypeId);
                        callback.onSuccess();
                    })
                    .addOnFailureListener(e -> {
                        Log.e("CategoryService", "Error updating category", e);
                        callback.onFailure(e.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.getMessage());
        }
    }

    public void deleteEventType(String eventTypeId, EventTypeUploadCallback callback) {
        try {
            // Delete the category document from Firestore
            firestore.collection("eventTypesCollection").document(eventTypeId)
                    .delete()
                    .addOnSuccessListener(aVoid -> {
                        Log.d("CategoryService", "Category deleted successfully with ID: " + eventTypeId);
                        callback.onSuccess();
                    })
                    .addOnFailureListener(e -> {
                        Log.e("CategoryService", "Error deleting category", e);
                        callback.onFailure(e.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.getMessage());
        }
    }


    private EventType createEventTypeFromMap(Map<String, Object> eventTypeData) {
        String name = (String) eventTypeData.get("name");
        String description = (String) eventTypeData.get("description");
        boolean blocked = (boolean) eventTypeData.get("blocked");
        List<String> subcategories = (List<String>) eventTypeData.get("subcategories");

        return new EventType(name, description, blocked, subcategories);
    }

}
