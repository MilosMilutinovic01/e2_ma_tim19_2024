package com.example.eventplanner.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.async.OnEmployerEventsLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployerEventService {
    private FirebaseFirestore firestore;
    private Context context;

    public EmployerEventService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
    }

    public void getAllEmployerEvents(String employerId, String date, OnEmployerEventsLoadedListener listener){
        List<EmployerEvent> employerEvents = new ArrayList<>();
        firestore.collection("employerEventsCollection")
                .whereEqualTo("employerId", employerId)
                .whereEqualTo("date", date)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> eventsData = document.getData();
                            EmployerEvent event = new EmployerEvent();
                            event.deserialize(eventsData);
                            //User user = document.toObject(User.class);
                            event.setId(document.getId());
                            employerEvents.add(event);

                        }
                        listener.onEventsLoaded(employerEvents);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
            });
    }

    public void getAll(OnEmployerEventsLoadedListener listener){
        List<EmployerEvent> employerEvents = new ArrayList<>();
        firestore.collection("employerEventsCollection")
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> eventsData = document.getData();
                            EmployerEvent event = new EmployerEvent();
                            event.deserialize(eventsData);
                            //User user = document.toObject(User.class);
                            event.setId(document.getId());
                            employerEvents.add(event);

                        }
                        listener.onEventsLoaded(employerEvents);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }


    public boolean createEmployerEvent(EmployerEvent event) {
        try{
            Map<String, Object> eventData = event.serialize();
            firestore.collection("employerEventsCollection").add(eventData)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("EmployerEventService", "Event added with ID: " + documentReference.getId());
                    })
                    .addOnFailureListener(e -> {
                        Log.e("EmployerEventService", "Error adding event", e);
                    });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

}
