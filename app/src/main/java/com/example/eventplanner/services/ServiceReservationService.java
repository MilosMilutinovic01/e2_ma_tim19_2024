package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.async.OnRegistrationRequestsLoadedListener;
import com.example.eventplanner.async.OnServiceReservationsLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;import android.widget.Toast;

public class ServiceReservationService {
    private FirebaseFirestore firestore;
    private Context context;

    private FirebaseAuth firebaseAuth;

    public ServiceReservationService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void saveServiceReservation(ServiceReservation reservation) {
        Map<String, Object> reservationData = createReservationMap(reservation);

        firestore.collection("service_reservations")
                .add(reservationData)
                .addOnSuccessListener(documentReference -> {
                    String documentId = documentReference.getId();
                    Log.d("TAG", "Service reservation added with ID: " + documentId);
                    reservation.setId(documentId); // Assuming ServiceReservation has a setId method

                    // Optionally, update the reservation with the new ID in Firestore
                    firestore.collection("service_reservations")
                            .document(documentId)
                            .set(createReservationMap(reservation))
                            .addOnSuccessListener(aVoid -> {
                                Log.d("TAG", "Service reservation updated with document ID");
                                showToast("Service reservation added successfully with ID");
                            })
                            .addOnFailureListener(e -> {
                                Log.w("TAG", "Error updating service reservation with document ID", e);
                                showToast("Error updating service reservation with document ID");
                            });
                })
                .addOnFailureListener(e -> {
                    Log.w("TAG", "Error adding service reservation", e);
                    showToast("Error adding service reservation");
                });
    }


    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void getAllServiceReservations(OnServiceReservationsLoadedListener listener) {
        List<ServiceReservation> reservations = new ArrayList<>();
        firestore.collection("service_reservations")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Map<String, Object> rezervationData = document.getData();
                            ServiceReservation reservation = createReservationFromMap(rezervationData);
                            reservations.add(reservation);
                        }
                        if (listener != null) {
                            listener.OnServiceReservationsLoaded(reservations);
                        }
                    } else {
                        Log.e("TAG", "Error getting service reservations", task.getException());
                        if (listener != null) {
                            listener.onFailure(task.getException());
                        }
                    }
                });
    }

    private ServiceReservation createReservationFromMap(Map<String, Object> requestData) {
        String id = (String) requestData.get("id");
        String eventId = (String) requestData.get("eventId");
        String serviceName = (String) requestData.get("serviceName");
        String serviceDescription = (String) requestData.get("serviceDescription");
        String employeeId = (String) requestData.get("employeeId");
        String eventOrganizerId = (String) requestData.get("eventOrganizerId");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
        LocalDateTime eventDate = LocalDateTime.parse((String) requestData.get("eventDate"), formatter);
        LocalDateTime serviceStartTime = LocalDateTime.parse((String) requestData.get("serviceStartTime"), formatter);
        LocalDateTime serviceEndTime = LocalDateTime.parse((String) requestData.get("serviceEndTime"), formatter);

        boolean notificationSent = (boolean) requestData.get("notificationSent");
        boolean rejected = (boolean) requestData.get("rejected");

        return new ServiceReservation(id,eventId, serviceName, serviceDescription, employeeId,
                eventDate, serviceStartTime, serviceEndTime, notificationSent,rejected,eventOrganizerId);
    }

    public Map<String, Object> createReservationMap(ServiceReservation reservation) {
        Map<String, Object> reservationData = new HashMap<>();
        reservationData.put("eventId", reservation.getEventId());
        reservationData.put("serviceName", reservation.getServiceName());
        reservationData.put("serviceDescription", reservation.getServiceDescription());
        reservationData.put("employeeId", reservation.getEmployeeId());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
        reservationData.put("eventDate", reservation.getEventDate().format(formatter));
        reservationData.put("serviceStartTime", reservation.getServiceStartTime().format(formatter));
        reservationData.put("serviceEndTime", reservation.getServiceEndTime().format(formatter));

        reservationData.put("notificationSent", reservation.isNotificationSent());
        reservationData.put("rejected", reservation.isRejected());
        reservationData.put("eventOrganizerId", reservation.getEventOrganizerId());

        return reservationData;
    }
    public void updateServiceReservationData(ServiceReservation service) {
        firestore.collection("service_reservations").document(service.getId())
                .set(service)
                .addOnSuccessListener(aVoid -> Log.d("ServiceService", "Service updated successfully"))
                .addOnFailureListener(e -> Log.e("ServiceService", "Error updating service", e));
    }
    public void getActiveByEventOrganizerId(OnServiceReservationsLoadedListener listener, String eventOrganizerId){
        List<ServiceReservation> services = new ArrayList<>();
        firestore.collection("service_reservations").whereEqualTo("eventOrganizerId", eventOrganizerId).whereEqualTo("rejected",false)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> serviceData = document.getData();
                            ServiceReservation service = createReservationFromMap(serviceData);

                            service.setId(document.getId());
                            services.add(service);

                        }
                        listener.OnServiceReservationsLoaded(services);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getActiveByEmployerId(OnServiceReservationsLoadedListener listener, String eventOrganizerId){
        List<ServiceReservation> services = new ArrayList<>();
        firestore.collection("service_reservations").whereEqualTo("employerId", eventOrganizerId).whereEqualTo("rejected",false)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> serviceData = document.getData();
                            ServiceReservation service = createReservationFromMap(serviceData);

                            service.setId(document.getId());
                            services.add(service);

                        }
                        listener.OnServiceReservationsLoaded(services);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }



}
