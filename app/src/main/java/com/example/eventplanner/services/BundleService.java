package com.example.eventplanner.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.async.OnBundlesLoadedListener;
import com.example.eventplanner.async.OnServicesLoadedListener;
import com.example.eventplanner.async.OnUsersLoadedListener;
import com.example.eventplanner.model.Bundle;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.SubcategoryType;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingDay;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class BundleService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public BundleService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }
    private Bundle createBundleFromMap(Map<String, Object> bundleData) {
        Bundle bundle = new Bundle();
        String name = (String) bundleData.get("name");
        String description = (String) bundleData.get("description");
        //double price = (double) bundleData.get("price");
        double discount = (double) bundleData.get("discount");
        String cancellationDeadline = (String) bundleData.get("cancellationDeadline");
        String bookingDeadline = (String) bundleData.get("bookingDeadline");
        String image = (String) bundleData.get("image");
        boolean available = (boolean) bundleData.get("available");
        boolean visible = (boolean) bundleData.get("visible");
        String companyId = (String) bundleData.get("companyId");

        Map<String, Object> categoryMap = (Map<String, Object>) bundleData.get("category");
        ProductAndServiceCategory category = createProductAndServiceCategoryFromMap(categoryMap); // Assuming a method createCategoryFromMap exists


        List<Map<String, Object>> eventTypeMaps = (List<Map<String, Object>>) bundleData.get("eventTypes");
        ArrayList<EventType> eventTypes = new ArrayList<>();
        for (Map<String, Object> eventTypeMap : eventTypeMaps) {
            EventType eventType = createEventTypeFromMap(eventTypeMap); // Assuming a method createEventTypeFromMap exists
            eventTypes.add(eventType);
        }

        List<String> productIds = (List<String>) bundleData.get("productIds");
        ArrayList<String> pIds = new ArrayList<>();
        if (productIds != null) {
            pIds.addAll(productIds);
        }

        List<String> serviceIds = (List<String>) bundleData.get("serviceIds");
        ArrayList<String> sIds = new ArrayList<>();
        if (serviceIds != null) {
            sIds.addAll(serviceIds);
        }

        // Populate the service object with extracted data
        bundle.setName(name);
        bundle.setDescription(description);

        bundle.setDiscount(discount);
        bundle.setCancellationDeadline(cancellationDeadline);
        bundle.setBookingDeadline(bookingDeadline);
        bundle.setImageUrl(image);
        bundle.setAvailable(available);
        bundle.setVisible(visible);
        bundle.setCategory(category);
        bundle.setEventTypes(eventTypes);
        bundle.setCompanyId(companyId);
        bundle.setProducts(null);
        bundle.setServices(null);
        bundle.setProductIds(pIds);
        bundle.setServiceIds(sIds);
        bundle.setPrice(0);

        return bundle;
    }

    public static ProductAndServiceCategory createProductAndServiceCategoryFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        return new ProductAndServiceCategory(name, description);
    }
    public static EventType createEventTypeFromMap(Map<String, Object> map) {
        String name = (String) map.get("name");
        String description = (String) map.get("description");
        boolean blocked = (boolean) map.get("blocked");
        List<String> subcategories = (List<String>) map.get("subcategories");
        return new EventType(name, description, blocked, subcategories);
    }

    public void getBundlesByCompanyId(OnBundlesLoadedListener listener, String companyId){
        List<Bundle> bundles = new ArrayList<>();
        firestore.collection("bundles").whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            Map<String, Object> bundleData = document.getData();
                            Bundle bundle = createBundleFromMap(bundleData);

                            bundle.setId(document.getId());
                            bundles.add(bundle);

                        }
                        listener.onBundlesLoaded(bundles);
                    }
                    else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void updateBundleData(Bundle bundle) {
        firestore.collection("bundles").document(bundle.getId())
                .set(bundle)
                .addOnSuccessListener(aVoid -> Log.d("Bundleservice", "bundle updated successfully"))
                .addOnFailureListener(e -> Log.e("Bundleservice", "Error updating Bundle", e));
    }

}
