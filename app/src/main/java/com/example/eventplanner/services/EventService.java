package com.example.eventplanner.services;

import android.content.Context;
import android.util.Log;

import com.example.eventplanner.model.Budget;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventPrivacy;
import com.example.eventplanner.model.EventType;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventService {
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public EventService(Context context) {
        this.context = context;
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }

    public interface OnEventsLoadedListener {
        void onEventsLoaded(List<Event> events);

        void onFailure(Exception e);
    }

    public void getAllEvents(OnEventsLoadedListener listener) {
        List<Event> events = new ArrayList<>();
        firestore.collection("events")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Map<String, Object> eventData = document.getData();
                            Event event = createEventFromMap(eventData);
                            events.add(event);
                        }
                        listener.onEventsLoaded(events);
                    } else {
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void addEvent(Event event) {
        Map<String, Object> eventData = createEventMap(event);
        firestore.collection("events").add(eventData)
                .addOnSuccessListener(documentReference -> {
                    Log.d("EventService", "Event added with ID: " + documentReference.getId());
                    event.setId(documentReference.getId()); // Set the event ID in the object
                    // Update the event document with the ID
                    firestore.collection("events").document(documentReference.getId())
                            .update("id", documentReference.getId())
                            .addOnSuccessListener(aVoid -> Log.d("EventService", "Event ID updated"))
                            .addOnFailureListener(e -> Log.e("EventService", "Error updating event ID", e));
                })
                .addOnFailureListener(e -> Log.e("EventService", "Error adding event", e));
    }

    private Event createEventFromMap(Map<String, Object> eventData) {
        String eventId = (String) eventData.get("id");
        String eventName = (String) eventData.get("eventName");
        String eventDescription = (String) eventData.get("eventDescription");
        int maxParticipants = ((Long) eventData.get("maxParticipants")).intValue();
        EventPrivacy privacyType = EventPrivacy.valueOf((String) eventData.get("privacyType"));
        String location = (String) eventData.get("location");
        int maxDistanceInKm = ((Long) eventData.get("maxDistanceInKm")).intValue();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
        LocalDateTime eventDate = LocalDateTime.parse((String) eventData.get("eventDate"), formatter);
        EventType eventType = createEventTypeFromMap((Map<String, Object>) eventData.get("eventType"));
        List<String> productAndServiceCategories = (List<String>) eventData.get("productAndServiceCategories");
        List<String> productAndServiceSubcategories = (List<String>) eventData.get("productAndServiceSubcategories");
        //Budget budget = createBudgetFromMap((Map<String, Object>) eventData.get("budget"));

        Event event = new Event(eventName, eventDescription, maxParticipants, privacyType, location, maxDistanceInKm, eventDate, eventType, productAndServiceCategories, productAndServiceSubcategories, new Budget());
        event.setId(eventId); // Set the event ID
        return event;
    }

    private EventType createEventTypeFromMap(Map<String, Object> eventTypeData) {
        String typeName = (String) eventTypeData.get("typeName");
        String description = (String) eventTypeData.get("description");
        boolean blocked = (boolean) eventTypeData.get("blocked");
        List<String> subcategories = (List<String>) eventTypeData.get("subcategories");

        return new EventType(typeName, description, blocked, subcategories);
    }

    private Budget createBudgetFromMap(Map<String, Object> budgetData) {
        String eventId = (String) budgetData.get("eventId");
        double totalBudget = ((Number) budgetData.get("totalBudget")).doubleValue();
        List<Budget.BudgetItem> budgetItems = createBudgetItemsFromMap((List<Map<String, Object>>) budgetData.get("budgetItems"));

        Budget budget = new Budget();
        budget.setEventId(eventId);
        budget.setTotalBudget(totalBudget);
        budget.setBudgetItems(budgetItems);

        return budget;
    }

    public Map<String, Object> createEventMap(Event event) {
        Map<String, Object> eventData = new HashMap<>();
        eventData.put("id", event.getId()); // Ensure the ID is included in the map
        eventData.put("eventName", event.getEventName());
        eventData.put("eventDescription", event.getEventDescription());
        eventData.put("maxParticipants", event.getMaxParticipants());
        eventData.put("privacyType", event.getPrivacyType().name());
        eventData.put("location", event.getLocation());
        eventData.put("maxDistanceInKm", event.getMaxDistanceInKm());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
        eventData.put("eventDate", event.getEventDate().format(formatter));

        eventData.put("eventType", createEventTypeMap(event.getEventType()));
        eventData.put("productAndServiceCategories", event.getProductAndServiceCategories());
        eventData.put("productAndServiceSubcategories", event.getProductAndServiceSubcategories());
        //eventData.put("budget", createBudgetMap(event.getBudget()));

        return eventData;
    }

    private Map<String, Object> createEventTypeMap(EventType eventType) {
        Map<String, Object> eventTypeData = new HashMap<>();
        eventTypeData.put("typeName", eventType.getName());
        eventTypeData.put("description", eventType.getDescription());
        eventTypeData.put("blocked", eventType.isBlocked());
        eventTypeData.put("subcategories", eventType.getSubcategories());

        return eventTypeData;
    }

    private Map<String, Object> createBudgetMap(Budget budget) {
        Map<String, Object> budgetData = new HashMap<>();
        budgetData.put("eventId", budget.getEventId());
        budgetData.put("totalBudget", budget.getTotalBudget());

        List<Map<String, Object>> budgetItemsData = new ArrayList<>();
        for (Budget.BudgetItem budgetItem : budget.getBudgetItems()) {
            Map<String, Object> itemData = new HashMap<>();
            itemData.put("category", budgetItem.getCategory());
            itemData.put("subcategory", budgetItem.getSubcategory());
            itemData.put("plannedAmount", budgetItem.getPlannedAmount());
            itemData.put("isPurchased", budgetItem.isPurchased());
            budgetItemsData.add(itemData);
        }
        budgetData.put("budgetItems", budgetItemsData);

        return budgetData;
    }


    private List<Budget.BudgetItem> createBudgetItemsFromMap(List<Map<String, Object>> budgetItemsData) {
        List<Budget.BudgetItem> budgetItems = new ArrayList<>();
        for (Map<String, Object> itemData : budgetItemsData) {
            String category = (String) itemData.get("category");
            String subcategory = (String) itemData.get("subcategory");
            double plannedAmount = ((Number) itemData.get("plannedAmount")).doubleValue();
            boolean isPurchased = (boolean) itemData.get("isPurchased");

            Budget.BudgetItem budgetItem = new Budget.BudgetItem(category, subcategory, plannedAmount);
            budgetItem.setPurchased(isPurchased);

            budgetItems.add(budgetItem);
        }
        return budgetItems;
    }

    public void createMockedEventsInDatabase() {
        List<Event> mockedEvents = generateMockedEvents();

        for (Event event : mockedEvents) {
            addEvent(event);
        }
    }


    private List<Event> generateMockedEvents(){
        List<Event> events = new ArrayList<>();

        // Mock Event 1
        Event event1 = new Event(
                "Birthday Party",
                "A fun birthday celebration",
                50,
                EventPrivacy.OPEN,
                "123 Main St",
                10,
                LocalDateTime.of(2024, 6, 20, 18, 0),
                new EventType("Birthday", "Celebrating a birthday", false, null),
                Arrays.asList("Food & Drink", "Entertainment"),
                Arrays.asList("Cake", "DJ"),
                new Budget()
        );
        events.add(event1);

        // Mock Event 2
        Event event2 = new Event(
                "Wedding",
                "A beautiful wedding ceremony",
                100,
                EventPrivacy.CLOSED,
                "456 Elm St",
                20,
                LocalDateTime.of(2024, 7, 15, 14, 0),
                new EventType("Wedding", "A celebration of love", false, null),
                Arrays.asList("Venue", "Catering", "Flowers"),
                Arrays.asList("Church", "Buffet", "Bouquet"),
                new Budget()
        );
        events.add(event2);

        // Mock Event 3
        Event event3 = new Event(
                "Corporate Conference",
                "Annual business conference",
                200,
                EventPrivacy.CLOSED,
                "789 Oak St",
                30,
                LocalDateTime.of(2024, 8, 10, 9, 0),
                new EventType("Conference", "Business gathering", false, null),
                Arrays.asList("Venue", "Catering", "Audio-Visual Equipment"),
                Arrays.asList("Convention Center", "Buffet", "Projector"),
                new Budget()
        );
        events.add(event3);

        // Mock Event 4
        Event event4 = new Event(
                "Music Festival",
                "Outdoor music festival",
                500,
                EventPrivacy.OPEN,
                "101 Pine St",
                50,
                LocalDateTime.of(2024, 9, 5, 12, 0),
                new EventType("Music Festival", "Live music event", false, null),
                Arrays.asList("Stage", "Sound System", "Food & Drink"),
                Arrays.asList("Main Stage", "Speakers", "Food Trucks"),
                new Budget()
        );
        events.add(event4);

        // Mock Event 5
        Event event5 = new Event(
                "Baby Shower",
                "Celebrating the arrival of a baby",
                30,
                EventPrivacy.OPEN,
                "202 Maple Ave",
                15,
                LocalDateTime.of(2024, 10, 8, 15, 0),
                new EventType("Baby Shower", "Welcoming a new baby", false, null),
                Arrays.asList("Decorations", "Games", "Food & Drink"),
                Arrays.asList("Balloon Arch", "Baby Bingo", "Snacks"),
                new Budget()
        );
        events.add(event5);

        // Mock Event 6
        Event event6 = new Event(
                "Charity Gala",
                "Fundraising event for a cause",
                150,
                EventPrivacy.CLOSED,
                "303 Cedar St",
                25,
                LocalDateTime.of(2024, 11, 20, 17, 0),
                new EventType("Charity Gala", "Supporting a charity", false, null),
                Arrays.asList("Venue", "Catering", "Silent Auction"),
                Arrays.asList("Ballroom", "Dinner Buffet", "Artworks"),
                new Budget()
        );
        events.add(event6);

        // Mock Event 7
        Event event7 = new Event(
                "Art Exhibition",
                "Showcasing local artists",
                50,
                EventPrivacy.OPEN,
                "404 Elm St",
                10,
                LocalDateTime.of(2024, 12, 12, 12, 0),
                new EventType("Art Exhibition", "Displaying artworks", false, null),
                Arrays.asList("Gallery Space", "Refreshments", "Security"),
                Arrays.asList("Main Gallery", "Wine & Cheese", "Guards"),
                new Budget()
        );
        events.add(event7);

        // Mock Event 8
        Event event8 = new Event(
                "Holiday Party",
                "Year-end celebration",
                80,
                EventPrivacy.CLOSED,
                "505 Oak St",
                15,
                LocalDateTime.of(2024, 12, 24, 20, 0),
                new EventType("Holiday Party", "Festive gathering", false, null),
                Arrays.asList("Decorations", "Catering", "Entertainment"),
                Arrays.asList("Christmas Tree", "Buffet", "Live Band"),
                new Budget()
        );
        events.add(event8);

        // Mock Event 9
        Event event9 = new Event(
                "Book Launch",
                "Launching a new book",
                40,
                EventPrivacy.OPEN,
                "606 Maple Ave",
                12,
                LocalDateTime.of(2025, 1, 15, 14, 0),
                new EventType("Book Launch", "Introducing a new book", false, null),
                Arrays.asList("Venue", "Book Signing", "Refreshments"),
                Arrays.asList("Library", "Author Table", "Snacks"),
                new Budget()
        );
        events.add(event9);

        // Mock Event 10
        Event event10 = new Event(
                "Sports Tournament",
                "Competitive sports event",
                200,
                EventPrivacy.OPEN,
                "707 Cedar St",
                20,
                LocalDateTime.of(2025, 2, 28, 10, 0),
                new EventType("Sports Tournament", "Athletic competition", false, null),
                Arrays.asList("Venue", "Equipment Rental", "Prizes"),
                Arrays.asList("Stadium", "Balls", "Medals"),
                new Budget()
        );
        events.add(event10);

        return events;
    }
}
