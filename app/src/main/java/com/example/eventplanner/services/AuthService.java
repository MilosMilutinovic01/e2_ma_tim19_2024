package com.example.eventplanner.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.async.ImageUploadCallback;
import com.example.eventplanner.model.WorkingDay;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.time.format.DateTimeFormatter;

public class AuthService {
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Context context;

    public AuthService(Context context) {
        this.context = context;
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://eventplanner-91de1.appspot.com");
    }

    public boolean registerUser(String email, String password, String city, String country, String name, String phone, String street, String surname, Role role) {
        try {
            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        String uid = user.getUid();
                        Map<String, Object> userData = new HashMap<>();
                        userData.put("city", city);
                        userData.put("country", country);
                        userData.put("email", email);
                        userData.put("name", name);
                        userData.put("password", password);
                        userData.put("phone", phone);
                        userData.put("street", street);
                        userData.put("surname", surname);
                        userData.put("uid", uid);
                        userData.put("role", role);

                        firestore.collection("usersCollection")
                                .document(uid)
                                .set(userData)
                                .addOnSuccessListener(aVoid -> {
                                    user.sendEmailVerification().addOnCompleteListener(emailVerificationTask -> {
                                        if (emailVerificationTask.isSuccessful()) {
                                            Toast.makeText(context, "Successfully registered! Verification email sent.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Error sending verification email: " + emailVerificationTask.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                });
                    }
                } else {
                    Toast.makeText(context, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            return true; // Registration initiated successfully
        } catch (Exception e) {
            e.printStackTrace();
            return false; // Registration failed
        }
    }

    public boolean registerOwner(User owner, Company company, List<EventType> eventTypes, List<ProductAndServiceCategory> productAndServiceCategories) {
        try {
            firebaseAuth.createUserWithEmailAndPassword(owner.getEmail(), owner.getPassword()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        String uid = user.getUid();
                        owner.setId(uid);
                        Map<String, Object> userData = new HashMap<>();
                        userData.put("city", owner.getCity());
                        userData.put("country", owner.getCountry());
                        userData.put("email", owner.getEmail());
                        userData.put("name", owner.getName());
                        userData.put("password", owner.getPassword());
                        userData.put("phone", owner.getPhone());
                        userData.put("street", owner.getStreet());
                        userData.put("surname", owner.getSurname());
                        userData.put("uid", uid);
                        userData.put("role", Role.OWNER);

                        Map<String, Object> requestData = new HashMap<>();
                        requestData.put("approved", null);
                        requestData.put("companyName", company.getName());
                        requestData.put("companyName", company.getName());
                        requestData.put("ownerFirstName", owner.getName());
                        requestData.put("ownerLastName", owner.getSurname());
                        requestData.put("companyEmail", company.getEmail());
                        requestData.put("ownerEmail", owner.getEmail());
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");
                        requestData.put("requestTime", LocalDateTime.now().format(formatter));

                        List<String> categoryNames = new ArrayList<>();
                        for (ProductAndServiceCategory category : productAndServiceCategories) {
                            categoryNames.add(category.getName());
                        }
                        requestData.put("categories", categoryNames);

                        List<String> eventTypeNames = new ArrayList<>();
                        for (EventType eventType : eventTypes) {
                            eventTypeNames.add(eventType.getName());
                        }
                        requestData.put("eventTypes", eventTypeNames);

                        firestore.collection("usersCollection")
                                .document(uid)
                                .set(userData)
                                .addOnSuccessListener(aVoid -> {
                                    saveCompany(owner, company, eventTypes, productAndServiceCategories);
                                    user.sendEmailVerification().addOnCompleteListener(emailVerificationTask -> {
                                        if (emailVerificationTask.isSuccessful()) {
                                            Toast.makeText(context, "Successfully registered! Verification email sent.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Error sending verification email: " + emailVerificationTask.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                });
                        firestore.collection("registrationRequests")
                                .add(requestData)
                                .addOnSuccessListener(documentReference -> {
                                    String requestId = documentReference.getId();
                                    updateRequestDataWithId(requestId);
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(context, "Error saving request: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                });
                    }
                } else {
                    Toast.makeText(context, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean registerEmployer(User employer, Bitmap image) {
        try {
            firebaseAuth.createUserWithEmailAndPassword(employer.getEmail(), employer.getPassword()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        String uid = user.getUid();
                        Map<String, Object> userData = new HashMap<>();
                        userData.put("city", employer.getCity());
                        userData.put("country", employer.getCountry());
                        userData.put("email", employer.getEmail());
                        userData.put("name", employer.getName());
                        userData.put("password", employer.getPassword());
                        userData.put("phone", employer.getPhone());
                        userData.put("street", employer.getStreet());
                        userData.put("surname", employer.getSurname());
                        userData.put("uid", uid);
                        userData.put("companyId", employer.getCompanyId());
                        userData.put("role", Role.EMPLOYER);

                        List<Map<String, Object>> workingDaysData = new ArrayList<>();
                        for (WorkingDay workingDay : employer.getWorkingDays()) {
                            workingDaysData.add(workingDay.serialize());
                        }
                        userData.put("workingDays", workingDaysData);

                        // Call saveImage with a callback
                        saveImage(employer.getName(), image, new ImageUploadCallback() {
                            @Override
                            public void onImageUploadSuccess(String imageURL) {
                                userData.put("profileImage", imageURL);
                                // Save user data to Firestore
                                firestore.collection("usersCollection").document(uid).set(userData)
                                        .addOnSuccessListener(aVoid -> {
                                            user.sendEmailVerification().addOnCompleteListener(emailVerificationTask -> {
                                                if (emailVerificationTask.isSuccessful()) {
                                                    Toast.makeText(context, "Successfully registered! Verification email sent.", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(context, "Error sending verification email: " + emailVerificationTask.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        })
                                        .addOnFailureListener(e -> {
                                            Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                        });
                            }

                            @Override
                            public void onImageUploadFailure(String errorMessage) {
                                Toast.makeText(context, "Error uploading image: " + errorMessage, Toast.LENGTH_SHORT).show();
                            }

                        });
                    } else {
                        Toast.makeText(context, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return true; // Registration initiated successfully
        } catch (Exception e) {
            e.printStackTrace();
            return false; // Registration failed
        }
    }


    private void saveCompany(User owner, Company company, List<EventType> eventTypes, List<ProductAndServiceCategory> productAndServiceCategories) {
        Map<String, Object> companyData = new HashMap<>();
        companyData.put("email", company.getEmail());
        companyData.put("name", company.getName());
        companyData.put("description", company.getDescription());
        companyData.put("phone", company.getPhone());
        companyData.put("street", company.getStreet());
        companyData.put("city", company.getCity());
        companyData.put("country", company.getCountry());
        companyData.put("profileImage", company.getProfileImage());

        List<Map<String, Object>> workingDaysData = new ArrayList<>();
        for (WorkingDay workingDay : company.getWorkingDays()) {
            workingDaysData.add(workingDay.serialize());
        }
        companyData.put("workingDays", workingDaysData);

        firestore.collection("companiesCollection")
                .add(companyData)
                .addOnSuccessListener(documentReference -> {
                    // After company is saved successfully, update user data with company ID
                    String companyId = documentReference.getId();
                    updateUserDataWithCompany(owner, companyId, eventTypes, productAndServiceCategories);
                    updateCompanyDataWithCompany(companyId, productAndServiceCategories);
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(context, "Error saving company: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    // Method to save event types data
    private void saveEventTypes(List<EventType> eventTypes) {
        for (EventType eventType : eventTypes) {
            firestore.collection("eventTypesCollection")
                    .add(eventType)
                    .addOnSuccessListener(aVoid -> Log.d("TAG", "EventType saved successfully"))
                    .addOnFailureListener(e -> Log.e("TAG", "Error saving eventType", e));
        }
    }

    private void saveProductAndServiceCategories(List<ProductAndServiceCategory> productAndServiceCategories){
        for (ProductAndServiceCategory productAndServiceCategory : productAndServiceCategories) {
            firestore.collection("productAndServiceCategoriesCollection")
                    .add(productAndServiceCategory)
                    .addOnSuccessListener(aVoid -> Log.d("TAG", "EventType saved successfully"))
                    .addOnFailureListener(e -> Log.e("TAG", "Error saving eventType", e));
        }
    }

    private void updateCompanyDataWithCompany(String companyId, List<ProductAndServiceCategory> productAndServiceCategories) {
        Map<String, Object> companyData = new HashMap<>();
        companyData.put("companyId", companyId);

        firestore.collection("companiesCollection")
                .document(companyId)
                .update(companyData)
                .addOnSuccessListener(aVoid -> {
                    Toast.makeText(context, "Succesfull: " , Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(context, "Error updating user data with company ID: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    private void updateRequestDataWithId(String requestId) {
        Map<String, Object> requestData = new HashMap<>();
        requestData.put("requestId", requestId);

        firestore.collection("registrationRequests")
                .document(requestId)
                .update(requestData)
                .addOnSuccessListener(aVoid -> {
                })
                .addOnFailureListener(e -> {
                });
    }

    private void updateUserDataWithCompany(User owner, String companyId, List<EventType> eventTypes, List<ProductAndServiceCategory> productAndServiceCategories) {
        Map<String, Object> userData = new HashMap<>();
        userData.put("companyId", companyId);

        firestore.collection("usersCollection")
                .document(owner.getId())
                .update(userData)
                .addOnSuccessListener(aVoid -> {
                    //saveEventTypes(eventTypes);
                    saveProductAndServiceCategories(productAndServiceCategories);
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(context, "Error updating user data with company ID: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    private String saveImage(String employerName, Bitmap image, ImageUploadCallback callback){
        AtomicReference<String> imageURL = new AtomicReference<>(null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        // Create a reference to the image file in Firebase Storage
        StorageReference imageRef = storageRef.child("images/"+ employerName +".jpg");

        // Upload the byte array to Firebase Storage
        UploadTask uploadTask = imageRef.putBytes(imageBytes);

        // Listen for upload success/failure
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            // Upload successful
            Log.d("TAG", "Image uploaded successfully!");
            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
            result.addOnSuccessListener(uri -> {
                callback.onImageUploadSuccess(uri.toString());
            });
        }).addOnFailureListener(exception -> {
            // Upload failed
            Log.e("TAG", "Image upload failed: " + exception.getMessage());
            callback.onImageUploadFailure(exception.getMessage());
        });

        return imageURL.get();
    }

    public Task<User> loginUser(String email, String password) {
        Log.d("AuthService", "Attempting to sign in with email: " + email);

        return firebaseAuth.signInWithEmailAndPassword(email, password)
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                        if (currentUser != null && currentUser.isEmailVerified()) {
                            // Fetch user role from Firestore
                            Query query = firestore.collection("usersCollection").whereEqualTo("uid", currentUser.getUid());
                            return query.get();
                        } else {
                            throw new Exception("User not signed in or email not verified");
                        }
                    } else {
                        throw task.getException();
                    }
                })
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            //String role = documentSnapshot.getString("role");
                            Map<String, Object> userData = documentSnapshot.getData();
                            User user = createUserFromMap(userData);
                            Log.d("AuthService", "User found: " + user);
                            return user;
                        } else {
                            Log.d("AuthService", "User not found in Firestore");
                            throw new Exception("User not found");
                        }
                    } else {
                        Log.e("AuthService", "Query task failed", task.getException());
                        throw task.getException();
                    }
                });
    }
    private User createUserFromMap(Map<String, Object> userData) {
        String email = (String) userData.get("email");
        String password = (String) userData.get("password");
        String name = (String) userData.get("name");
        String surname = (String) userData.get("surname");
        String phone = (String) userData.get("phone");
        String street = (String) userData.get("street");
        String city = (String) userData.get("city");
        String country = (String) userData.get("country");
        String image = (String) userData.get("profileImage");
        String role = (String) userData.get("role");
        String companyId = (String) userData.get("companyId");

        // Deserialize working days
        List<WorkingDay> workingDays = null;
        if (userData.containsKey("workingDays")) {
            List<Map<String, Object>> workingDaysData = (List<Map<String, Object>>) userData.get("workingDays");
            if (workingDaysData != null) {
                workingDays = new ArrayList<>();
                for (Map<String, Object> day : workingDaysData) {
                    WorkingDay workingDay = new WorkingDay();
                    workingDay.deserialize(day);
                    workingDays.add(workingDay);
                }
            }
        }
        User user = new User(email, password, name, surname, phone, street, city, country, image, workingDays);
        user.setRole(Role.valueOf(role));
        user.setCompanyId(companyId);
        return user;
    }
    public Task<User> getCurrentUser() {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();
            return firestore.collection("usersCollection")
                    .document(uid)
                    .get()
                    .continueWith(task -> {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot != null && documentSnapshot.exists()) {
                                Map<String, Object> userData = documentSnapshot.getData();
                                User user = createUserFromMap(userData);
                                user.setId(documentSnapshot.getId());
                                return user;
                            } else {
                                throw new Exception("User not found");
                            }
                        } else {
                            throw task.getException();
                        }
                    });
        } else {
            return null;
        }
    }

}
