package com.example.eventplanner.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toolbar;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.ActivityMainBinding;
import com.example.eventplanner.services.NotificationService;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //EdgeToEdge.enable(this);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // ne znam sta je ovo
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.drawerLayout), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        setupNavigationBar(binding.bottomNavView);
        setNavBarVisibility();

        binding.btnNotif.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.notificationsFragment);
            }
        });
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection.
        switch (item.getItemId()) {
            case R.id.notifMenu:
            default:
                navController.navigate(R.id.notificationsFragment);
                return true;
        }
    }*/

    /*  private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    NotificationService.CHANNEL_ID,
                    "Counter",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("Sve notifikacije");

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }*/


    private void setupNavigationBar(BottomNavigationView bottomNav){
        navController = Navigation.findNavController(this, R.id.fragment);
        NavigationUI.setupWithNavController(bottomNav, navController);

        /*Set<Integer> topLevelDestinations = new HashSet<>();
        topLevelDestinations.add(R.id.ownerHomeViewFragment);
        appBarConfiguration = new AppBarConfiguration.Builder(topLevelDestinations)
                .setOpenableLayout(drawerLayout)
                .build();
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph())
                .setOpenableLayout(drawerLayout)
                .build();

        //setSupportActionBar(binding.mToolbar);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        */
    }

    /*@Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }*/


    private void setNavBarVisibility(){
        navController.addOnDestinationChangedListener((navController1, navDestination, bundle) -> {
            if(navDestination.getId() == R.id.splashScreenFragment){
                binding.bottomNavViewOd.setVisibility(View.GONE);
                binding.bottomNavViewPupz.setVisibility(View.GONE);
                binding.bottomNavViewAdmin.setVisibility(View.GONE);
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
                binding.btnNotif.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.employerRegistrationFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.employerDetailsFragment){
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.ownerHomeViewFragment){
                setupNavigationBar(binding.bottomNavView);
                binding.bottomNavView.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
                binding.btnNotif.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.scheduleFragment){
                setupNavigationBar(binding.bottomNavViewPupz);
                binding.abLayout.setVisibility(View.GONE);
                binding.bottomNavViewPupz.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.serviceAndProductCategoryFragment){
                setupNavigationBar(binding.bottomNavViewAdmin);
                binding.bottomNavViewAdmin.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
                binding.btnNotif.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.servicesFragment){
                binding.bottomNavView.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
            }else if(navDestination.getId() == R.id.productsFragment){
                binding.bottomNavView.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
            }else if(navDestination.getId() == R.id.bundlesFragment){
                binding.bottomNavView.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
                binding.btnNotif.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.productCreationFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.serviceCreationFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.productDetailsFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.bundleCreationFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.serviceDetailsFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.serviceEditFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.eventOrganizerHomeViewFragment){
                setupNavigationBar(binding.bottomNavViewOd);
                binding.bottomNavViewOd.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
                binding.btnNotif.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.editPriceFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.priceListFragment){
                binding.bottomNavView.setVisibility(View.VISIBLE);
                binding.abLayout.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.companyProfileFragment){
                binding.bottomNavView.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.userProfileFragment){
                binding.bottomNavViewAdmin.setVisibility(View.GONE);
                binding.abLayout.setVisibility(View.GONE);
                binding.bottomNavView.setVisibility(View.GONE);
            }
            else if(navDestination.getId() == R.id.userReportsFragment){
                binding.abLayout.setVisibility(View.GONE);
                binding.bottomNavViewAdmin.setVisibility(View.VISIBLE);
            }
            else if(navDestination.getId() == R.id.notificationsFragment){
                binding.abLayout.setVisibility(View.VISIBLE);
            }
            // TODO : Setujte visibility na true i na ostalim prvim prozorima posle registracije ili logina
            // npr. home view za admina, organizatora itd
            // ja sam napravila za vlasnika
        });
    }
}