package com.example.eventplanner.model;

public enum UserReportStatus {
    REPORTED,
    ACCEPTED,
    REJECTED
}
