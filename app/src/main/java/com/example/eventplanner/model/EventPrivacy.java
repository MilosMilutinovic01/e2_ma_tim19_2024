package com.example.eventplanner.model;

public enum EventPrivacy {
    OPEN,
    CLOSED
}
