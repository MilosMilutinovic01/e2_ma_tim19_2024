package com.example.eventplanner.model;

import java.time.LocalDateTime;

public class ServiceReservation {
    private String id;
    private String eventId;
    private String serviceName;
    private String serviceDescription;
    private String employeeId; // Selected employee for the service
    private LocalDateTime eventDate; // Date of the event
    private LocalDateTime serviceStartTime; // Start time of the service
    private LocalDateTime serviceEndTime; // End time of the service
    private boolean notificationSent;
    private boolean rejected;
    private String eventOrganizerId;


    public ServiceReservation(String id,String eventId, String serviceName, String serviceDescription, String employeeId,
                              LocalDateTime eventDate, LocalDateTime serviceStartTime, LocalDateTime serviceEndTime,
                              boolean notificationSent, boolean rejected, String eventOrganizerId) {
        this.id = eventId;
        this.eventId = eventId;
        this.serviceName = serviceName;
        this.serviceDescription = serviceDescription;
        this.employeeId = employeeId;
        this.eventDate = eventDate;
        this.serviceStartTime = serviceStartTime;
        this.serviceEndTime = serviceEndTime;
        this.notificationSent = notificationSent;
        this.rejected = rejected;
        this.eventOrganizerId = eventOrganizerId;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public LocalDateTime getServiceStartTime() {
        return serviceStartTime;
    }

    public void setServiceStartTime(LocalDateTime serviceStartTime) {
        this.serviceStartTime = serviceStartTime;
    }

    public LocalDateTime getServiceEndTime() {
        return serviceEndTime;
    }

    public void setServiceEndTime(LocalDateTime serviceEndTime) {
        this.serviceEndTime = serviceEndTime;
    }

    public boolean isNotificationSent() {
        return notificationSent;
    }

    public void setNotificationSent(boolean notificationSent) {
        this.notificationSent = notificationSent;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void setRejected(boolean rejected) {
        this.rejected = rejected;
    }

    public String getEventOrganizerId() {
        return eventOrganizerId;
    }

    public void setEventOrganizerId(String eventOrganizerId) {
        this.eventOrganizerId = eventOrganizerId;
    }
}
