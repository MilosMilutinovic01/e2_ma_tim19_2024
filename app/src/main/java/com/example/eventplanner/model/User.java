package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class User implements Parcelable {
    private String id;
    private String email;
    private String password;
    private String name;
    private String surname;
    private String phone;
    private String street;
    private String city;
    private String country;
    private String profileImage;
    private String companyId;
    private List<WorkingDay> workingDays;
    private Role role;
    private List<EmployerEvent> events;

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public User(){

    }

    public User(String email, String password, String name, String surname, String phone, String street, String city, String country, String image, List<WorkingDay> workingDays) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.street = street;
        this.city = city;
        this.country = country;
        this.profileImage = image;
        this.workingDays = workingDays;
    }

    public User(String email, String password, String name, String surname, String phone) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }

    public List<EmployerEvent> getEvents() {
        return events;
    }

    public void setEvents(List<EmployerEvent> events) {
        this.events = events;
    }

    // Method to calculate unavailable hours based on events
    public List<UnavailableHours> calculateUnavailableHours() {
        List<UnavailableHours> unavailableHoursList = new ArrayList<>();
        for (EmployerEvent event : events) {
            LocalDateTime dateFrom = event.getDate().atTime(event.getStartTime());
            LocalDateTime dateTo = event.getDate().atTime(event.getEndTime());
            unavailableHoursList.add(new UnavailableHours(dateFrom, dateTo));
        }
        return unavailableHoursList;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<WorkingDay> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<WorkingDay> workingDays) {
        this.workingDays = workingDays;
    }

    protected User(Parcel in) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(phone);
        dest.writeString(companyId);
        dest.writeValue(profileImage);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        return  name + " " + surname ;
    }
}
