package com.example.eventplanner.model;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class EventType implements Parcelable {
    private String id;
    private String name;
    private String description;
    private boolean blocked;
    private List<String> subcategories;

    public EventType() {

    }

    public EventType(String name, String description, boolean blocked, List<String> subcategories) {
        this.name = name;
        this.description = description;
        this.blocked = blocked;
        this.subcategories = subcategories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public List<String> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<String> subcategories) {
        this.subcategories = subcategories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected EventType(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        id = in.readString();
        name = in.readString();
        description = in.readString();
        blocked = Boolean.parseBoolean(in.readString());
        subcategories = new ArrayList<>();
        in.readStringList(subcategories);
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(String.valueOf(blocked));
        dest.writeStringList(subcategories);
    }

    public static final Creator<ProductAndServiceCategory> CREATOR = new Creator<ProductAndServiceCategory>() {
        @Override
        public ProductAndServiceCategory createFromParcel(Parcel in) {
            return new ProductAndServiceCategory(in);
        }

        @Override
        public ProductAndServiceCategory[] newArray(int size) {
            return new ProductAndServiceCategory[size];
        }
    };
    public String toString() {
        return name;
    }
}
