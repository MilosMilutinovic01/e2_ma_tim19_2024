package com.example.eventplanner.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public class CompanyWorkingTime {
    private Map<DayOfWeek, WorkingDay> standardWorkingHours;

    public CompanyWorkingTime() {
        standardWorkingHours = new HashMap<>();
        standardWorkingHours.put(DayOfWeek.MONDAY, new WorkingDay(DayOfWeek.MONDAY, LocalTime.of(8, 0), LocalTime.of(16, 0)));
        standardWorkingHours.put(DayOfWeek.TUESDAY, new WorkingDay(DayOfWeek.TUESDAY, LocalTime.of(8, 0), LocalTime.of(16, 0)));
        standardWorkingHours.put(DayOfWeek.WEDNESDAY, new WorkingDay(DayOfWeek.WEDNESDAY, LocalTime.of(8, 0), LocalTime.of(16, 0)));
        standardWorkingHours.put(DayOfWeek.THURSDAY, new WorkingDay(DayOfWeek.THURSDAY, LocalTime.of(8, 0), LocalTime.of(16, 0)));
        standardWorkingHours.put(DayOfWeek.FRIDAY, new WorkingDay(DayOfWeek.FRIDAY, LocalTime.of(8, 0), LocalTime.of(16, 0)));
    }

    public WorkingDay getWorkingHours(DayOfWeek dayOfWeek) {
        return standardWorkingHours.get(dayOfWeek);
    }

    public void updateWorkingHours(DayOfWeek dayOfWeek, String startTimeString, String endTimeString) {
        // Parse the input strings to LocalTime objects
        LocalTime startTime = LocalTime.parse(startTimeString);
        LocalTime endTime = LocalTime.parse(endTimeString);

        WorkingDay workingDay = standardWorkingHours.get(dayOfWeek);
        if (workingDay != null) {
            workingDay.setStartTime(startTime);
            workingDay.setEndTime(endTime);
        } else {
            System.out.println("Invalid day of the week.");
        }
    }
}