package com.example.eventplanner.model;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Service implements Parcelable {
        private String id;
        private ProductAndServiceCategory category;
        private Subcategory subcategory;
        private String name;
        private String description;
        private String specificities;
        private double price;
        private double discount;
        private long duration;
        private ArrayList<User> employers;
        private ArrayList<EventType> eventTypes;
        private String cancellationDeadline;
        private String bookingDeadline;
        private String image;
        private boolean available;
        private boolean visible;
        private ReservationConfirmationType reservationConfirmation;
        private String companyId;

    public Service() {
    }

    public Service(String id, ProductAndServiceCategory category, Subcategory subcategory, String name, String description, String specificities, double price, double discount, long duration, ArrayList<User> employers, ArrayList<EventType> eventTypes, String cancellationDeadline, String bookingDeadline, String image, boolean available, boolean visible, ReservationConfirmationType reservationConfirmation, String companyId) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.specificities = specificities;
        this.price = price;
        this.discount = discount;
        this.duration = duration;
        this.employers = employers;
        this.eventTypes = eventTypes;
        this.cancellationDeadline = cancellationDeadline;
        this.bookingDeadline = bookingDeadline;
        this.image = image;
        this.available = available;
        this.visible = visible;
        this.reservationConfirmation = reservationConfirmation;
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductAndServiceCategory getCategory() {
        return category;
    }

    public void setCategory(ProductAndServiceCategory category) {
        this.category = category;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecificities() {
        return specificities;
    }

    public void setSpecificities(String specificities) {
        this.specificities = specificities;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public ArrayList<User> getEmployers() {
        return employers;
    }

    public void setEmployers(ArrayList<User> employers) {
        this.employers = employers;
    }

    public ArrayList<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(ArrayList<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public String getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public String getBookingDeadline() {
        return bookingDeadline;
    }

    public void setBookingDeadline(String bookingDeadline) {
        this.bookingDeadline = bookingDeadline;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public ReservationConfirmationType getReservationConfirmation() {
        return reservationConfirmation;
    }

    public void setReservationConfirmation(ReservationConfirmationType reservationConfirmation) {
        this.reservationConfirmation = reservationConfirmation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {

    }
}
