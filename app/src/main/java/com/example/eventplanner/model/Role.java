package com.example.eventplanner.model;

public enum Role {
    OWNER,
    EMPLOYER,
    EVENT_ORGANIZER,
    ADMINISTRATOR
}
