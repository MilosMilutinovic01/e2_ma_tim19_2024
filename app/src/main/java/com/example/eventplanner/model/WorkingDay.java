package com.example.eventplanner.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public class WorkingDay {
    private DayOfWeek dayOfWeek;
    private LocalTime startTime;
    private LocalTime endTime;

    public WorkingDay(){}
    public WorkingDay(DayOfWeek dayOfWeek, LocalTime startTime, LocalTime endTime) {
        this.dayOfWeek = dayOfWeek;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Map<String, Object> serialize(){
        Map<String, Object> workingDayData = new HashMap<>();
        workingDayData.put("dayOfWeek", dayOfWeek.toString());
        workingDayData.put("startTime", startTime.toString());
        workingDayData.put("endTime", endTime.toString());

        return workingDayData;
    }

    public void deserialize(Map<String, Object> map) {
        this.dayOfWeek = DayOfWeek.valueOf((String) map.get("dayOfWeek"));
        this.startTime = LocalTime.parse((String) map.get("startTime"));
        this.endTime = LocalTime.parse((String) map.get("endTime"));
    }
}
