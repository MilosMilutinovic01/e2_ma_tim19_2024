package com.example.eventplanner.model;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Bundle implements Parcelable {
    private String id;
    private ProductAndServiceCategory category;
    private String name;
    private String description;
    private double price;
    private double discount;
    private ArrayList<Product> products;
    private ArrayList<Service> services;
    private ArrayList<String> productIds;
    private ArrayList<String> serviceIds;
    private ArrayList<EventType> eventTypes;
    private String cancellationDeadline;
    private String bookingDeadline;
    private String imageUrl;
    private boolean available;
    private boolean visible;
    private ReservationConfirmationType reservationConfirmation;
    private String companyId;

    public Bundle() {
    }

    public Bundle(String id, ProductAndServiceCategory category, String name, String description, double price, double discount, ArrayList<Product> products, ArrayList<Service> services, ArrayList<String> productIds, ArrayList<String> serviceIds, ArrayList<EventType> eventTypes, String cancellationDeadline, String bookingDeadline, String imageUrl, boolean available, boolean visible, ReservationConfirmationType reservationConfirmation, String companyId) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.products = products;
        this.services = services;
        this.productIds = productIds;
        this.serviceIds = serviceIds;
        this.eventTypes = eventTypes;
        this.cancellationDeadline = cancellationDeadline;
        this.bookingDeadline = bookingDeadline;
        this.imageUrl = imageUrl;
        this.available = available;
        this.visible = visible;
        this.reservationConfirmation = reservationConfirmation;
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductAndServiceCategory getCategory() {
        return category;
    }

    public void setCategory(ProductAndServiceCategory category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<Service> getServices() {
        return services;
    }

    public void setServices(ArrayList<Service> services) {
        this.services = services;
    }

    public ArrayList<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(ArrayList<String> productIds) {
        this.productIds = productIds;
    }

    public ArrayList<String> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(ArrayList<String> serviceIds) {
        this.serviceIds = serviceIds;
    }

    public ArrayList<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(ArrayList<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public String getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public String getBookingDeadline() {
        return bookingDeadline;
    }

    public void setBookingDeadline(String bookingDeadline) {
        this.bookingDeadline = bookingDeadline;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public ReservationConfirmationType getReservationConfirmation() {
        return reservationConfirmation;
    }

    public void setReservationConfirmation(ReservationConfirmationType reservationConfirmation) {
        this.reservationConfirmation = reservationConfirmation;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {

    }
}
