package com.example.eventplanner.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

public class Company implements Parcelable {
    private String id;
    private String email;
    private String name;
    private String description;
    private String phone;
    private String street;
    private String city;
    private String country;
    private Bitmap profileImage;
    private List<WorkingDay> workingDays;


    public Company() {
    }

    public Company(String email, String name, String description, String phone, String street, String city, String country, List<WorkingDay> workingDays) {
        this.email = email;
        this.name = name;
        this.description = description;
        this.phone = phone;
        this.street = street;
        this.city = city;
        this.country = country;
        this.workingDays = workingDays;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Bitmap getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Bitmap profileImage) {
        this.profileImage = profileImage;
    }

    public List<WorkingDay> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<WorkingDay> workingDays) {
        this.workingDays = workingDays;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Company(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        name = in.readString();
        email = in.readString();
        description = in.readString();
        street = in.readString();
        city = in.readString();
        country = in.readString();
        phone = in.readString();
        profileImage = (Bitmap) in.readValue(null);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(description);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(phone);
        dest.writeValue(profileImage);
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

}
