package com.example.eventplanner.model;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.time.format.DateTimeFormatter;

public class EmployerEvent {
    private String id;
    private String name;
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private String employerId;

    public EmployerEvent(){
    }

    public EmployerEvent(String name, LocalDate date, LocalTime startTime, LocalTime endTime, String employerId) {
        this.name = name;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.employerId = employerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public String getEmployerId() {
        return employerId;
    }

    public void setEmployerId(String employerId) {
        this.employerId = employerId;
    }

    public Map<String, Object> serialize(){
        Map<String, Object> employerEvent = new HashMap<>();
        employerEvent.put("name", name.toString());

        String formattedDate = date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
        employerEvent.put("date", formattedDate);

        employerEvent.put("startTime", startTime.toString());
        employerEvent.put("endTime", endTime.toString());
        employerEvent.put("employerId", employerId.toString());

        return employerEvent;
    }

    public void deserialize(Map<String, Object> map) {
        this.name = (String) map.get("name");
        String dateString = (String) map.get("date");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy."); 
        this.date = LocalDate.parse(dateString, formatter);
        this.startTime = LocalTime.parse((String) map.get("startTime"));
        this.endTime = LocalTime.parse((String) map.get("endTime"));
        this.employerId = (String) map.get("employerId");
    }
}
