package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RatingReport implements Parcelable {

    public enum Status {
        ACCEPTED, REJECTED, REPORTED
    }

    private String id;
    private String ratingId;
    private String reportReason;
    private LocalDate reportDate;
    private Status status;
    private String reporterId;

    public RatingReport(String ratingId, String reportReason, LocalDate reportDate, Status status, String reporterId) {
        this.ratingId = ratingId;
        this.reportReason = reportReason;
        this.reportDate = reportDate;
        this.status = status;
        this.reporterId = reporterId;
    }

    protected RatingReport(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        id = in.readString();
        reportReason = in.readString();
        reporterId = in.readString();
        ratingId = in.readString();
        reportDate = LocalDate.parse(in.readString(), DateTimeFormatter.ofPattern("dd.MM.yyyy."));
        status = Status.valueOf(in.readString());
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRatingId() {
        return ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public LocalDate getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDate reportDate) {
        this.reportDate = reportDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {

    }
    public static final Creator<RatingReport> CREATOR = new Creator<RatingReport>() {
        @Override
        public RatingReport createFromParcel(Parcel in) {
            return new RatingReport(in);
        }

        @Override
        public RatingReport[] newArray(int size) {
            return new RatingReport[size];
        }
    };
}
