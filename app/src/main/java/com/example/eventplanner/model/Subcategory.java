package com.example.eventplanner.model;

public class Subcategory {
    private String id;
    private String name;
    private String description;
    private SubcategoryType subcategoryType;
    private ProductAndServiceCategory category;

    public Subcategory() {
    }

    public Subcategory(String name, String description, SubcategoryType subcategoryType, ProductAndServiceCategory category) {
        this.name = name;
        this.description = description;
        this.subcategoryType = subcategoryType;
        this.category = category;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubcategoryType getSubcategoryType() {
        return subcategoryType;
    }

    public void setSubcategoryType(SubcategoryType subcategoryType) {
        this.subcategoryType = subcategoryType;
    }

    public ProductAndServiceCategory getCategory() {
        return category;
    }

    public void setCategory(ProductAndServiceCategory category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return name;
    }
}
