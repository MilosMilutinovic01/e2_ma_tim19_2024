package com.example.eventplanner.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class ProductAndServiceCategory implements Parcelable {
    private String id;
    private String name;
    private String description;

    public ProductAndServiceCategory() {

    }

    public ProductAndServiceCategory(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected ProductAndServiceCategory(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        //id = in.readString();
        name = in.readString();
        description = in.readString();
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(name);
        //dest.writeString(id);
        dest.writeString(description);
    }

    public static final Creator<ProductAndServiceCategory> CREATOR = new Creator<ProductAndServiceCategory>() {
        @Override
        public ProductAndServiceCategory createFromParcel(Parcel in) {
            return new ProductAndServiceCategory(in);
        }

        @Override
        public ProductAndServiceCategory[] newArray(int size) {
            return new ProductAndServiceCategory[size];
        }
    };
    @Override
    public String toString() {
        return name;
    }
}
