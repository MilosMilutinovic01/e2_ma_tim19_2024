package com.example.eventplanner.model;

import static java.lang.System.out;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.net.URL;


public class Employer implements Parcelable {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String street;
    private String city;
    private String country;
    private String phone;
    private Bitmap image;

    public Employer() {
    }

    public Employer(Long id, String name, String surname, String email, String password, String street, String city, String country, String phone, Bitmap image) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.street = street;
        this.city = city;
        this.country = country;
        this.phone = phone;
        this.image = image;
    }

    protected Employer(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        id = in.readLong();
        name = in.readString();
        surname = in.readString();
        email = in.readString();
        password = in.readString();
        street = in.readString();
        city = in.readString();
        country = in.readString();
        phone = in.readString();
        image = (Bitmap) in.readValue(null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(phone);
        dest.writeValue(image);

    }

    public static final Creator<Employer> CREATOR = new Creator<Employer>() {
        @Override
        public Employer createFromParcel(Parcel in) {
            return new Employer(in);
        }

        @Override
        public Employer[] newArray(int size) {
            return new Employer[size];
        }
    };

    @Override
    public String toString() {
        return name;
    }
}
