package com.example.eventplanner.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class CompanyRating implements Parcelable {
    private String id;
    private int rating;
    private String comment;
    private LocalDate date;
    private String companyId;

    // Constructor
    public CompanyRating(int rating, String comment, LocalDate date, String companyId) {
        this.rating = rating;
        this.comment = comment;
        this.date = date;
        this.companyId = companyId;
    }
    protected CompanyRating(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        id = in.readString();
        rating = in.readInt();
        comment = in.readString();
        date = LocalDate.parse(in.readString(), DateTimeFormatter.ofPattern("dd.MM.yyyy."));
        companyId = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    // Getter for rating
    public int getRating() {
        return rating;
    }

    // Setter for rating
    public void setRating(int rating) {
        this.rating = rating;
    }

    // Getter for comment
    public String getComment() {
        return comment;
    }

    // Setter for comment
    public void setComment(String comment) {
        this.comment = comment;
    }

    // Getter for date
    public LocalDate getDate() {
        return date;
    }

    // Setter for date
    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {

    }
    public static final Creator<CompanyRating> CREATOR = new Creator<CompanyRating>() {
        @Override
        public CompanyRating createFromParcel(Parcel in) {
            return new CompanyRating(in);
        }

        @Override
        public CompanyRating[] newArray(int size) {
            return new CompanyRating[size];
        }
    };
}
