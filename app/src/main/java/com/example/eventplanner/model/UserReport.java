package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.util.List;

public class UserReport{
    private String id;
    private String reporterId;
    private String reportedId;
    private User reporter;
    private User reported;
    private LocalDate date;
    private String reason;
    private UserReportStatus status;
    private String rejectReason;

    public UserReport() {
    }

    public UserReport(String id, String reporterId, String reportedId, User reporter, User reported, LocalDate date, String reason, UserReportStatus status, String rejectReason) {
        this.id = id;
        this.reporterId = reporterId;
        this.reportedId = reportedId;
        this.reporter = reporter;
        this.reported = reported;
        this.date = date;
        this.reason = reason;
        this.status = status;
        this.rejectReason = rejectReason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getReportedId() {
        return reportedId;
    }

    public void setReportedId(String reportedId) {
        this.reportedId = reportedId;
    }

    public User getReporter() {
        return reporter;
    }

    public void setReporter(User reporter) {
        this.reporter = reporter;
    }

    public User getReported() {
        return reported;
    }

    public void setReported(User reported) {
        this.reported = reported;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public UserReportStatus getStatus() {
        return status;
    }

    public void setStatus(UserReportStatus status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }
    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }
}
