package com.example.eventplanner.model;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistrationRequest {
    private String requestId;
    private Boolean approved;
    private String companyName;
    private String ownerFirstName;
    private String ownerLastName;
    private String companyEmail;
    private String ownerEmail;
    private LocalDateTime requestTime;
    private List<String> categories;
    private List<String> eventTypes;
    private String rejectionReason; // Nullable

    public RegistrationRequest(String companyName, String ownerFirstName, String ownerLastName, String companyEmail, String ownerEmail, LocalDateTime requestTime, List<String> categories, List<String> eventTypes) {
        this.approved = null;
        this.companyName = companyName;
        this.ownerFirstName = ownerFirstName;
        this.ownerLastName = ownerLastName;
        this.companyEmail = companyEmail;
        this.ownerEmail = ownerEmail;
        this.requestTime = requestTime;
        this.categories = categories;
        this.eventTypes = eventTypes;
    }

    public RegistrationRequest(String requestId, String companyName, String ownerFirstName, String ownerLastName, String companyEmail, String ownerEmail, LocalDateTime requestTime, List<String> categories, List<String> eventTypes) {
        this.requestId = requestId;
        this.approved = null;
        this.companyName = companyName;
        this.ownerFirstName = ownerFirstName;
        this.ownerLastName = ownerLastName;
        this.companyEmail = companyEmail;
        this.ownerEmail = ownerEmail;
        this.requestTime = requestTime;
        this.categories = categories;
        this.eventTypes = eventTypes;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setServiceCategory(List<String> serviceCategory) {
        this.categories = serviceCategory;
    }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventType(List<String> eventType) {
        this.eventTypes = eventType;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }
}

