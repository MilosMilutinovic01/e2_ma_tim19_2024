package com.example.eventplanner.model;

public enum SubcategoryType {
    SERVICE,
    PRODUCT
}
