package com.example.eventplanner.model;

import java.time.LocalDateTime;
import java.util.List;

public class Event {
    private String id;
    private String eventName;
    private String eventDescription;
    private int maxParticipants;
    private EventPrivacy privacyType;
    private String location;
    private int maxDistanceInKm;
    private LocalDateTime eventDate;
    private EventType eventType;
    private List<String> productAndServiceCategories;
    private List<String> productAndServiceSubcategories;
    private Budget budget;

    public Event() {
    }

    public Event(String eventName, String eventDescription, int maxParticipants, EventPrivacy privacyType,
                 String location, int maxDistanceInKm, LocalDateTime eventDate, EventType eventType,
                 List<String> productAndServiceCategories, List<String> productAndServiceSubcategories, Budget budget) {
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.maxParticipants = maxParticipants;
        this.privacyType = privacyType;
        this.location = location;
        this.maxDistanceInKm = maxDistanceInKm;
        this.eventDate = eventDate;
        this.eventType = eventType;
        this.productAndServiceCategories = productAndServiceCategories;
        this.productAndServiceSubcategories = productAndServiceSubcategories;
        this.budget = budget;
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public EventPrivacy getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(EventPrivacy privacyType) {
        this.privacyType = privacyType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxDistanceInKm() {
        return maxDistanceInKm;
    }

    public void setMaxDistanceInKm(int maxDistanceInKm) {
        this.maxDistanceInKm = maxDistanceInKm;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public List<String> getProductAndServiceCategories() {
        return productAndServiceCategories;
    }

    public void setProductAndServiceCategories(List<String> productAndServiceCategories) {
        this.productAndServiceCategories = productAndServiceCategories;
    }

    public List<String> getProductAndServiceSubcategories() {
        return productAndServiceSubcategories;
    }

    public void setProductAndServiceSubcategories(List<String> productAndServiceSubcategories) {
        this.productAndServiceSubcategories = productAndServiceSubcategories;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }
}