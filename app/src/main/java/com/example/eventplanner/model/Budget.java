package com.example.eventplanner.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Budget {
    private String id;
    private String eventId;
    private String userId;

    private List<BudgetItem> budgetItems;
    private double totalBudget;

    public Budget() {
        budgetItems = new ArrayList<>();
    }

    public Budget(String eventId, String userId, List<BudgetItem> budgetItems, double totalBudget) {
        this.eventId = eventId;
        this.userId = userId;
        this.budgetItems = budgetItems;
        this.totalBudget = totalBudget;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public List<BudgetItem> getBudgetItems() {
        return budgetItems;
    }

    public void setBudgetItems(List<BudgetItem> budgetItems) {
        this.budgetItems = budgetItems;
    }

    public double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(double totalBudget) {
        this.totalBudget = totalBudget;
    }

    public static class BudgetItem {
        private String category;
        private String subcategory;
        private double plannedAmount;
        private boolean isPurchased;

        // No-argument constructor required for Firestore
        public BudgetItem() {
        }

        // Constructor
        public BudgetItem(String category, String subcategory, double plannedAmount) {
            this.category = category;
            this.subcategory = subcategory;
            this.plannedAmount = plannedAmount;
            this.isPurchased = false;
        }

        // Getters and Setters

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getSubcategory() {
            return subcategory;
        }

        public void setSubcategory(String subcategory) {
            this.subcategory = subcategory;
        }

        public double getPlannedAmount() {
            return plannedAmount;
        }

        public void setPlannedAmount(double plannedAmount) {
            this.plannedAmount = plannedAmount;
        }

        public boolean isPurchased() {
            return isPurchased;
        }

        public void setPurchased(boolean purchased) {
            isPurchased = purchased;
        }
    }

    public static Budget createBudgetFromMap(Map<String, Object> budgetData) {
        String budgetId = (String) budgetData.get("budgetId");
        String eventId = (String) budgetData.get("eventId");
        String userId = (String) budgetData.get("userId");
        List<BudgetItem> budgetItems = (List<BudgetItem>) budgetData.get("budgetItems");
        double totalBudget = (double) budgetData.get("totalBudget");
        Budget budget = new Budget(eventId, userId, budgetItems, totalBudget);
        budget.setId(budgetId);
        return budget;
    }

    // Method to convert Budget object to Map
    public Map<String, Object> createBudgetMap() {
        Map<String, Object> budgetData = new HashMap<>();
        budgetData.put("budgetId", id);
        budgetData.put("eventId", eventId);
        budgetData.put("userId", userId);
        budgetData.put("budgetItems", budgetItems);
        budgetData.put("totalBudget", totalBudget);
        return budgetData;
    }

}
