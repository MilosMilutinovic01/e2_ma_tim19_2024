package com.example.eventplanner.model;

public enum ReservationConfirmationType {
    AUTOMATIC,
    MANUAL
}
