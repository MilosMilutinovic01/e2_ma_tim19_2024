package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.fragments.owner.ProductDetailsFragment;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<Product> products;
    Context context;

    public ProductsAdapter(Context context, List<Product> products,RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.products = products;
        this.recyclerViewInterface = recyclerViewInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.product_card, parent, false);
        return new ViewHolder(view,recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = products.get(position);
        if (product != null) {
            double oldPrice = product.getPrice();
            double newPrice = 0.0;
            double discount = product.getDiscount();
            holder.name.setText(product.getName());
            holder.description.setText(product.getDescription());
            Picasso.get()
                    .load(product.getImage())
                    .into(holder.image);
            holder.oldPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", product.getPrice()));
            holder.newPrice.setText("");
            ProductAndServiceCategory category = product.getCategory();
            Subcategory subcategory = product.getSubcategory();
            if (category != null && subcategory != null) {
                holder.category.setText(category.getName() + ", " + subcategory.getName());
            }
            List<EventType> eventTypesList = product.getEventTypes();
            if (eventTypesList != null && !eventTypesList.isEmpty()) {
                StringBuilder eventTypesText = new StringBuilder();
                for (EventType eventType : eventTypesList) {
                    eventTypesText.append(eventType.getName()).append(", ");
                }
                eventTypesText.setLength(eventTypesText.length() - 2);
                holder.eventTypes.setText(eventTypesText.toString());
            }
            if (discount != 0.0) {
                newPrice = oldPrice - (oldPrice * (discount / 100));
                holder.newPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
                holder.oldPrice.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                holder.oldPrice.setPaintFlags(holder.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.newPrice.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
            }
            if (product.isAvailable()) {
                holder.available.setText("AVAILABLE");
                holder.available.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
            } else {
                holder.available.setText("NOT AVAILABLE");
                holder.available.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
            }
            if (product.isVisible()) {
                holder.visible.setText("VISIBLE");
            } else {
                holder.visible.setText("NOT VISIBLE");
            }
        }
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, description, oldPrice, newPrice, category, available, eventTypes, visible;
        ImageView image;
        public ViewHolder(@NonNull View productView, RecyclerViewInterface recyclerViewInterface){
            super(productView);
            name = productView.findViewById(R.id.tvName);
            description = productView.findViewById(R.id.tvDescription);
            image = productView.findViewById(R.id.ivProductImg);
            image.setClipToOutline(true);
            oldPrice = productView.findViewById(R.id.tvOldPrice);
            newPrice = productView.findViewById(R.id.tvNewPrice);
            category = productView.findViewById(R.id.tvCategory);
            available = productView.findViewById(R.id.tvAvailable);
            eventTypes = productView.findViewById(R.id.tvEventTypes);
            visible = productView.findViewById(R.id.tvVisibility);

            productView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }

}
