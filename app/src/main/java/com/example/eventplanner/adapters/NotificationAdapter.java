package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.Notification;

import java.util.List;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<Notification> notifications;
    Context context;

    public NotificationAdapter(Context context, List<Notification> notifications,RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.notifications = notifications;
        this.recyclerViewInterface = recyclerViewInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.notification_card, parent, false);
        return new ViewHolder(view,recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Notification notification = notifications.get(position);
        if (notification != null) {
            holder.title.setText(notification.getTitle().toString());
            holder.body.setText(notification.getBody().toString());
        }
    }

    public void setUserReports(List<Notification> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return notifications != null ? notifications.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView title, body;
        public ViewHolder(@NonNull View notificationView, RecyclerViewInterface recyclerViewInterface){
            super(notificationView);
            title = notificationView.findViewById(R.id.tvTitle);
            body = notificationView.findViewById(R.id.tvBody);
            notificationView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }

}
