package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.fragments.NewServiceAndProductSubcategoryFragment;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;

import java.util.List;

public class ServiceAndProductSubcategoryAdapter extends RecyclerView.Adapter<ServiceAndProductSubcategoryAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<Subcategory> subcategories;

    public ServiceAndProductSubcategoryAdapter(Context context, List<Subcategory> subcategories, RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.subcategories = subcategories;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @NonNull
    @Override
    public ServiceAndProductSubcategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.subcategory_card, parent, false);
        return new ServiceAndProductSubcategoryAdapter.ViewHolder(view, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceAndProductSubcategoryAdapter.ViewHolder holder, int position) {
        Subcategory subcategory = subcategories.get(position);
        holder.name.setText(subcategory.getName());
        holder.description.setText(subcategory.getDescription());
        holder.type.setText(subcategory.getSubcategoryType().toString());
    }

    @Override
    public int getItemCount() {
        return subcategories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, description, type;
        Button editButton;
        Button deleteButton;
        public ViewHolder(@NonNull View subcategoryView, RecyclerViewInterface recyclerViewInterface){
            super(subcategoryView);
            name = subcategoryView.findViewById(R.id.tvSubName);
            description = subcategoryView.findViewById(R.id.tvSubDescription);
            type = subcategoryView.findViewById(R.id.tvSubType);
            editButton = subcategoryView.findViewById(R.id.bSubEdit);
            deleteButton = subcategoryView.findViewById(R.id.bSubDelete);

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onEditClick(position);
                    }
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onDeleteClick(position);
                    }
                }
            });

            subcategoryView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}
