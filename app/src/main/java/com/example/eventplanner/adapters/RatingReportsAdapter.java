package com.example.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.RatingReport;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.UserService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class RatingReportsAdapter extends RecyclerView.Adapter<RatingReportsAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<RatingReport> reports;
    private UserService userService;
    private NotificationService notificationService;
    private View view;

    //private UserService userService;

    public RatingReportsAdapter(Context context, List<RatingReport> reports,
                          RecyclerViewInterface recyclerViewInterface, UserService userService,
                                NotificationService notificationService){
        this.layoutInflater = LayoutInflater.from(context);
        this.reports = reports;
        this.recyclerViewInterface = recyclerViewInterface;
        this.userService = userService;
        this.notificationService = notificationService;
    }


    @NonNull
    @Override
    public RatingReportsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = layoutInflater.inflate(R.layout.rating_report_card, parent, false);
        return new RatingReportsAdapter.ViewHolder(recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingReportsAdapter.ViewHolder holder, int position) {
        RatingReport report = reports.get(position);

        userService.getUserById(report.getReporterId())
                .addOnSuccessListener(user -> {
                    holder.name.setText(user.getName() + " " + user.getSurname());
                    holder.comment.setText(report.getReportReason());
                    holder.date.setText(report.getReportDate().toString());
                    holder.status.setText(report.getStatus().toString());

                    if (report.getStatus() == RatingReport.Status.ACCEPTED ||
                            report.getStatus() == RatingReport.Status.REJECTED) {
                        holder.acceptButton.setEnabled(false);
                        holder.acceptButton.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.darker_gray));
                        holder.rejectButton.setEnabled(false);
                        holder.rejectButton.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.darker_gray));
                    }
                })
                .addOnFailureListener(e -> Log.e(TAG, "Error getting user: " + e.getMessage()));

    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, comment, date, status;
        Button acceptButton, rejectButton, profileButton;
        public ViewHolder(RecyclerViewInterface recyclerViewInterface){
            super(view);
            name = view.findViewById(R.id.tvReporterName);
            comment = view.findViewById(R.id.tvComment);
            date = view.findViewById(R.id.tvDate);
            status = view.findViewById(R.id.tvStatus);
            acceptButton = view.findViewById(R.id.bAccept);
            rejectButton = view.findViewById(R.id.bReject);
            profileButton = view.findViewById(R.id.bProfile);

            profileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onDeleteClick(pos);
                        }
                    }
                }
            });

            rejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onBlockClick(pos);
                        }
                    }
                }
            });

        }
    }
}
