package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.fragments.owner.ProductDetailsFragment;
import com.example.eventplanner.model.Bundle;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.ViewHolder> {

    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<Object> items;
    Context context;

    public PriceListAdapter(Context context, List<Object> items, RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.items = items;
        this.recyclerViewInterface = recyclerViewInterface;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.price_list_card, parent, false);
        return new ViewHolder(view,recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = items.get(position);
        holder.number.setText(String.format(Locale.getDefault(), "%d", position + 1));
        if (item != null) {
            if (item instanceof Product) {
                Product product = (Product) item;
                holder.name.setText(product.getName());
                holder.price.setText(String.format(Locale.getDefault(), "%.2f RSD", product.getPrice()));
                holder.discount.setText(String.format(Locale.getDefault(), "%.0f%%", product.getDiscount()));
                holder.discount.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                double newPrice = product.getPrice() - (product.getPrice() * (product.getDiscount() / 100));
                holder.priceWithDiscount.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
            } else if (item instanceof Service) {
                Service service = (Service) item;
                holder.name.setText(service.getName());
                holder.price.setText(String.format(Locale.getDefault(), "%.2f RSD", service.getPrice()));
                holder.discount.setText(String.format(Locale.getDefault(), "%.0f%%", service.getDiscount()));
                holder.discount.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                double newPrice = service.getPrice() - (service.getPrice() * (service.getDiscount() / 100));
                holder.priceWithDiscount.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
            } else if (item instanceof Bundle) {
                Bundle bundle = (Bundle) item;
                holder.name.setText(bundle.getName());
                holder.price.setText(String.format(Locale.getDefault(), "%.2f RSD", bundle.getPrice()));
                holder.discount.setText(String.format(Locale.getDefault(), "%.0f%%", bundle.getDiscount()));
                holder.discount.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                double newPrice = bundle.getPrice() - (bundle.getPrice() * (bundle.getDiscount() / 100));
                holder.priceWithDiscount.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
            }
        }
    }

    public void setItems(List<Object> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView number, name, price, discount, priceWithDiscount;
        public ViewHolder(@NonNull View priceListView, RecyclerViewInterface recyclerViewInterface){
            super(priceListView);
            number = priceListView.findViewById(R.id.tvNumber);
            name = priceListView.findViewById(R.id.tvName);
            price = priceListView.findViewById(R.id.tvPrice);
            discount = priceListView.findViewById(R.id.tvDiscount);
            priceWithDiscount = priceListView.findViewById(R.id.tvPriceWithDiscount);
            priceListView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}
