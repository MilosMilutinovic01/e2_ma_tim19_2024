package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.EventType;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.chip.ChipGroup;

import java.security.AccessController;
import java.util.List;

public class EventTypesAdapter extends RecyclerView.Adapter<EventTypesAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<EventType> eventTypes;

    public EventTypesAdapter(Context context, List<EventType> eventTypes, RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.eventTypes = eventTypes;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @NonNull
    @Override
    public EventTypesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.event_type_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventTypesAdapter.ViewHolder holder, int position) {
        EventType eventType = eventTypes.get(position);
        holder.name.setText(eventType.getName());
        holder.description.setText(eventType.getDescription());
        holder.chipGroup.removeAllViews();

        /*for(String tag: eventType.getSubcategories()){
            //Chip chip = new Chip(holder.itemView.getContext());
            Chip chip = (Chip) LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.chip_layout, null);
            chip.setText(tag);


            holder.chipGroup.addView(chip);
        }*/
    }

    @Override
    public int getItemCount() {
        return eventTypes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, description;
        ChipGroup chipGroup;
        Button blockButton;
        Button editButton;
        public ViewHolder(@NonNull View eventTypeView){
            super(eventTypeView);
            name = eventTypeView.findViewById(R.id.tvName);
            description = eventTypeView.findViewById(R.id.tvDescription);
            chipGroup = eventTypeView.findViewById(R.id.chip_group);
            blockButton = eventTypeView.findViewById(R.id.bBlock);
            editButton = eventTypeView.findViewById(R.id.bEdit);

            blockButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onBlockClick(position);
                    }
                }
            });

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onEditClick(position);
                    }
                }
            });
        }
    }
}
