package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.ProductAndServiceCategory;

import java.util.List;

public class ServiceAndProductCategoryAdapter extends RecyclerView.Adapter<ServiceAndProductCategoryAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<ProductAndServiceCategory> categories;

    public ServiceAndProductCategoryAdapter(Context context, List<ProductAndServiceCategory> categories, RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.categories = categories;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @NonNull
    @Override
    public ServiceAndProductCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.category_card, parent, false);
        return new ServiceAndProductCategoryAdapter.ViewHolder(view, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceAndProductCategoryAdapter.ViewHolder holder, int position) {
        ProductAndServiceCategory category = categories.get(position);
        holder.name.setText(category.getName());
        holder.description.setText(category.getDescription());
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, description;
        Button editButton;
        Button deleteButton;
        public ViewHolder(@NonNull View categoryView, RecyclerViewInterface recyclerViewInterface){
            super(categoryView);
            name = categoryView.findViewById(R.id.tvName);
            description = categoryView.findViewById(R.id.tvDescription);
            editButton = categoryView.findViewById(R.id.bCatEdit);
            deleteButton = categoryView.findViewById(R.id.bDelete);

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onEditClick(position);
                    }
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onDeleteClick(position);
                    }
                }
            });
            categoryView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}
