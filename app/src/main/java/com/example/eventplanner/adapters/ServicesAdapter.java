package com.example.eventplanner.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.Employer;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<Service> services;
    private Context context;

    public ServicesAdapter(Context context, List<Service> services, RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.services = services;
        this.recyclerViewInterface = recyclerViewInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.service_card, parent, false);
        return new ServicesAdapter.ViewHolder(view, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Service service = services.get(position);
        if(service != null){
            double oldPrice = service.getPrice();
            double newPrice = 0.0;
            double discount = service.getDiscount();
            holder.name.setText(service.getName());
            holder.description.setText(service.getDescription());
            Picasso.get()
                    .load(service.getImage())
                    .into(holder.image);
            holder.oldPrice.setText(String.format(Locale.getDefault(), "%.2f RSD/h", service.getPrice()));
            holder.newPrice.setText("");
            ProductAndServiceCategory category = service.getCategory();
            Subcategory subcategory = service.getSubcategory();
            if (category != null && subcategory != null) {
                holder.category.setText(category.getName() + ", " + subcategory.getName());
            }
            List<EventType> eventTypesList = service.getEventTypes();
            if (eventTypesList != null && !eventTypesList.isEmpty()) {
                StringBuilder eventTypesText = new StringBuilder();
                for (EventType eventType : eventTypesList) {
                    eventTypesText.append(eventType.getName()).append(", ");
                }
                eventTypesText.setLength(eventTypesText.length() - 2);
                holder.eventTypes.setText(eventTypesText.toString());
            }
            if (discount != 0.0) {
                newPrice = oldPrice - (oldPrice * (discount / 100));
                holder.newPrice.setText(String.format(Locale.getDefault(), "%.2f RSD/h", newPrice));
                holder.oldPrice.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                holder.oldPrice.setPaintFlags(holder.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.newPrice.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
            }
            if (service.isAvailable()) {
                holder.available.setText("AVAILABLE");
                holder.available.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
            } else {
                holder.available.setText("NOT AVAILABLE");
                holder.available.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
            }
            if (service.isVisible()) {
                holder.visible.setText("VISIBLE");
            } else {
                holder.visible.setText("NOT VISIBLE");
            }
            holder.duration.setText(String.format(Locale.getDefault(), "Trajanje: %d sati", service.getDuration()));}
            holder.specificities.setText("Specifikacije:" + service.getSpecificities());
    }
    public void setServices(List<Service> services) {
        this.services = services;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() { return services != null ? services.size() : 0; }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, description, oldPrice, newPrice, category, available, eventTypes, visible, duration, specificities;
        ImageView image;
        public ViewHolder(@NonNull View servicesView, RecyclerViewInterface recyclerViewInterface){
            super(servicesView);
            name = servicesView.findViewById(R.id.tvName);
            description = servicesView.findViewById(R.id.tvDescription);
            image = servicesView.findViewById(R.id.ivServiceImg);
            image.setClipToOutline(true);
            oldPrice = servicesView.findViewById(R.id.tvOldPrice);
            newPrice = servicesView.findViewById(R.id.tvNewPrice);
            category = servicesView.findViewById(R.id.tvCategory);
            available = servicesView.findViewById(R.id.tvAvailable);
            eventTypes = servicesView.findViewById(R.id.tvEventTypes);
            visible = servicesView.findViewById(R.id.tvVisibility);
            duration = servicesView.findViewById(R.id.tvDuration);
            specificities = servicesView.findViewById(R.id.tvSpecificities);
            servicesView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}


