package com.example.eventplanner.adapters;

import android.content.Context;
import android.media.Rating;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.async.OnRatingsLoadedListener;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.Role;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.AuthService;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RatingsAdapter extends RecyclerView.Adapter<RatingsAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<CompanyRating> ratings;
    private View view;

    private AuthService authService;

    //private UserService userService;

    public RatingsAdapter(Context context, List<CompanyRating> ratings,
                            RecyclerViewInterface recyclerViewInterface, AuthService authService){
        this.layoutInflater = LayoutInflater.from(context);
        this.ratings = ratings;
        this.recyclerViewInterface = recyclerViewInterface;
        this.authService = authService;
    }

    public void setFilteredList(List<CompanyRating> filteredList){
        this.ratings = filteredList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RatingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = layoutInflater.inflate(R.layout.rating_card, parent, false);
        return new RatingsAdapter.ViewHolder(recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingsAdapter.ViewHolder holder, int position) {
        CompanyRating rating = ratings.get(position);
        holder.rating.setText(String.valueOf(rating.getRating()));
        holder.comment.setText(rating.getComment());
        holder.date.setText(rating.getDate().toString());

        authService.getCurrentUser()
                .addOnSuccessListener(user -> {
                    if(user.getRole() != Role.OWNER){
                        holder.reportButton.setVisibility(View.GONE);
                    }
                });
        }

    @Override
    public int getItemCount() {
        return ratings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView rating, comment, date;
        Button reportButton;
        public ViewHolder(RecyclerViewInterface recyclerViewInterface){
            super(view);
            rating = view.findViewById(R.id.tvRating);
            comment = view.findViewById(R.id.tvComment);
            date = view.findViewById(R.id.tvDate);
            reportButton = view.findViewById(R.id.bReport);


            reportButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });

        }
    }

}
