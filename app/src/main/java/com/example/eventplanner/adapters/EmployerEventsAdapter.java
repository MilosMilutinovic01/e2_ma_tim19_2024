package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.EmployerEvent;
import com.example.eventplanner.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EmployerEventsAdapter extends RecyclerView.Adapter<EmployerEventsAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<EmployerEvent> events;
    private View view;

    public EmployerEventsAdapter(Context context, List<EmployerEvent> events,
                            RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.events = events;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @NonNull
    @Override
    public EmployerEventsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = layoutInflater.inflate(R.layout.event, parent, false);
        return new EmployerEventsAdapter.ViewHolder(recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployerEventsAdapter.ViewHolder holder, int position) {
        EmployerEvent event = events.get(position);
        holder.name.setText(event.getName());
        holder.time.setText(event.getStartTime() + " - " + event.getEndTime());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, time;
        public ViewHolder(RecyclerViewInterface recyclerViewInterface){
            super(view);
            name = view.findViewById(R.id.tvName);
            time = view.findViewById(R.id.tvHours);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}
