package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Bundle;
import com.example.eventplanner.model.Employer;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BundlesAdapter extends RecyclerView.Adapter<BundlesAdapter.ViewHolder> {
    private LayoutInflater layoutInflater;
    private ArrayList<Bundle> bundles;
    private Context context;

    public BundlesAdapter(Context context, ArrayList<Bundle> bundles){
        this.layoutInflater = LayoutInflater.from(context);
        this.bundles = bundles;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.bundle_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bundle bundle = bundles.get(position);
        double oldPrice = bundle.getPrice();
        double newPrice = 0.0;
        double discount = bundle.getDiscount();
        holder.name.setText(bundle.getName());
        holder.description.setText(bundle.getDescription());
        Picasso.get()
                .load(bundle.getImageUrl())
                .into(holder.image);
        holder.oldPrice.setText(String.format(Locale.getDefault(), "%.2f RSD/h", bundle.getPrice()));
        holder.newPrice.setText("");
        holder.category.setText(bundle.getCategory().getName());
        List<EventType> eventTypesList = bundle.getEventTypes();
        if (eventTypesList != null && !eventTypesList.isEmpty()) {
            StringBuilder eventTypesText = new StringBuilder();
            for (EventType eventType : eventTypesList) {
                eventTypesText.append(eventType.getName()).append(", ");
            }
            eventTypesText.setLength(eventTypesText.length() - 2);
            holder.eventTypes.setText(eventTypesText.toString());
        }
        if(discount != 0.0){
            newPrice = oldPrice - (oldPrice * (discount / 100));
            holder.newPrice.setText(String.format(Locale.getDefault(), "%.2f RSD", newPrice));
            holder.oldPrice.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
            holder.oldPrice.setPaintFlags(holder.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.newPrice.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
        }
        if(bundle.isAvailable()){
            holder.available.setText("AVAILABLE");
            holder.available.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
        }
        else{
            holder.available.setText("NOT AVAILABLE");
            holder.available.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
        }
        if(bundle.isVisible()){
            holder.visible.setText("VISIBLE");
        }
        else{
            holder.visible.setText("NOT VISIBLE");
        }
        holder.servicesAndProducts.setText("Snimanje dronom + Snimanje U 4K + Fotografisanje");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return bundles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, description, oldPrice, newPrice, category, available, eventTypes, visible, servicesAndProducts;
        ImageView image;

        public ViewHolder(@NonNull View bundlesView){
                super(bundlesView);
                name = bundlesView.findViewById(R.id.tvName);
                description = bundlesView.findViewById(R.id.tvDescription);
                image = bundlesView.findViewById(R.id.ivBundleImg);
                image.setClipToOutline(true);
                oldPrice = bundlesView.findViewById(R.id.tvOldPrice);
                newPrice = bundlesView.findViewById(R.id.tvNewPrice);
                category = bundlesView.findViewById(R.id.tvCategory);
                available = bundlesView.findViewById(R.id.tvAvailable);
                eventTypes = bundlesView.findViewById(R.id.tvEventTypes);
                visible = bundlesView.findViewById(R.id.tvVisibility);
                servicesAndProducts =  bundlesView.findViewById(R.id.tvServicesAndProducts);
        }
    }

}
