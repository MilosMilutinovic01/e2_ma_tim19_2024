package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.fragments.owner.ProductDetailsFragment;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductAndServiceCategory;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.UserReportStatus;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class UserReportsAdapter extends RecyclerView.Adapter<UserReportsAdapter.ViewHolder> {

    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<UserReport> userReports;
    Context context;
    Button btnReporterProfile,btnReportedProfile,btnAcceptReport,btnRejectReport;

    public UserReportsAdapter(Context context, List<UserReport> userReports,RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.userReports = userReports;
        this.recyclerViewInterface = recyclerViewInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.user_report_card, parent, false);
        return new ViewHolder(view,recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserReport userReport = userReports.get(position);
        if (userReport != null) {
            holder.reporter.setText("Reporter: " + userReport.getReporter().getName() + " " + userReport.getReporter().getSurname());
            holder.reported.setText("Reported: " + userReport.getReported().getName() + " " + userReport.getReported().getSurname());
            holder.status.setText(userReport.getStatus().toString() + " " + userReport.getRejectReason());
            holder.date.setText(userReport.getDate().toString());
            holder.reason.setText(userReport.getReason());
            if (userReport.getStatus() == UserReportStatus.REPORTED) {
                btnAcceptReport.setEnabled(true);
                btnRejectReport.setEnabled(true);
            } else {
                btnAcceptReport.setEnabled(false);
                btnRejectReport.setEnabled(false);
                btnAcceptReport.setBackgroundColor(ContextCompat.getColor(context, R.color.grey));
                btnRejectReport.setBackgroundColor(ContextCompat.getColor(context, R.color.grey));
            }
        }
    }

    public void setUserReports(List<UserReport> userReports) {
        this.userReports = userReports;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return userReports != null ? userReports.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView reporter, reported, status, date, reason;
        public ViewHolder(@NonNull View userReportView, RecyclerViewInterface recyclerViewInterface){
            super(userReportView);
            reporter = userReportView.findViewById(R.id.tvReporter);
            reported = userReportView.findViewById(R.id.tvReported);
            reason = userReportView.findViewById(R.id.tvReason);
            date = userReportView.findViewById(R.id.tvDate);
            status = userReportView.findViewById(R.id.tvStatus);

            btnAcceptReport = userReportView.findViewById(R.id.btnAcceptReport);
            btnRejectReport = userReportView.findViewById(R.id.btnRejectReport);
            btnReportedProfile = userReportView.findViewById(R.id.btnReportedProfile);
            btnReporterProfile = userReportView.findViewById(R.id.btnReporterProfile);

            btnReportedProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onItemClick(position);
                    }
                }
            });

            btnReporterProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onEditClick(position);
                    }
                }
            });

            btnAcceptReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onDeleteClick(position);
                    }
                }
            });
            btnRejectReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Reason for Rejecting Report");

                        final EditText input = new EditText(context);
                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder.setView(input);

                        // Set up the buttons
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String reason = input.getText().toString().trim();

                                // Update the UserReport with the reason for rejection
                                UserReport userReport = userReports.get(position);
                                userReport.setRejectReason(reason);
                                recyclerViewInterface.onBlockClick(position); // Notify the activity/fragment
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                    }
                }
            });
            userReportView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }

}
