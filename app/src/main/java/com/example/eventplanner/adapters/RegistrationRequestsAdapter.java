package com.example.eventplanner.adapters;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.model.RegistrationRequest;
import com.google.android.material.chip.ChipGroup;

import java.util.List;

public class RegistrationRequestsAdapter extends RecyclerView.Adapter<RegistrationRequestsAdapter.ViewHolder>{
    private final RecyclerViewInterface recyclerViewInterface;
    private LayoutInflater layoutInflater;
    private List<RegistrationRequest> registrationRequests;

    public RegistrationRequestsAdapter(Context context, List<RegistrationRequest> registrationRequests, RecyclerViewInterface recyclerViewInterface){
        this.layoutInflater = LayoutInflater.from(context);
        this.registrationRequests = registrationRequests;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    public void setFilteredList(List<RegistrationRequest> filteredList){
        this.registrationRequests = filteredList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RegistrationRequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.registration_request_card, parent, false);
        return new RegistrationRequestsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RegistrationRequestsAdapter.ViewHolder holder, int position) {
        RegistrationRequest registrationRequest = registrationRequests.get(position);
        // Set owner name
        holder.name.setText("Owner Name: " + registrationRequest.getOwnerFirstName() + " " + registrationRequest.getOwnerLastName());

        // Set company name
        holder.companyName.setText("Company Name: " + registrationRequest.getCompanyName());

        // Set owner email
        holder.ownerEmail.setText("Owner Email: " + registrationRequest.getOwnerEmail());

        // Set company email
        holder.companyEmail.setText("Company Email: " + registrationRequest.getCompanyEmail());

        // Set request time
        holder.requestTime.setText("Request Time: " + registrationRequest.getRequestTime().toString());

        // Set categories
        holder.categories.setText("Categories: " + TextUtils.join(", ", registrationRequest.getCategories()));

        // Set event types
        holder.eventTypes.setText("Event Types: " + TextUtils.join(", ", registrationRequest.getEventTypes()));
    }

    @Override
    public int getItemCount() {
        return registrationRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView companyName;
        TextView ownerEmail;
        TextView companyEmail;
        TextView requestTime;
        TextView categories;
        TextView eventTypes;
        Button rejectButton;
        Button acceptButton;
        public ViewHolder(@NonNull View eventTypeView){
            super(eventTypeView);
            name = eventTypeView.findViewById(R.id.tvName);
            companyName = eventTypeView.findViewById(R.id.tvCompanyName);
            ownerEmail = eventTypeView.findViewById(R.id.tvOwnerEmail);
            companyEmail = eventTypeView.findViewById(R.id.tvCompanyEmail);
            requestTime = eventTypeView.findViewById(R.id.tvRequestTime);
            categories = eventTypeView.findViewById(R.id.tvCategories);
            eventTypes = eventTypeView.findViewById(R.id.tvEventTypes);
            rejectButton = eventTypeView.findViewById(R.id.bReject);
            acceptButton = eventTypeView.findViewById(R.id.bAccept);

            rejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onBlockClick(position);
                        notifyDataSetChanged();
                    }
                }
            });

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                        recyclerViewInterface.onEditClick(position);
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }
}
