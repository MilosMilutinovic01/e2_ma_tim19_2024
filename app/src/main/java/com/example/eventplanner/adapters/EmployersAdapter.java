package com.example.eventplanner.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.RecyclerViewInterface;
import com.example.eventplanner.model.Employer;
import com.example.eventplanner.model.User;
import com.example.eventplanner.services.UserService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EmployersAdapter extends RecyclerView.Adapter<EmployersAdapter.ViewHolder> {
   private final RecyclerViewInterface recyclerViewInterface;
   private LayoutInflater layoutInflater;
   private List<User> employers;
   private View view;
   private UserService userService;

   public EmployersAdapter(Context context, List<User> employers,
                           RecyclerViewInterface recyclerViewInterface,
                           UserService userService){
       this.layoutInflater = LayoutInflater.from(context);
       this.employers = employers;
       this.recyclerViewInterface = recyclerViewInterface;
       this.userService = userService;
   }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       view = layoutInflater.inflate(R.layout.worker_card, parent, false);
       return new ViewHolder(recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User employer = employers.get(position);
        holder.name.setText(employer.getName() + " " + employer.getSurname());
        holder.phone.setText(employer.getPhone());
        holder.email.setText(employer.getEmail());
        Picasso.get().load(employer.getProfileImage()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return employers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
       TextView name, email, phone;
       ImageView image;
       Button blockButton;
       public ViewHolder(RecyclerViewInterface recyclerViewInterface){
            super(view);
           name = view.findViewById(R.id.tvName);
           email = view.findViewById(R.id.tvEmail);
           phone = view.findViewById(R.id.tvPhone);
           image = view.findViewById(R.id.ivProfile);
           image.setClipToOutline(true);
           blockButton = view.findViewById(R.id.bBlock);

           blockButton.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   int position = getAdapterPosition();
                   if (position != RecyclerView.NO_POSITION && recyclerViewInterface != null) {
                       //recyclerViewInterface.onBlockButtonClick(position);
                   }
               }
           });

           view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null){
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION){
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }


}
