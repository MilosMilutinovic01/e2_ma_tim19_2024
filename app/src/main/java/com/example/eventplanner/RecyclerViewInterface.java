package com.example.eventplanner;

public interface RecyclerViewInterface {
    void onItemClick(int position);
    void onEditClick(int position);
    void onDeleteClick(int position);
    void onBlockClick(int position);
}
